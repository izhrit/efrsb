drop temporary table if exists manager_emails;
create temporary table manager_emails as
select m.id_Manager, count(em.id_Email) c
from manager m
inner join email_manager em on m.id_Manager=em.id_Manager
group by m.id_Manager;
select avg(c) from manager_emails;

drop temporary table if exists manager_phones;
create temporary table manager_phones as
select m.id_Manager, count(pm.id_Phone) c
from manager m
inner join phone_manager pm on m.id_Manager=pm.id_Manager
group by m.id_Manager;
select avg(c) from manager_phones;

drop temporary table if exists phone_managers;
create temporary table phone_managers as
select pm.id_Phone id_Phone, count(distinct m.id_Manager) c
from phone_manager pm
left join manager m on pm.id_Manager=m.id_Manager
group by id_Phone;

select p.Number `�������`, c `�-�� ��`
from phone_managers  pm
inner join phone p on p.id_Phone=pm.id_Phone
where c>=10 && 11=length(p.Number)
order by c desc
;

drop temporary table if exists email_managers;
create temporary table email_managers as
select em.id_Email id_Email, count(distinct m.id_Manager) c
from email_manager em
left join manager m on em.id_Manager=m.id_Manager
group by id_Email;

select e.address `email`, c `�-�� ��`
from email_managers  em
inner join email e on e.id_Email=em.id_Email
where c>=10
order by c desc
;




