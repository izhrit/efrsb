select 
	(select count(*) from message) messages
	, LastParsedContactsMessageRevision
	, (select count(*) from phone) phones
	, (select count(*) from email) emails
	, (select count(*) from manager) managers
	, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID where s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) active_managers
	, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID left join phone_manager pm on pm.id_Manager=m.id_Manager where pm.id_Phone is null && s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) no_phone
	, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID left join email_manager pe on pe.id_Manager=m.id_Manager where pe.id_Email is null && s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) no_email 

from processstatus;

select (select count(*) from message) messages, LastParsedContactsMessageRevision, (select count(*) from phone) phones, (select count(*) from email) emails, (select count(*) from manager) managers, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID where s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) active_managers, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID left join phone_manager pm on pm.id_Manager=m.id_Manager where pm.id_Phone is null && s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) no_phone, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID left join email_manager pe on pe.id_Manager=m.id_Manager where pe.id_Email is null && s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) no_email from processstatus\G
select (select count(*) from phone) phones, (select count(*) from email) emails, (select count(*) from manager) managers, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID where s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) active_managers, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID left join phone_manager pm on pm.id_Manager=m.id_Manager where pm.id_Phone is null && s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) no_phone, (select count(distinct m.ArbitrManagerID) from manager m inner join message s on s.ArbitrManagerID=m.ArbitrManagerID left join email_manager pe on pe.id_Manager=m.id_Manager where pe.id_Email is null && s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)) no_email from processstatus\G

select count(distinct m.ArbitrManagerID) 
from manager m 
inner join message s on s.ArbitrManagerID=m.ArbitrManagerID 
left join email_manager pe on pe.id_Manager=m.id_Manager 
 left join phone_manager pm on pm.id_Manager=m.id_Manager 
where pe.id_Email is null && pm.id_Phone is null  && s.PublishDate>DATE(NOW() - INTERVAL 4 MONTH)
;




