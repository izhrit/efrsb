select.
    d.Name
    , d.INN
    , d.SNILS
    , rd.Name debtor_region
    , concat(m.LastName,' ',m.FirstName,' ',m.MiddleName) manager
    , rm.Name manager_region
    , s.Name sro
from debtor d
inner join region rd on rd.id_Region=d.id_Region
inner join manager m on d.ArbitrManagerID=m.ArbitrManagerID
inner join region rm on rm.id_Region=d.id_Region
inner join sro s on m.SRORegNum=s.RegNum
where (d.LastMessageDate > '2018-08-01' or d.LastReportDate > '2018-08-01')
and (12=length(d.INN) or d.SNILS is not null)
limit 100
;

select
    count(*)
from debtor d
inner join region rd on rd.id_Region=d.id_Region
inner join manager m on d.ArbitrManagerID=m.ArbitrManagerID
inner join region rm on rm.id_Region=d.id_Region
inner join sro s on m.SRORegNum=s.RegNum
where (d.LastMessageDate > '2019-01-01' or d.LastReportDate > '2019-01-01' or d.DateLastModif>'2019-01-01')
and (12=length(d.INN) or d.SNILS is not null)
;

drop temporary table if exists selected_d;
create temporary table selected_d
select distinct BankruptId
from message m
where m.PublishDate > '2019-01-01'
and (12=length(m.INN) or m.SNILS is not null)
;

select
    count(distinct d.BankruptId)
from debtor d
inner join selected_d sd on sd.BankruptId=d.BankruptId
inner join region rd on rd.id_Region=d.id_Region
inner join manager m on d.ArbitrManagerID=m.ArbitrManagerID
inner join region rm on rm.id_Region=d.id_Region
inner join sro s on m.SRORegNum=s.RegNum
;