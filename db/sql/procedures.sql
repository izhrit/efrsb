delimiter //

drop procedure if exists Add_downloaded_message//
create procedure Add_downloaded_message(
    pINN varchar(12)
  , pSNILS varchar(20)
  , pOGRN varchar(15) -- 8077363037447 
  , pPublishDate DateTime
  , pBody LONGBLOB
  , pefrsb_id int

  , pArbitrManagerID int(11)
  , pBankruptId bigint(20)

  , pMessageInfo_MessageType char(1)

  , pNumber varchar(30)
  , pMessageGUID varchar(32)
)
begin
  declare lMaxRevision bigint;

  select ifnull(max(Revision), 0) into lMaxRevision from message;

  insert into message
    ( INN, SNILS, OGRN, PublishDate,          Body,  efrsb_id,Revision,       ArbitrManagerID, BankruptId, MessageInfo_MessageType, Number, MessageGUID)
  values
    (pINN,pSNILS,pOGRN,pPublishDate,compress(pBody),pefrsb_id,lMaxRevision+1,pArbitrManagerID,pBankruptId,pMessageInfo_MessageType,pNumber,pMessageGUID)
  on duplicate key update efrsb_id = pefrsb_id, 
	ArbitrManagerID=pArbitrManagerID
	, BankruptId=pBankruptId
	, MessageInfo_MessageType=pMessageInfo_MessageType
	, Number=pNumber
	, MessageGUID=pMessageGUID
  ;

end//

drop procedure if exists Add_phone//
create procedure Add_phone(
	 pid_Manager INT
	,pNumber     VARCHAR(11)
	,pid_Message INT
)
begin
	declare lid_Phone int;

	select id_Phone into lid_Phone from phone where `Number`=pNumber;
	if (lid_Phone is null) then
		insert into phone (`Number`) values (pNumber);
		select last_insert_id() into lid_Phone;
	end if;

	insert into phone_manager (id_Manager, id_Phone, id_Messages)
	values (pid_Manager,lid_Phone,compress(pid_Message))
	on duplicate key update id_Messages=compress(concat(uncompress(id_Messages),',',pid_Message));
end//

drop procedure if exists Add_email//
create procedure Add_email(
	 pid_Manager INT
	,pAddress    VARCHAR(50)
	,pid_Message INT
)
begin
	declare lid_Email int;

	select id_Email into lid_Email from email where address=pAddress;
	if (lid_Email is null) then
		insert into email (address) values (pAddress);
		select last_insert_id() into lid_Email;
	end if;

	insert into email_manager (id_Manager, id_Email, id_Messages)
	values (pid_Manager,lid_Email,compress(pid_Message))
	on duplicate key update id_Messages=compress(concat(uncompress(id_Messages),',',pid_Message));
end//

delimiter ;
