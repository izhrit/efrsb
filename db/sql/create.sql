
CREATE TABLE debtor(
    id_Debtor          INT             AUTO_INCREMENT,
    BankruptId         BIGINT          NOT NULL,
    ArbitrManagerID    INT,
    id_Region          INT             NOT NULL,
    Body               LONGBLOB        NOT NULL,
    LastMessageDate    DATETIME,
    LastReportDate     DATETIME,
    INN                VARCHAR(12),
    SNILS              VARCHAR(11),
    Name               VARCHAR(152),
    OGRN               VARCHAR(15),
    DateLastModif      DATETIME        NOT NULL,
    Category           VARCHAR(100)    NOT NULL,
    CategoryCode       VARCHAR(50)     NOT NULL,
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE debtor_manager(
    id_Debtor_Manager        INT         AUTO_INCREMENT,
    BankruptId               BIGINT      NOT NULL,
    ArbitrManagerID          INT         NOT NULL,
    DateTime_MessageFirst    DATETIME    NOT NULL,
    DateTime_MessageLast     DATETIME    NOT NULL,
    PRIMARY KEY (id_Debtor_Manager)
)ENGINE=INNODB
;



CREATE TABLE downloaded_day(
    DownloadedDate    DATE    NOT NULL,
    PRIMARY KEY (DownloadedDate)
)ENGINE=INNODB
;



CREATE TABLE email(
    id_Email    INT            AUTO_INCREMENT,
    address     VARCHAR(50)    NOT NULL,
    PRIMARY KEY (id_Email)
)ENGINE=INNODB
;



CREATE TABLE email_manager(
    id_Email       INT         NOT NULL,
    id_Manager     INT         NOT NULL,
    id_Messages    LONGBLOB    NOT NULL,
    PRIMARY KEY (id_Email, id_Manager)
)ENGINE=INNODB
;



CREATE TABLE manager(
    id_Manager         INT            AUTO_INCREMENT,
    ArbitrManagerID    INT            NOT NULL,
    SRORegNum          VARCHAR(30),
    id_Region          INT,
    FirstName          VARCHAR(50)    NOT NULL,
    MiddleName         VARCHAR(50),
    LastName           VARCHAR(50)    NOT NULL,
    OGRNIP             VARCHAR(15),
    INN                VARCHAR(12)    NOT NULL,
    DateLastModif      DATETIME       NOT NULL,
    DateReg            DATETIME,
    SRORegDate         DATETIME,
    DateDelete         DATETIME,
    Body               LONGBLOB       NOT NULL,
    RegNum             VARCHAR(30),
    DownloadDate       TIMESTAMP      NOT NULL,
    PRIMARY KEY (id_Manager)
)ENGINE=INNODB
;



CREATE TABLE message(
    id_Message                 INT            AUTO_INCREMENT,
    efrsb_id                   INT            NOT NULL,
    BankruptId                 BIGINT,
    ArbitrManagerID            INT,
    INN                        VARCHAR(12),
    SNILS                      VARCHAR(20),
    OGRN                       VARCHAR(15),
    Revision                   BIGINT         NOT NULL,
    PublishDate                DATETIME       NOT NULL,
    Body                       LONGBLOB       NOT NULL,
    MessageInfo_MessageType    CHAR(1),
    Number                     VARCHAR(30),
    MessageGUID                VARCHAR(32),
    PRIMARY KEY (id_Message)
)ENGINE=INNODB
;



CREATE TABLE phone(
    id_Phone    INT            AUTO_INCREMENT,
    Number      VARCHAR(11)    NOT NULL,
    PRIMARY KEY (id_Phone)
)ENGINE=INNODB
;



CREATE TABLE phone_manager(
    id_Manager     INT         NOT NULL,
    id_Phone       INT         NOT NULL,
    id_Messages    LONGBLOB    NOT NULL,
    PRIMARY KEY (id_Manager, id_Phone)
)ENGINE=INNODB
;



CREATE TABLE ProcessStatus(
    LastParsedContactsMessageRevision    BIGINT
)ENGINE=INNODB
;



CREATE TABLE region(
    id_Region    INT             AUTO_INCREMENT,
    Name         VARCHAR(250),
    PRIMARY KEY (id_Region)
)ENGINE=INNODB
;



CREATE TABLE sro(
    id_SRO           INT             NOT NULL,
    UrAdress         VARCHAR(250),
    OGRN             VARCHAR(15),
    Stitle           VARCHAR(250),
    RegNum           VARCHAR(30)     NOT NULL,
    Body             LONGBLOB        NOT NULL,
    INN              VARCHAR(10)     NOT NULL,
    DateLastModif    DATETIME        NOT NULL,
    Name             VARCHAR(250),
    PRIMARY KEY (id_SRO)
)ENGINE=INNODB
;



CREATE TABLE tbl_migration(
    MigrationNumber    VARCHAR(250)    NOT NULL,
    MigrationName      VARCHAR(250)    NOT NULL,
    MigrationTime      TIMESTAMP       NOT NULL,
    PRIMARY KEY (MigrationNumber)
)ENGINE=INNODB
;



CREATE UNIQUE INDEX byBankruptId ON debtor(BankruptId)
;
CREATE INDEX byLastMessageDate ON debtor(LastMessageDate)
;
CREATE INDEX byLastReportDate ON debtor(LastReportDate)
;
CREATE INDEX byINN ON debtor(INN)
;
CREATE INDEX bySNILS ON debtor(SNILS)
;
CREATE INDEX byOGRN ON debtor(OGRN)
;
CREATE INDEX byName ON debtor(Name)
;
CREATE INDEX RefDebtorRegion ON debtor(id_Region)
;
CREATE INDEX RefDebtorLastManager ON debtor(ArbitrManagerID)
;
CREATE UNIQUE INDEX byBankruptArbitrManager ON debtor_manager(BankruptId, ArbitrManagerID)
;
CREATE INDEX RefDebtor_ManagerDebtor ON debtor_manager(BankruptId)
;
CREATE INDEX RefDebtor_ManagerManager ON debtor_manager(ArbitrManagerID)
;
CREATE UNIQUE INDEX byAddress ON email(address)
;
CREATE INDEX RefEmail_ManagerEmail ON email_manager(id_Email)
;
CREATE INDEX RefEmail_ManagerManager ON email_manager(id_Manager)
;
CREATE INDEX byDateLastModif ON manager(DateLastModif)
;
CREATE UNIQUE INDEX byArbitrManagerID ON manager(ArbitrManagerID)
;
CREATE INDEX byRegNum ON manager(RegNum)
;
CREATE INDEX RefManagerRegion ON manager(id_Region)
;
CREATE INDEX refManagerSRO ON manager(SRORegNum)
;
CREATE UNIQUE INDEX byEfrsbID ON message(efrsb_id)
;
CREATE INDEX byPublishDate ON message(PublishDate)
;
CREATE UNIQUE INDEX byInnRevision ON message(INN, Revision)
;
CREATE UNIQUE INDEX byRevision ON message(Revision)
;
CREATE INDEX byInn ON message(INN)
;
CREATE UNIQUE INDEX byOgrnRevision ON message(OGRN, Revision)
;
CREATE UNIQUE INDEX bySnilsRevision ON message(SNILS, Revision)
;
CREATE INDEX byPublishDateBankruptManager ON message(PublishDate, BankruptId, ArbitrManagerID)
;
CREATE INDEX byMessageInfo_MessageType ON message(MessageInfo_MessageType)
;
CREATE INDEX byNumber ON message(Number)
;
CREATE INDEX RefMessageDebtor ON message(BankruptId)
;
CREATE INDEX refMessageManager ON message(ArbitrManagerID)
;
CREATE UNIQUE INDEX byNumber ON phone(Number)
;
CREATE INDEX RefPhone_ManagerManager ON phone_manager(id_Manager)
;
CREATE INDEX RefPhone_ManagerPhone ON phone_manager(id_Phone)
;
CREATE UNIQUE INDEX byName ON region(Name)
;
CREATE UNIQUE INDEX byRegNum ON sro(RegNum)
;
CREATE INDEX byDateLastModif ON sro(DateLastModif)
;
ALTER TABLE debtor ADD CONSTRAINT RefDebtorRegion 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;


ALTER TABLE email_manager ADD CONSTRAINT RefEmail_ManagerEmail 
    FOREIGN KEY (id_Email)
    REFERENCES email(id_Email)
;

ALTER TABLE email_manager ADD CONSTRAINT RefEmail_ManagerManager 
    FOREIGN KEY (id_Manager)
    REFERENCES manager(id_Manager)
;


ALTER TABLE manager ADD CONSTRAINT RefManagerRegion 
    FOREIGN KEY (id_Region)
    REFERENCES region(id_Region)
;

ALTER TABLE phone_manager ADD CONSTRAINT RefPhone_ManagerManager 
    FOREIGN KEY (id_Manager)
    REFERENCES manager(id_Manager)
;

ALTER TABLE phone_manager ADD CONSTRAINT RefPhone_ManagerPhone 
    FOREIGN KEY (id_Phone)
    REFERENCES phone(id_Phone)
;


