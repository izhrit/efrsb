@set MYSQLDIR=C:\Program Files\MySQL\MySQL Server 5.7\bin\
@set DBName="efrsbdevel"

call "%MYSQLDIR%\mysqldump.exe" --defaults-extra-file=%~dp0\mysql.conf --hex-blob --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert --result-file=backup.%date%.%random%.sql %DBName%