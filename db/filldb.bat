@echo *******************************************************

@echo prepare create_sql
@call %~dp0\create_sql.bat

@set DBName=efrsbdevel

@echo prepare empty database %DBName%
@set SQL_DROP_CREATE=drop database if exists %DBName%; create database %DBName% default character set utf8;
@echo %SQL_DROP_CREATE% | %~dp0\run_mysql.bat --database=test

@echo execute create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create.sql
@echo execute post_create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\post_create.sql
@echo execute procedures.sql
@call %~dp0\run_mysql.bat --default-character-set=utf8 < %~dp0\sql\procedures.sql
@echo execute migrations.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\migrations.sql