#! /bin/sh

cd ../../../

# echo m1_DebtorManager.sql
# ./run_mysql.sh < ./repo/db/migrations/m1_DebtorManager.sql

echo m2_MessageIndexes.sql
./run_mysql.sh < ./repo/db/migrations/m2_MessageIndexes.sql

echo procedures.sql
./run_mysql.sh < ./repo/db/sql/procedures.sql

cd ./repo/db/migrations/