alter table message drop KEY `byNumber`;
alter table message add KEY `byNumber` (`Number`);

insert into tbl_migration set MigrationNumber='m016', MigrationName='ChangeNumberIndex';