
alter table message add UNIQUE KEY `byOgrnRevision` (`OGRN`,`Revision`);
alter table message add UNIQUE KEY `bySnilsRevision` (`SNILS`,`Revision`);
alter table message add index `byInn` (`INN`);

insert into tbl_migration
set MigrationNumber='m002'
,   MigrationName='MessageIndexes'
;