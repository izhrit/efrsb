select '' as 'start m11';

select '' as 'add column DownloadDate in manager table';

ALTER TABLE manager
  ADD COLUMN `DownloadDate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;

select '' as 'add phone and email indexes';

ALTER TABLE email add UNIQUE KEY `byAddress` (`address`);
ALTER TABLE phone add UNIQUE KEY `byNumber` (`Number`);

select '' as 'finish m11';
insert into tbl_migration set MigrationNumber='m011', MigrationName='AddDateDownloadManager';