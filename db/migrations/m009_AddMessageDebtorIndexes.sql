select '' as 'start m9';
select '' as 'add key message.byBankruptArbitrManager';
alter table debtor_manager add UNIQUE KEY `byBankruptArbitrManager` (`BankruptId`,`ArbitrManagerID`);
select '' as 'add key message.byPublishDateBankruptManager';
alter table message add KEY `byPublishDateBankruptManager` (`PublishDate`,`BankruptId`,`ArbitrManagerID`);
select '' as 'add key message.byMessageInfo_MessageType';
alter table message add KEY `byMessageInfo_MessageType` (`MessageInfo_MessageType`);
select '' as 'finish m9';
insert into tbl_migration set MigrationNumber='m009', MigrationName='AddMessageDebtorIndexes';