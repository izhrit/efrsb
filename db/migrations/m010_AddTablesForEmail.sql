select '' as 'start m10';
select '' as 'add email table';

CREATE TABLE `email` (
  `address` varchar(50) NOT NULL,
  `id_Email` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

select '' as 'add email_manager table';

CREATE TABLE `email_manager` (
  `id_Manager` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL,
  `id_Email` int(11) NOT NULL,
  PRIMARY KEY (`id_Email`,`id_Manager`),
  KEY `RefEmail_ManagerManager` (`id_Manager`),
  KEY `RefEmail_ManagerEmail` (`id_Email`),
  KEY `RefEmail_ManagerMessage` (`id_Message`),
  CONSTRAINT `RefEmail_ManagerEmail` FOREIGN KEY (`id_Email`) REFERENCES `email` (`id_Email`),
  CONSTRAINT `RefEmail_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `RefEmail_ManagerMessage` FOREIGN KEY (`id_Message`) REFERENCES `message` (`id_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

update tbl_migration set MigrationNumber='m001' where MigrationNumber='m1';
update tbl_migration set MigrationNumber='m002' where MigrationNumber='m2';
update tbl_migration set MigrationNumber='m003' where MigrationNumber='m3';
update tbl_migration set MigrationNumber='m004' where MigrationNumber='m4';
update tbl_migration set MigrationNumber='m005' where MigrationNumber='m5';
update tbl_migration set MigrationNumber='m006' where MigrationNumber='m6';
update tbl_migration set MigrationNumber='m007' where MigrationNumber='m7';
update tbl_migration set MigrationNumber='m008' where MigrationNumber='m8';
update tbl_migration set MigrationNumber='m009' where MigrationNumber='m9';

select '' as 'finish m10';
insert into tbl_migration set MigrationNumber='m010', MigrationName='AddTablesForEmail';