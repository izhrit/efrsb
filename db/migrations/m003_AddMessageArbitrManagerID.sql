
alter table debtor add `ArbitrManagerID` int(11) DEFAULT NULL; 
alter table debtor drop foreign key RefDebtorLastManager; 
alter table debtor drop id_LastManager; 
alter table debtor add index `RefDebtorLastManager` (`ArbitrManagerID`); 

alter table debtor_manager add `ArbitrManagerID` int(11) NOT NULL;
alter table debtor_manager add `BankruptId` bigint(20) NOT NULL;
alter table debtor_manager drop foreign key RefDebtor_ManagerDebtor;
alter table debtor_manager drop foreign key RefDebtor_ManagerManager;
alter table debtor_manager drop id_Debtor;
alter table debtor_manager drop id_Manager;
alter table debtor_manager add KEY `RefDebtor_ManagerDebtor` (`BankruptId`);
alter table debtor_manager add KEY `RefDebtor_ManagerManager` (`ArbitrManagerID`);

alter table message add `ArbitrManagerID` int(11) DEFAULT NULL;
alter table message add KEY `refMessageManager` (`ArbitrManagerID`);

alter table manager drop foreign key `RefManagerSRO`;
alter table manager drop id_SRO;
alter table manager modify `RegNum` varchar(30) DEFAULT NULL;
alter table manager modify `SRORegNum` varchar(30) DEFAULT NULL;
alter table manager add index `refManagerSRO` (`SRORegNum`);

alter table sro add `DateLastModif` datetime NOT NULL;
alter table sro add `RegNum` varchar(30) NOT NULL;
alter table sro add UNIQUE KEY `byRegNum` (`RegNum`);
alter table sro add KEY `byDateLastModif` (`DateLastModif`);

insert into tbl_migration set MigrationNumber='m003', MigrationName='AddMessageArbitrManagerID';