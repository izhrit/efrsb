drop table if exists tbl_migration;
create table tbl_migration
(
  MigrationNumber varchar(250) not null,
  MigrationName varchar(250) not null,
  MigrationTime timestamp,
  PRIMARY KEY (MigrationNumber)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table message add column `MessageInfo_MessageType` varchar(50) DEFAULT NULL;
alter table message drop key byInnOgrnSnilsRevision;
alter table message drop foreign key RefDebtor2;
alter table message drop key `Ref32`;
alter table message add UNIQUE KEY `byInnRevision` (`INN`,`Revision`);

alter table message change column `BankruptId` `BankruptId` bigint(20) DEFAULT NULL;
alter table message add KEY `RefMessageDebtor` (`BankruptId`);

CREATE TABLE `sro` (
  `Body` longblob NOT NULL,
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `Body` longblob NOT NULL,
  `DateDelete` datetime DEFAULT NULL,
  `DateLastModif` datetime NOT NULL,
  `DateReg` datetime NOT NULL,
  `FirstName` varchar(50) NOT NULL,
  `INN` varchar(12) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) NOT NULL,
  `OGRNIP` varchar(15) NOT NULL,
  `RegNum` varchar(30) NOT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `SRORegNum` varchar(30) NOT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) DEFAULT NULL,
  `id_SRO` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Manager`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`),
  KEY `RefManagerRegion` (`id_Region`),
  KEY `RefManagerSRO` (`id_SRO`),
  KEY `byDateLastModif` (`DateLastModif`),
  KEY `byRegNum` (`RegNum`),
  CONSTRAINT `RefManagerRegion` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`),
  CONSTRAINT `RefManagerSRO` FOREIGN KEY (`id_SRO`) REFERENCES `sro` (`id_SRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table debtor add column `CategoryCode` varchar(50) NOT NULL;
alter table debtor add column `Category` varchar(100) NOT NULL;
alter table debtor add column `DateLastModif` datetime NOT NULL;
alter table debtor add column `INN` varchar(12) DEFAULT NULL;
alter table debtor add column `LastMessageDate` datetime DEFAULT NULL;
alter table debtor add column `LastReportDate` datetime DEFAULT NULL;
alter table debtor add column `Name` varchar(152) DEFAULT NULL;
alter table debtor add column `OGRN` varchar(15) DEFAULT NULL;
alter table debtor add column `SNILS` varchar(11) DEFAULT NULL;
alter table debtor add column `id_LastManager` int(11) DEFAULT NULL;

alter table debtor drop foreign key RefRegion1;
alter table debtor drop key Ref41;

alter table debtor add index `RefDebtorLastManager` (`id_LastManager`);
alter table debtor add index `RefDebtorRegion` (`id_Region`);
alter table debtor add CONSTRAINT `RefDebtorLastManager` FOREIGN KEY (`id_LastManager`) REFERENCES `manager` (`id_Manager`);
alter table debtor add CONSTRAINT `RefDebtorRegion` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`);
alter table debtor add UNIQUE KEY `byBankruptId` (`BankruptId`);

CREATE TABLE `debtor_manager` (
  `DateTime_MessageFirst` datetime NOT NULL,
  `DateTime_MessageLast` datetime NOT NULL,
  `id_Debtor_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `id_Debtor` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  PRIMARY KEY (`id_Debtor_Manager`),
  KEY `RefDebtor_ManagerDebtor` (`id_Debtor`),
  KEY `RefDebtor_ManagerManager` (`id_Manager`),
  CONSTRAINT `RefDebtor_ManagerDebtor` FOREIGN KEY (`id_Debtor`) REFERENCES `debtor` (`id_Debtor`),
  CONSTRAINT `RefDebtor_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `phone` (
  `Number` varchar(11) NOT NULL,
  `id_Phone` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `phone_manager` (
  `id_Manager` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL,
  `id_Phone` int(11) NOT NULL,
  PRIMARY KEY (`id_Manager`,`id_Phone`),
  KEY `RefPhone_ManagerManager` (`id_Manager`),
  KEY `RefPhone_ManagerPhone` (`id_Phone`),
  KEY `RefPhone_Manager_Message` (`id_Message`),
  CONSTRAINT `RefPhone_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `RefPhone_ManagerPhone` FOREIGN KEY (`id_Phone`) REFERENCES `phone` (`id_Phone`),
  CONSTRAINT `RefPhone_Manager_Message` FOREIGN KEY (`id_Message`) REFERENCES `message` (`id_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration
set MigrationNumber='m001'
,   MigrationName='DebtorManager'
;