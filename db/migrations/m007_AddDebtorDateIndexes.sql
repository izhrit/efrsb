alter table debtor add KEY `byLastMessageDate` (`LastMessageDate`);
alter table debtor add KEY `byLastReportDate` (`LastReportDate`);

insert into tbl_migration set MigrationNumber='m007', MigrationName='AddDebtorDateIndexes';