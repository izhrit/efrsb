alter table message 
  add column `Number` varchar(30) DEFAULT NULL
, add column `MessageGUID` varchar(32) DEFAULT NULL
, add UNIQUE KEY `byNumber` (`Number`);

insert into tbl_migration set MigrationNumber='m014', MigrationName='AddMessageNumberAndGUID';