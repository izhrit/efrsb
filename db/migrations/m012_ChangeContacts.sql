
alter table email_manager drop foreign key RefEmail_ManagerMessage;
alter table email_manager drop key RefEmail_ManagerMessage;
alter table email_manager drop `id_Message`;
alter table email_manager add `id_Messages` longblob NOT NULL;

alter table phone_manager drop foreign key RefPhone_Manager_Message;
alter table phone_manager drop key RefPhone_Manager_Message;
alter table phone_manager drop `id_Message`;
alter table phone_manager add `id_Messages` longblob NOT NULL;

CREATE TABLE `processstatus` (
  `LastParsedContactsMessageRevision` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into tbl_migration set MigrationNumber='m012', MigrationName='ChangeContacts';