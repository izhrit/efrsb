
alter table debtor add KEY `byINN` (`INN`);
alter table debtor add KEY `byName` (`Name`);
alter table debtor add KEY `byOGRN` (`OGRN`);
alter table debtor add KEY `bySNILS` (`SNILS`);

insert into tbl_migration set MigrationNumber='m013', MigrationName='AddDebtorIndexes';