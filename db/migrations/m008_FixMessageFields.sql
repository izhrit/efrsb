alter table message drop column BankruptType;
alter table message modify MessageInfo_MessageType char(1) DEFAULT NULL;

insert into tbl_migration set MigrationNumber='m008', MigrationName='FixMessageFields';