--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
--



--
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `BankruptId` bigint(20) NOT NULL,
  `Body` longblob NOT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) NOT NULL,
  PRIMARY KEY (`id_Debtor`),
  KEY `Ref41` (`id_Region`)
  CONSTRAINT `RefRegion1` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `downloaded_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloaded_day` (
  `DownloadedDate` date NOT NULL,
  PRIMARY KEY (`DownloadedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `BankruptId` int(11) DEFAULT NULL,
  `BankruptType` char(1) DEFAULT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `PublishDate` datetime NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(20) DEFAULT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Message`),
  UNIQUE KEY `byEfrsbID` (`efrsb_id`),
  UNIQUE KEY `byRevision` (`Revision`),
  KEY `Ref32` (`BankruptId`),
  KEY `byInnOgrnSnilsRevision` (`INN`,`OGRN`,`SNILS`,`Revision`),
  KEY `byPublishDate` (`PublishDate`)
  CONSTRAINT `RefDebtor2` FOREIGN KEY (`BankruptId`) REFERENCES `debtor` (`id_Debtor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `Name` varchar(250) DEFAULT NULL,
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Region`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
/*!50003 DROP PROCEDURE IF EXISTS `Add_downloaded_message` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`efrsbdevel`@`%` PROCEDURE `Add_downloaded_message`(
	  pINN varchar(12)
	, pSNILS varchar(20)
	, pOGRN varchar(15) 
	, pPublishDate DateTime
	, pBody LONGBLOB
	, pefrsb_id int
)
begin
	declare lMaxRevision bigint;

	select ifnull(max(Revision), 0) into lMaxRevision from Message;

	insert into Message
		(INN,SNILS, OGRN, PublishDate, Body, efrsb_id, Revision)
	values
		(pINN,pSNILS,pOGRN,pPublishDate,compress(pBody),pefrsb_id,lMaxRevision+1)
	on duplicate key update efrsb_id = pefrsb_id;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

