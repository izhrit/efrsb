--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
--



--
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `Body` longblob NOT NULL,
  `CategoryCode` varchar(50) NOT NULL,
  `Category` varchar(100) NOT NULL,
  `DateLastModif` datetime NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `LastMessageDate` datetime DEFAULT NULL,
  `LastReportDate` datetime DEFAULT NULL,
  `Name` varchar(152) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `SNILS` varchar(11) DEFAULT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) NOT NULL,
  PRIMARY KEY (`id_Debtor`),
  UNIQUE KEY `byBankruptId` (`BankruptId`),
  KEY `RefDebtorLastManager` (`ArbitrManagerID`),
  KEY `RefDebtorRegion` (`id_Region`),
  KEY `byINN` (`INN`),
  KEY `byLastMessageDate` (`LastMessageDate`),
  KEY `byLastReportDate` (`LastReportDate`),
  KEY `byName` (`Name`),
  KEY `byOGRN` (`OGRN`),
  KEY `bySNILS` (`SNILS`)
  CONSTRAINT `RefDebtorRegion` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `debtor_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor_manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `BankruptId` bigint(20) NOT NULL,
  `DateTime_MessageFirst` datetime NOT NULL,
  `DateTime_MessageLast` datetime NOT NULL,
  `id_Debtor_Manager` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Debtor_Manager`),
  UNIQUE KEY `byBankruptArbitrManager` (`BankruptId`,`ArbitrManagerID`),
  KEY `RefDebtor_ManagerDebtor` (`BankruptId`),
  KEY `RefDebtor_ManagerManager` (`ArbitrManagerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `downloaded_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloaded_day` (
  `DownloadedDate` date NOT NULL,
  PRIMARY KEY (`DownloadedDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email` (
  `address` varchar(50) NOT NULL,
  `id_Email` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email`),
  UNIQUE KEY `byAddress` (`address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_manager` (
  `id_Email` int(11) NOT NULL,
  `id_Manager` int(11) NOT NULL,
  `id_Messages` longblob NOT NULL,
  PRIMARY KEY (`id_Email`,`id_Manager`),
  KEY `RefEmail_ManagerEmail` (`id_Email`),
  KEY `RefEmail_ManagerManager` (`id_Manager`)
  CONSTRAINT `RefEmail_ManagerEmail` FOREIGN KEY (`id_Email`) REFERENCES `email` (`id_Email`),
  CONSTRAINT `RefEmail_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `ArbitrManagerID` int(11) NOT NULL,
  `Body` longblob NOT NULL,
  `DateDelete` datetime DEFAULT NULL,
  `DateLastModif` datetime NOT NULL,
  `DateReg` datetime DEFAULT NULL,
  `DownloadDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FirstName` varchar(50) NOT NULL,
  `INN` varchar(12) NOT NULL,
  `LastName` varchar(50) NOT NULL,
  `MiddleName` varchar(50) DEFAULT NULL,
  `OGRNIP` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) DEFAULT NULL,
  `SRORegDate` datetime DEFAULT NULL,
  `SRORegNum` varchar(30) DEFAULT NULL,
  `id_Manager` int(11) NOT NULL AUTO_INCREMENT,
  `id_Region` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Manager`),
  UNIQUE KEY `byArbitrManagerID` (`ArbitrManagerID`),
  KEY `RefManagerRegion` (`id_Region`),
  KEY `byDateLastModif` (`DateLastModif`),
  KEY `byRegNum` (`RegNum`),
  KEY `refManagerSRO` (`SRORegNum`)
  CONSTRAINT `RefManagerRegion` FOREIGN KEY (`id_Region`) REFERENCES `region` (`id_Region`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `ArbitrManagerID` int(11) DEFAULT NULL,
  `BankruptId` bigint(20) DEFAULT NULL,
  `Body` longblob NOT NULL,
  `INN` varchar(12) DEFAULT NULL,
  `MessageGUID` varchar(32) DEFAULT NULL,
  `MessageInfo_MessageType` char(1) DEFAULT NULL,
  `Number` varchar(30) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `PublishDate` datetime NOT NULL,
  `Revision` bigint(20) NOT NULL,
  `SNILS` varchar(20) DEFAULT NULL,
  `efrsb_id` int(11) NOT NULL,
  `id_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Message`),
  UNIQUE KEY `byEfrsbID` (`efrsb_id`),
  UNIQUE KEY `byRevision` (`Revision`),
  UNIQUE KEY `byInnRevision` (`INN`,`Revision`),
  UNIQUE KEY `byOgrnRevision` (`OGRN`,`Revision`),
  UNIQUE KEY `bySnilsRevision` (`SNILS`,`Revision`),
  KEY `RefMessageDebtor` (`BankruptId`),
  KEY `byInn` (`INN`),
  KEY `byMessageInfo_MessageType` (`MessageInfo_MessageType`),
  KEY `byNumber` (`Number`),
  KEY `byPublishDateBankruptManager` (`PublishDate`,`BankruptId`,`ArbitrManagerID`),
  KEY `byPublishDate` (`PublishDate`),
  KEY `refMessageManager` (`ArbitrManagerID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `phone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phone` (
  `Number` varchar(11) NOT NULL,
  `id_Phone` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Phone`),
  UNIQUE KEY `byNumber` (`Number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `phone_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phone_manager` (
  `id_Manager` int(11) NOT NULL,
  `id_Messages` longblob NOT NULL,
  `id_Phone` int(11) NOT NULL,
  PRIMARY KEY (`id_Manager`,`id_Phone`),
  KEY `RefPhone_ManagerManager` (`id_Manager`),
  KEY `RefPhone_ManagerPhone` (`id_Phone`)
  CONSTRAINT `RefPhone_ManagerManager` FOREIGN KEY (`id_Manager`) REFERENCES `manager` (`id_Manager`),
  CONSTRAINT `RefPhone_ManagerPhone` FOREIGN KEY (`id_Phone`) REFERENCES `phone` (`id_Phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `processstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processstatus` (
  `LastParsedContactsMessageRevision` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `Name` varchar(250) DEFAULT NULL,
  `id_Region` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Region`),
  UNIQUE KEY `byName` (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `sro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sro` (
  `Body` longblob NOT NULL,
  `DateLastModif` datetime NOT NULL,
  `INN` varchar(10) NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `OGRN` varchar(15) DEFAULT NULL,
  `RegNum` varchar(30) NOT NULL,
  `Stitle` varchar(250) DEFAULT NULL,
  `UrAdress` varchar(250) DEFAULT NULL,
  `id_SRO` int(11) NOT NULL,
  PRIMARY KEY (`id_SRO`),
  UNIQUE KEY `byRegNum` (`RegNum`),
  KEY `byDateLastModif` (`DateLastModif`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationName` varchar(250) NOT NULL,
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
/*!50003 DROP PROCEDURE IF EXISTS `Add_downloaded_message` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`efrsbdevel`@`%` PROCEDURE `Add_downloaded_message`(
    pINN varchar(12)
  , pSNILS varchar(20)
  , pOGRN varchar(15) 
  , pPublishDate DateTime
  , pBody LONGBLOB
  , pefrsb_id int

  , pArbitrManagerID int(11)
  , pBankruptId bigint(20)

  , pMessageInfo_MessageType char(1)

  , pNumber varchar(30)
  , pMessageGUID varchar(32)
)
begin
  declare lMaxRevision bigint;

  select ifnull(max(Revision), 0) into lMaxRevision from message;

  insert into message
    ( INN, SNILS, OGRN, PublishDate,          Body,  efrsb_id,Revision,       ArbitrManagerID, BankruptId, MessageInfo_MessageType, Number, MessageGUID)
  values
    (pINN,pSNILS,pOGRN,pPublishDate,compress(pBody),pefrsb_id,lMaxRevision+1,pArbitrManagerID,pBankruptId,pMessageInfo_MessageType,pNumber,pMessageGUID)
  on duplicate key update efrsb_id = pefrsb_id, 
	ArbitrManagerID=pArbitrManagerID
	, BankruptId=pBankruptId
	, MessageInfo_MessageType=pMessageInfo_MessageType
	, Number=pNumber
	, MessageGUID=pMessageGUID
  ;

end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_email` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`efrsbdevel`@`%` PROCEDURE `Add_email`(
	 pid_Manager INT
	,pAddress    VARCHAR(50)
	,pid_Message INT
)
begin
	declare lid_Email int;

	select id_Email into lid_Email from email where address=pAddress;
	if (lid_Email is null) then
		insert into email (address) values (pAddress);
		select last_insert_id() into lid_Email;
	end if;

	insert into email_manager (id_Manager, id_Email, id_Messages)
	values (pid_Manager,lid_Email,compress(pid_Message))
	on duplicate key update id_Messages=compress(concat(uncompress(id_Messages),',',pid_Message));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Add_phone` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`efrsbdevel`@`%` PROCEDURE `Add_phone`(
	 pid_Manager INT
	,pNumber     VARCHAR(11)
	,pid_Message INT
)
begin
	declare lid_Phone int;

	select id_Phone into lid_Phone from phone where `Number`=pNumber;
	if (lid_Phone is null) then
		insert into phone (`Number`) values (pNumber);
		select last_insert_id() into lid_Phone;
	end if;

	insert into phone_manager (id_Manager, id_Phone, id_Messages)
	values (pid_Manager,lid_Phone,compress(pid_Message))
	on duplicate key update id_Messages=compress(concat(uncompress(id_Messages),',',pid_Message));
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

