﻿insert into region set id_Region=333, Name='Мордор';

insert into `debtor` set `id_Debtor`= 1, `BankruptId`= 1, `ArbitrManagerID`= 1, `Name`= 'ООО Герундий', `INN`= '7907655280', `OGRN`= '1136422185130'
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `debtor` set `id_Debtor`= 2, `BankruptId`= 2, `ArbitrManagerID`= 1, `Name`= 'Колкий Колк Колкович', `INN`= '436364310031', `SNILS`= '79972271685'
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `debtor` set `id_Debtor`= 3, `BankruptId`= 3, `ArbitrManagerID`= 2, `Name`= 'ОАО Порт семи морей', `INN`= '8380289542'
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `debtor` set `id_Debtor`= 4, `BankruptId`= 4, `Name`= 'ООО Снявские майки', `INN`= '5591845539', `ArbitrManagerID`= 2
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `debtor` set `id_Debtor`= 5, `BankruptId`= 5, `Name`= 'ИП Троцкий Л Д', `INN`= '929309724309', `ArbitrManagerID`= 1
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';
insert into `debtor` set `id_Debtor`= 6, `BankruptId`= 6, `Name`= 'ООО "Новострой-Технология"', `INN`= '322223234', `OGRN`= '234234', `ArbitrManagerID`= 3
, `Body`= compress(''), id_Region=333, CategoryCode='', Category='', DateLastModif='2020-01-01';


insert into `manager` set `id_Manager`= 1, `ArbitrManagerID`= 1, `FirstName`= 'Олег', `LastName`= 'Олегов', `MiddleName`= 'Олегович', `INN`= '055554427038', `RegNum`= 2
, `Body`= compress(''), DateLastModif='2020-01-01', DownloadDate='2020-01-01';
insert into `manager` set `id_Manager`= 2, `ArbitrManagerID`= 2, `FirstName`= 'Юлия', `LastName`= 'Юльева', `MiddleName`= 'Юльевна', `INN`= '167591839508'
, `Body`= compress(''), DateLastModif='2020-01-01', DownloadDate='2020-01-01';
insert into `manager` set `id_Manager`= 3, `ArbitrManagerID`= 3, `FirstName`= 'Иван', `LastName`= 'Иванов', `MiddleName`= 'Иванович', `INN`= '364484332544'
, `Body`= compress(''), DateLastModif='2020-01-01', DownloadDate='2020-01-01';

insert into `debtor_manager` set `id_Debtor_manager`= 1, `BankruptId`= 1, `ArbitrManagerID`= 1
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `debtor_manager` set `id_Debtor_manager`= 2, `BankruptId`= 2, `ArbitrManagerID`= 2
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `debtor_manager` set `id_Debtor_manager`= 3, `BankruptId`= 3, `ArbitrManagerID`= 3
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `debtor_manager` set `id_Debtor_manager`= 4, `BankruptId`= 4, `ArbitrManagerID`= 1
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `debtor_manager` set `id_Debtor_manager`= 5, `BankruptId`= 5, `ArbitrManagerID`= 2
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `debtor_manager` set `id_Debtor_manager`= 6, `BankruptId`= 6, `ArbitrManagerID`= 3
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';
insert into `debtor_manager` set `id_Debtor_manager`= 7, `BankruptId`= 2, `ArbitrManagerID`= 1
, DateTime_MessageFirst='2020-01-01', DateTime_MessageLast='2020-01-01';

insert into `message` set `id_Message`= 1001, `ArbitrManagerID`= 1, `BankruptId`= 1, `INN`= '7907655280', `OGRN`= '1136422185130', `PublishDate`= '2019-10-15T10:01', `MessageInfo_MessageType`= 'f', `Number`= '345', `MessageGUID`= '234234', `efrsb_id`= '1', `Body`= compress('')
, Revision=1001;
insert into `message` set `id_Message`= 1002, `ArbitrManagerID`= 2, `BankruptId`= 2, `INN`= '436364310031', `SNILS`= '79972271685', `PublishDate`= '2019-10-15T10:01', `MessageInfo_MessageType`= 'g', `Number`= '567', `MessageGUID`= '26378', `efrsb_id`= '2', `Body`= compress('')
, Revision=1002;
insert into `message` set `id_Message`= 1003, `ArbitrManagerID`= 1, `BankruptId`= 1, `INN`= '7907655280', `OGRN`= '1136422185130', `PublishDate`= '2019-10-15T10:01', `MessageInfo_MessageType`= 'h', `Number`= '239', `MessageGUID`= '36767', `efrsb_id`= '3', `Body`= compress('')
, Revision=1003;
insert into `message` set `id_Message`= 1004, `ArbitrManagerID`= 1, `BankruptId`= 1, `INN`= '7907655280', `OGRN`= '1136422185130', `PublishDate`= '2019-10-15T10:01', `MessageInfo_MessageType`= 'k', `Number`= '903', `MessageGUID`= '9234', `efrsb_id`= '4', `Body`= compress('')
, Revision=1004;