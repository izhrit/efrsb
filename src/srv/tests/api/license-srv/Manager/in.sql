INSERT INTO Region set Name='Mordor';
set @id_region= last_insert_id();
INSERT INTO debtor set BankruptId=1000, id_Region=@id_region, Body=compress(''), CategoryCode='c', Category='ccc', DateLastModif=now();
set @id_debtor= last_insert_id();

-- �������� �� c ����� ���������� � ����� email ��
INSERT INTO manager (ArbitrManagerID, Body, DateLastModif, FirstName, LastName, MiddleName, INN, OGRNIP, RegNum)
VALUES ('1112', compress('Lorem1'), '2019-07-23', 'm', 'm','m', 'm', '000000', '0321');
set @id_manager1= last_insert_id();

-- �� c ����� ���������� � ����� email �� ��� ����� � �����������
INSERT INTO manager (ArbitrManagerID, Body, DateLastModif, FirstName, LastName, MiddleName, INN, OGRNIP, RegNum)
VALUES ('1113', compress('Lorem1'), '2019-07-23', 'm', 'm','m', 'm', '000000', '0321');
set @id_manager2= last_insert_id();

-- �� �������� �� c ����� ���������� � ����� email �� ��� ����� � �����������
INSERT INTO manager (ArbitrManagerID, Body, DateLastModif, FirstName, LastName, MiddleName, INN, OGRNIP, RegNum)
VALUES ('1114', compress('Lorem1'), '2019-07-23', 'm4', 'm','m', 'm', '000000', '0321');
set @id_manager3= last_insert_id();

-- �������� �� ��� ��������� � email ��
INSERT INTO manager (ArbitrManagerID, Body, DateLastModif, FirstName, LastName, MiddleName, INN, OGRNIP, RegNum)
VALUES ('1115', compress('Lorem1'), '2019-07-23', 'm4', 'm','m', 'm', '000000', '0321');
set @id_manager4= last_insert_id();

-- �������� �� � ����� ���������
INSERT INTO manager (ArbitrManagerID, Body, DateLastModif, FirstName, LastName, MiddleName, INN, OGRNIP, RegNum)
VALUES ('1116', compress('Lorem1'), '2019-07-23', 'm4', 'm','m', 'm', '000000', '0321');
set @id_manager5= last_insert_id();

-- �������� �� � ����� email ��
INSERT INTO manager (ArbitrManagerID, Body, DateLastModif, FirstName, LastName, MiddleName, INN, OGRNIP, RegNum)
VALUES ('1117', compress('Lorem1'), '2019-07-23', 'm4', 'm','m', 'm', '000000', '0321');
set @id_manager6= last_insert_id();

insert into debtor_manager set BankruptId=1000, ArbitrManagerID= '1112', DateTime_MessageFirst=now(), DateTime_MessageLast=now();
insert into debtor_manager set BankruptId=1000, ArbitrManagerID= '1114', DateTime_MessageFirst=now(), DateTime_MessageLast=DATE(NOW() - INTERVAL 5 MONTH);
insert into debtor_manager set BankruptId=1000, ArbitrManagerID= '1115', DateTime_MessageFirst=now(), DateTime_MessageLast=now();
insert into debtor_manager set BankruptId=1000, ArbitrManagerID= '1116', DateTime_MessageFirst=now(), DateTime_MessageLast=now();
insert into debtor_manager set BankruptId=1000, ArbitrManagerID= '1117', DateTime_MessageFirst=now(), DateTime_MessageLast=now();

INSERT INTO message (efrsb_id, Revision, PublishDate, Body, ArbitrManagerID)
VALUES (1661764,9,NOW(),compress('Lorem2'),1112);
set @id_message1= last_insert_id();

INSERT INTO email set address='test@email.ru';  set @id_email1= last_insert_id();
INSERT INTO email set address='test2@email.ru'; set @id_email2= last_insert_id();

INSERT INTO phone set Number='89111111111';     set @id_phone1= last_insert_id();
INSERT INTO phone set Number='89111111112';     set @id_phone2= last_insert_id();

INSERT INTO email_manager 
 (id_Manager,   id_Messages,   id_Email) 
values
 (@id_manager1, @id_message1, @id_email1)
,(@id_manager1, @id_message1, @id_email2)

,(@id_manager2, @id_message1, @id_email1)
,(@id_manager2, @id_message1, @id_email2)

,(@id_manager3, @id_message1, @id_email1)
,(@id_manager3, @id_message1, @id_email2)

,(@id_manager6, @id_message1, @id_email2)
;

INSERT INTO phone_manager 
 (id_Manager,   id_Messages,   id_Phone) 
values
 (@id_manager1, @id_message1, @id_phone1)
,(@id_manager1, @id_message1, @id_phone2)

,(@id_manager2, @id_message1, @id_phone1)
,(@id_manager2, @id_message1, @id_phone2)

,(@id_manager3, @id_message1, @id_phone1)
,(@id_manager3, @id_message1, @id_phone2)

,(@id_manager5, @id_message1, @id_phone2)
;