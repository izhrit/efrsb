 {"procedures":{
	"Procedures":
	[
		{
			"DealNumber":"А06-10774/2015"
			,"Type":"Н"
			,"deptor_name":"Общество с ограниченной отвественностью \"Торглайт\""
			,"deptor_inn":"3015012501"
			,"deptor_ogrn":"1023000927520"
			,"deptor_snils":""
			,"manager_name":"Баунова Евгения Юрьевна"
			,"manager_efrsb":"14377"
			,"contract_number":"6509"
			,"id_Procedure":111503
		}
		,{
			"DealNumber":"А44-8108/2016"
			,"Type":"Н"
			,"deptor_name":"ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ \"НОВЫЙ - ПОРТ\""
			,"deptor_inn":"5321082082"
			,"deptor_ogrn":"1025300797400"
			,"deptor_snils":""
			,"manager_name":"Баунова Евгения Юрьевна"
			,"manager_efrsb":"14377"
			,"contract_number":"6509"
			,"id_Procedure":111504
		}
	]
	,"Managers":
	[
		{
			"Name":"Богданов Андрей Борисович"
			,"EFRSBNumber":"9347"
		}
		,{
			"Name":"Баунова Евгения Юрьевна"
			,"EFRSBNumber":"14377"
		}
	]
}
}