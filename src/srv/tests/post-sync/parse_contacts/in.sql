INSERT INTO `manager` (ArbitrManagerID, Body, DateLastModif, FirstName, LastName, MiddleName, INN, OGRNIP, RegNum)
VALUES ('-1112', compress('content'), '2019-07-23', 'manager', 'manager','manager', 'manager', '000000', '0321');

INSERT INTO `message` 
 (efrsb_id, Revision, PublishDate, ArbitrManagerID, Body)
VALUES  
 (1661764,  10009,'2019-02-12 16:43:15',-1112,compress('Lorem ipsum dolor sit amet, +7-922-44-55-667 consectetur test@mail.ru adipiscing elit'))
,(1661765, 100010,'2019-02-12 16:43:15',-1112,compress('Lorem ipsum dolor sit amet, 79224455668 consectetur mail@mail.ru adipiscing elit'))
,(1661766, 100011,'2019-02-12 16:43:15',-1112,compress('Lorem ipsum dolor sit amet, 89224455669 consectetur mail@mailru adipiscing elit'))
,(1661767, 100012,'2019-02-12 16:43:15',-1112,compress('Lorem ipsum dolor sit amet, +7-922-44-55-667 consectetur test@mail.ru adipiscing elit'))
;
