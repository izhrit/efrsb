*************************** 1. row ***************************
ArbitrManagerID: 22328
     DateDelete: NULL
  DateLastModif: 2019-04-16 17:35:55
      FirstName: Ерофей
       LastName: Ерофеев
     MiddleName: Ерофеевич
            INN: 635007494339
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: 2019-04-10 00:00:00
      SRORegNum: 001-4
      id_Region: 1
           Body: {
	"!ArbitrManagerID": "22328",
	"!FirstName": "Ерофей",
	"!MiddleName": "Ерофеевич",
	"!LastName": "Ерофеев",
	"!RegNum": "",
	"!INN": "635007494339",
	"!OGRNIP": "",
	"!SRORegNum": "001-4",
	"!DateLastModif": "2019-04-16T17:35:54.517",
	"!Region": "Владимирская область",
	"!SRORegDate": "2019-04-10T00:00:00"
}
*************************** 2. row ***************************
ArbitrManagerID: 12329
     DateDelete: NULL
  DateLastModif: 2019-04-18 15:35:41
      FirstName: Татьяна
       LastName: Яманчева
     MiddleName: Владимировна
            INN: 732700875560
         RegNum: 7951
         OGRNIP: 
         RegNum: 7951
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 2
           Body: {
	"!ArbitrManagerID": "12329",
	"!FirstName": "Татьяна",
	"!MiddleName": "Владимировна",
	"!LastName": "Яманчева",
	"!RegNum": "7951",
	"!INN": "732700875560",
	"!OGRNIP": "",
	"!DateLastModif": "2019-04-18T15:35:41.37",
	"!Region": "Ульяновская область",
	"!DateReg": "2006-11-27T00:00:00"
}
*************************** 3. row ***************************
ArbitrManagerID: 22329
     DateDelete: NULL
  DateLastModif: 2019-04-18 15:52:12
      FirstName: Улей 
       LastName: Изначальный 
     MiddleName: Диогенович
            INN: 638422355509
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 3
           Body: {
	"!ArbitrManagerID": "22329",
	"!FirstName": "Улей ",
	"!MiddleName": "Диогенович",
	"!LastName": "Изначальный ",
	"!RegNum": "",
	"!INN": "638422355509",
	"!OGRNIP": "",
	"!DateLastModif": "2019-04-18T15:52:12.403",
	"!Region": "Амурская область"
}
*************************** 4. row ***************************
ArbitrManagerID: 22330
     DateDelete: NULL
  DateLastModif: 2019-04-18 15:54:48
      FirstName: Апогей 
       LastName: Истребляющий 
     MiddleName: Парацельсиевич
            INN: 227917330829
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 4
           Body: {
	"!ArbitrManagerID": "22330",
	"!FirstName": "Апогей ",
	"!MiddleName": "Парацельсиевич",
	"!LastName": "Истребляющий ",
	"!RegNum": "",
	"!INN": "227917330829",
	"!OGRNIP": "",
	"!DateLastModif": "2019-04-18T15:54:48.197",
	"!Region": "Алтайский край"
}
*************************** 5. row ***************************
ArbitrManagerID: 22331
     DateDelete: NULL
  DateLastModif: 2019-04-18 16:15:08
      FirstName: Медальон
       LastName: Ужасный
     MiddleName: Бэконович
            INN: 503734876601
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 5
           Body: {
	"!ArbitrManagerID": "22331",
	"!FirstName": "Медальон",
	"!MiddleName": "Бэконович",
	"!LastName": "Ужасный",
	"!RegNum": "",
	"!INN": "503734876601",
	"!OGRNIP": "",
	"!DateLastModif": "2019-04-18T16:15:07.513",
	"!Region": "Астраханская область"
}
*************************** 6. row ***************************
ArbitrManagerID: 16909
     DateDelete: NULL
  DateLastModif: 2019-04-22 14:42:33
      FirstName: Светлана
       LastName: Бондаренко
     MiddleName: Александровна
            INN: 666203529719
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: 2019-04-22 00:00:00
      SRORegNum: 0037
      id_Region: 6
           Body: {
	"!ArbitrManagerID": "16909",
	"!FirstName": "Светлана",
	"!MiddleName": "Александровна",
	"!LastName": "Бондаренко",
	"!RegNum": "",
	"!INN": "666203529719",
	"!OGRNIP": "",
	"!SRORegNum": "0037",
	"!DateLastModif": "2019-04-22T14:42:32.65",
	"!Region": "Свердловская область",
	"!SRORegDate": "2019-04-22T00:00:00"
}
*************************** 7. row ***************************
ArbitrManagerID: 22311
     DateDelete: NULL
  DateLastModif: 2019-04-30 10:27:46
      FirstName: Мария
       LastName: Баннова
     MiddleName: Николаевна
            INN: 525613699841
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: 2017-10-17 00:00:00
      SRORegNum: 0022
      id_Region: 4
           Body: {
	"!ArbitrManagerID": "22311",
	"!FirstName": "Мария",
	"!MiddleName": "Николаевна",
	"!LastName": "Баннова",
	"!RegNum": "",
	"!INN": "525613699841",
	"!OGRNIP": "",
	"!SRORegNum": "0022",
	"!DateLastModif": "2019-04-30T10:27:46.147",
	"!Region": "Алтайский край",
	"!SRORegDate": "2017-10-17T00:00:00"
}
*************************** 8. row ***************************
ArbitrManagerID: 22312
     DateDelete: NULL
  DateLastModif: 2019-09-06 10:03:14
      FirstName: Татьяна
       LastName: Одина
     MiddleName: Николаевна
            INN: 526017964561
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 4
           Body: {
	"!ArbitrManagerID": "22312",
	"!FirstName": "Татьяна",
	"!MiddleName": "Николаевна",
	"!LastName": "Одина",
	"!RegNum": "",
	"!INN": "526017964561",
	"!OGRNIP": "",
	"!DateLastModif": "2019-09-06T10:03:13.517",
	"!Region": "Алтайский край"
}
*************************** 9. row ***************************
ArbitrManagerID: 22310
     DateDelete: NULL
  DateLastModif: 2019-11-19 12:47:52
      FirstName: Татьяна
       LastName: Новикова
     MiddleName: Сергеевна
            INN: 526004723463
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 7
           Body: {
	"!ArbitrManagerID": "22310",
	"!FirstName": "Татьяна",
	"!MiddleName": "Сергеевна",
	"!LastName": "Новикова",
	"!RegNum": "",
	"!INN": "526004723463",
	"!OGRNIP": "",
	"!DateLastModif": "2019-11-19T12:47:52.243",
	"!Region": "Волгоградская область"
}
*************************** 10. row ***************************
ArbitrManagerID: 22297
     DateDelete: NULL
  DateLastModif: 2019-12-24 17:31:21
      FirstName: Иван
       LastName: Васильев
     MiddleName: Петрович
            INN: 110115686508
         RegNum: 457674635345
         OGRNIP: 
         RegNum: 457674635345
     SRORegDate: NULL
      SRORegNum: 001-1
      id_Region: 8
           Body: {
	"!ArbitrManagerID": "22297",
	"!FirstName": "Иван",
	"!MiddleName": "Петрович",
	"!LastName": "Васильев",
	"!RegNum": "457674635345",
	"!INN": "110115686508",
	"!OGRNIP": "",
	"!SRORegNum": "001-1",
	"!DateLastModif": "2019-12-24T17:31:21.46",
	"!Region": "г. Москва",
	"!DateReg": "2012-12-19T00:00:00"
}
*************************** 11. row ***************************
ArbitrManagerID: 10740
     DateDelete: NULL
  DateLastModif: 2020-01-17 13:06:50
      FirstName: Амерби
       LastName: Абазов
     MiddleName: Каншобиевич
            INN: 071306421073
         RegNum: 2
         OGRNIP: 
         RegNum: 2
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 9
           Body: {
	"!ArbitrManagerID": "10740",
	"!FirstName": "Амерби",
	"!MiddleName": "Каншобиевич",
	"!LastName": "Абазов",
	"!RegNum": "2",
	"!INN": "071306421073",
	"!OGRNIP": "",
	"!DateLastModif": "2020-01-17T13:06:50.06",
	"!Region": "Кабардино-Балкарская Республика",
	"!DateReg": "2009-09-30T00:00:00"
}
*************************** 12. row ***************************
ArbitrManagerID: 22332
     DateDelete: NULL
  DateLastModif: 2020-02-26 09:22:01
      FirstName: Златослава
       LastName: Соловьева
     MiddleName: Владимировна
            INN: 472597475034
         RegNum: 
         OGRNIP: 
         RegNum: 
     SRORegDate: NULL
      SRORegNum: NULL
      id_Region: 8
           Body: {
	"!ArbitrManagerID": "22332",
	"!FirstName": "Златослава",
	"!MiddleName": "Владимировна",
	"!LastName": "Соловьева",
	"!RegNum": "",
	"!INN": "472597475034",
	"!OGRNIP": "",
	"!DateLastModif": "2020-02-26T09:22:01.34",
	"!Region": "г. Москва",
	"!DateReg": "2020-02-03T00:00:00"
}
