*************************** 1. row ***************************
             id_Message: 1
             BankruptId: 89823
        ArbitrManagerID: NULL
               efrsb_id: 1661762
MessageInfo_MessageType: f
                    INN: 3015012501
                  SNILS: NULL
                   OGRN: 1023000927520
               Revision: 1
            PublishDate: 2019-02-12 16:43:15
                   Body: <MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1661762</Id>
  <Number>1661762</Number>
  <CaseNumber>А06-10774/2015</CaseNumber>
  <PublisherInfo PublisherType="ForeignSystem">
    <ForeignSystem Id="4" Name="Государственная корпорация «Агентство по страхованию вкладов»" />
  </PublisherInfo>
  <MessageInfo MessageType="Other">
    <Other>
      <Text>&lt;p&gt;Иное + 2 файла&lt;/p&gt;</Text>
    </Other>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="CreditOrganization">
    <BankruptFirm Id="78932" InsolventCategoryName="Кредитная организация" FullName="ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО АГРОИНВЕСТИЦИОННЫЙ КОММЕРЧЕСКИЙ БАНК" ShortName="ПАО АГРОИНКОМБАНК" PostAddress="414000, г. Астрахань, пл. Ленина/ул. Бурова, 10/3, помещение 73" OKPO="" OGRN="1023000927520" LegalAddress="414000, г. Астрахань, пл. Ленина/ул. Бурова, 10/3, помещение 73">
      <INN>3015012501</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2019-02-12T16:43:15.273</PublishDate>
  <BankruptId>89823</BankruptId>
  <MessageGUID>F1F4F90A20916B2B8AB4464472A4A46A</MessageGUID>
  <MessageURLList>
    <MessageURL URLName="kniga2.xlsx" URL="http://test.fedresurs.ru/Download/file.fo?id=00000000-0000-0000-0000-000000000000&amp;type=MessageDocument" DownloadSize="9563" />
    <MessageURL URLName="yaycheyka.docx" URL="http://test.fedresurs.ru/Download/file.fo?id=00000000-0000-0000-0000-000000000000&amp;type=MessageDocument" DownloadSize="12921" />
  </MessageURLList>
</MessageData>
*************************** 2. row ***************************
             id_Message: 2
             BankruptId: 89823
        ArbitrManagerID: NULL
               efrsb_id: 1661763
MessageInfo_MessageType: r
                    INN: 3015012501
                  SNILS: NULL
                   OGRN: 1023000927520
               Revision: 2
            PublishDate: 2019-02-12 16:51:03
                   Body: <MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1661763</Id>
  <Number>1661763</Number>
  <CaseNumber>А06-10774/2015</CaseNumber>
  <PublisherInfo PublisherType="ForeignSystem">
    <ForeignSystem Id="4" Name="Государственная корпорация «Агентство по страхованию вкладов»" />
  </PublisherInfo>
  <MessageInfo MessageType="CommitteeResult">
    <Other>
      <Text>&lt;p&gt;Сообщение о результатах проведения комитета кредиторов + 2 ФАЙЛА&lt;/p&gt;</Text>
    </Other>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="CreditOrganization">
    <BankruptFirm Id="78932" InsolventCategoryName="Кредитная организация" FullName="ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО АГРОИНВЕСТИЦИОННЫЙ КОММЕРЧЕСКИЙ БАНК" ShortName="ПАО АГРОИНКОМБАНК" PostAddress="414000, г. Астрахань, пл. Ленина/ул. Бурова, 10/3, помещение 73" OKPO="" OGRN="1023000927520" LegalAddress="414000, г. Астрахань, пл. Ленина/ул. Бурова, 10/3, помещение 73">
      <INN>3015012501</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2019-02-12T16:51:03.257</PublishDate>
  <BankruptId>89823</BankruptId>
  <MessageGUID>1C71027345739279A3B4CCBA202A43C1</MessageGUID>
  <MessageURLList>
    <MessageURL URLName="kniga2.xlsx" URL="http://test.fedresurs.ru/Download/file.fo?id=00000000-0000-0000-0000-000000000000&amp;type=MessageDocument" DownloadSize="9563" />
    <MessageURL URLName="yaycheyka.docx" URL="http://test.fedresurs.ru/Download/file.fo?id=00000000-0000-0000-0000-000000000000&amp;type=MessageDocument" DownloadSize="12921" />
  </MessageURLList>
</MessageData>
*************************** 3. row ***************************
             id_Message: 8
             BankruptId: 138800
        ArbitrManagerID: 22297
               efrsb_id: 1661760
MessageInfo_MessageType: f
                    INN: 2777267292
                  SNILS: NULL
                   OGRN: 8077363037447
               Revision: 8
            PublishDate: 2019-02-11 12:51:56
                   Body: <MessageData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Id>1661760</Id>
  <Number>1661760</Number>
  <CaseNumber>А14-24246/2017 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="22297" FirstName="Иван" MiddleName="Петрович" LastName="Васильев" INN="110115686508" SNILS="11111111111" RegistryNumber="457674635345">
      <OGRN />
      <Sro SroId="1">
        <SroName>Ассоциация "Первая СРО АУ" - Ассоциация «Первая Саморегулируемая Организация Арбитражных Управляющих зарегистрированная в едином государственном реестре саморегулируемых организаций арбитражных управляющих»</SroName>
        <SroRegistryNumber>001-1</SroRegistryNumber>
        <OGRN>1025203032150</OGRN>
        <INN>5260111551</INN>
        <LegalAddress>109029, г. Москва, ул. Скотопрогонная, д. 29/1</LegalAddress>
      </Sro>
      <CorrespondenceAddress>484545646п</CorrespondenceAddress>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="Other">
    <Other>
      <Text>Проверка в06 сегодня.</Text>
    </Other>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="98901" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСВЕННОСТЬЮ &quot;ДЛЯ СВОИХ НУЖД&quot;" ShortName="ООО  &quot;ДЛЯ СВОИХ НУЖД&quot;" PostAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 " OKPO="" OGRN="8077363037447" LegalAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 ">
      <INN>2777267292</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2019-02-11T12:51:56.053</PublishDate>
  <BankruptId>138800</BankruptId>
  <MessageGUID>5CC3CAC8694B4DC8E0E48BB982664E87</MessageGUID>
</MessageData>
*************************** 4. row ***************************
             id_Message: 4
             BankruptId: 89823
        ArbitrManagerID: NULL
               efrsb_id: 1661761
MessageInfo_MessageType: f
                    INN: 3015012501
                  SNILS: NULL
                   OGRN: 1023000927520
               Revision: 4
            PublishDate: 2019-02-11 14:39:16
                   Body: <MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1661761</Id>
  <Number>1661761</Number>
  <CaseNumber>А06-10774/2015</CaseNumber>
  <PublisherInfo PublisherType="ForeignSystem">
    <ForeignSystem Id="4" Name="Государственная корпорация «Агентство по страхованию вкладов»" />
  </PublisherInfo>
  <MessageInfo MessageType="Other">
    <Other>
      <Text>&lt;p&gt;Тест с файлом&lt;/p&gt;</Text>
    </Other>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="CreditOrganization">
    <BankruptFirm Id="78932" InsolventCategoryName="Кредитная организация" FullName="ПУБЛИЧНОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО АГРОИНВЕСТИЦИОННЫЙ КОММЕРЧЕСКИЙ БАНК" ShortName="ПАО АГРОИНКОМБАНК" PostAddress="414000, г. Астрахань, пл. Ленина/ул. Бурова, 10/3, помещение 73" OKPO="" OGRN="1023000927520" LegalAddress="414000, г. Астрахань, пл. Ленина/ул. Бурова, 10/3, помещение 73">
      <INN>3015012501</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2019-02-11T14:39:16.147</PublishDate>
  <BankruptId>89823</BankruptId>
  <MessageGUID>8F30F0FC0172E55A406495971D984889</MessageGUID>
  <MessageURLList>
    <MessageURL URLName="yaycheyka.docx" URL="http://test.fedresurs.ru/Download/file.fo?id=00000000-0000-0000-0000-000000000000&amp;type=MessageDocument" DownloadSize="12921" />
  </MessageURLList>
</MessageData>
*************************** 5. row ***************************
             id_Message: 5
             BankruptId: 138800
        ArbitrManagerID: 22297
               efrsb_id: 1661758
MessageInfo_MessageType: f
                    INN: 2777267292
                  SNILS: NULL
                   OGRN: 8077363037447
               Revision: 5
            PublishDate: 2019-02-08 16:43:49
                   Body: <MessageData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Id>1661758</Id>
  <Number>1661758</Number>
  <CaseNumber>А14-22212/2018 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="22297" FirstName="Иван" MiddleName="Петрович" LastName="Васильев" INN="110115686508" SNILS="11111111111" RegistryNumber="457674635345">
      <OGRN />
      <Sro SroId="1">
        <SroName>Ассоциация "Первая СРО АУ" - Ассоциация «Первая Саморегулируемая Организация Арбитражных Управляющих зарегистрированная в едином государственном реестре саморегулируемых организаций арбитражных управляющих»</SroName>
        <SroRegistryNumber>001-1</SroRegistryNumber>
        <OGRN>1025203032150</OGRN>
        <INN>5260111551</INN>
        <LegalAddress>109029, г. Москва, ул. Скотопрогонная, д. 29/1</LegalAddress>
      </Sro>
      <CorrespondenceAddress>484545646п</CorrespondenceAddress>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="Other">
    <Other>
      <Text>Testing service</Text>
    </Other>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="98901" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСВЕННОСТЬЮ &quot;ДЛЯ СВОИХ НУЖД&quot;" ShortName="ООО  &quot;ДЛЯ СВОИХ НУЖД&quot;" PostAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 " OKPO="" OGRN="8077363037447" LegalAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 ">
      <INN>2777267292</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2019-02-08T16:43:49.233</PublishDate>
  <BankruptId>138800</BankruptId>
  <MessageGUID>47A484929709B1CB68F4A38A611B5DFD</MessageGUID>
</MessageData>
*************************** 6. row ***************************
             id_Message: 6
             BankruptId: 138800
        ArbitrManagerID: 22297
               efrsb_id: 1661759
MessageInfo_MessageType: f
                    INN: 2777267292
                  SNILS: NULL
                   OGRN: 8077363037447
               Revision: 6
            PublishDate: 2019-02-08 17:00:46
                   Body: <MessageData xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Id>1661759</Id>
  <Number>1661759</Number>
  <CaseNumber>А14-22212/2018 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="22297" FirstName="Иван" MiddleName="Петрович" LastName="Васильев" INN="110115686508" SNILS="11111111111" RegistryNumber="457674635345">
      <OGRN />
      <Sro SroId="1">
        <SroName>Ассоциация "Первая СРО АУ" - Ассоциация «Первая Саморегулируемая Организация Арбитражных Управляющих зарегистрированная в едином государственном реестре саморегулируемых организаций арбитражных управляющих»</SroName>
        <SroRegistryNumber>001-1</SroRegistryNumber>
        <OGRN>1025203032150</OGRN>
        <INN>5260111551</INN>
        <LegalAddress>109029, г. Москва, ул. Скотопрогонная, д. 29/1</LegalAddress>
      </Sro>
      <CorrespondenceAddress>484545646п</CorrespondenceAddress>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="Other">
    <Other>
      <Text>Test2 service</Text>
    </Other>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="98901" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСВЕННОСТЬЮ &quot;ДЛЯ СВОИХ НУЖД&quot;" ShortName="ООО  &quot;ДЛЯ СВОИХ НУЖД&quot;" PostAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 " OKPO="" OGRN="8077363037447" LegalAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 ">
      <INN>2777267292</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2019-02-08T17:00:46.22</PublishDate>
  <BankruptId>138800</BankruptId>
  <MessageGUID>AB9BDA8FF298635B7444E2BFE415231C</MessageGUID>
</MessageData>
*************************** 7. row ***************************
             id_Message: 7
             BankruptId: 138800
        ArbitrManagerID: 22297
               efrsb_id: 1661756
MessageInfo_MessageType: m
                    INN: 2777267292
                  SNILS: NULL
                   OGRN: 8077363037447
               Revision: 7
            PublishDate: 2019-02-04 14:43:17
                   Body: <MessageData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Id>1661756</Id>
  <Number>1661756</Number>
  <CaseNumber>А14-22212/2018 </CaseNumber>
  <PublisherInfo PublisherType="ArbitrManager">
    <ArbitrManager Id="22297" FirstName="Иван" MiddleName="Петрович" LastName="Васильев" INN="110115686508" SNILS="11111111111" RegistryNumber="457674635345">
      <OGRN />
      <Sro SroId="1">
        <SroName>Ассоциация "Первая СРО АУ" - Ассоциация «Первая Саморегулируемая Организация Арбитражных Управляющих зарегистрированная в едином государственном реестре саморегулируемых организаций арбитражных управляющих»</SroName>
        <SroRegistryNumber>001-1</SroRegistryNumber>
        <OGRN>1025203032150</OGRN>
        <INN>5260111551</INN>
        <LegalAddress>109029, г. Москва, ул. Скотопрогонная, д. 29/1</LegalAddress>
      </Sro>
      <CorrespondenceAddress>484545646п</CorrespondenceAddress>
    </ArbitrManager>
  </PublisherInfo>
  <MessageInfo MessageType="PropertyInventoryResult">
    <PropertyInventoryResult>
      <Text>Проверка входящих данных.</Text>
    </PropertyInventoryResult>
  </MessageInfo>
  <BankruptInfo BankruptType="Organization" BankruptCategory="SimpleOrganization">
    <BankruptFirm Id="98901" InsolventCategoryName="Обычная организация" FullName="ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСВЕННОСТЬЮ &quot;ДЛЯ СВОИХ НУЖД&quot;" ShortName="ООО  &quot;ДЛЯ СВОИХ НУЖД&quot;" PostAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 " OKPO="" OGRN="8077363037447" LegalAddress="394019, ВОРОНЕЖСКАЯ ОБЛ, ГОРОД ВОРОНЕЖ, УЛИЦА ЕРЕМЕЕВА, ДОМ 24 ">
      <INN>2777267292</INN>
    </BankruptFirm>
  </BankruptInfo>
  <PublishDate>2019-02-04T14:43:17.697</PublishDate>
  <BankruptId>138800</BankruptId>
  <MessageGUID>5D7D022C025A7A69FC7487E7EDFAB1FC</MessageGUID>
</MessageData>
