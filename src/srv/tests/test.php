<?

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once('../assets/libs/efrsb.php');
require_once('../assets/libs/Message.php');
require_once('../assets/libs/Register.php');
require_once('../assets/libs/VerificationOfProcedures.php');
require_once('../assets/libs/Manager.php');

$efrsb_api_params= $test_efrsb_api_params;

function test_GetMessageContent($id)
{
	$efrsb= new EfrsbClient();
	$result= $efrsb->GetMessageContent($id);
	print_r($result);
}

function test_GetDebtorByIdBankrupt($idBankrupt)
{
	$efrsb= new EfrsbClient();
	$result= $efrsb->GetDebtorByIdBankrupt($idBankrupt);
	print_r($result);
}

function test_GetMessageIds($startDate,$endDate)
{
	$efrsb= new EfrsbClient();
	$result= $efrsb->GetMessageIds($startDate,$endDate);
	print_r($result);
}

function test_GetDebtorsByLastPublicationPeriod($startDate,$endDate)
{
	$efrsb= new EfrsbClient();
	$result= $efrsb->GetDebtorsByLastPublicationPeriod($startDate,$endDate);
	print_r($result);
}

function test_GetArbitrManagerRegister($date)
{
	$efrsb= new EfrsbClient();
	$result= $efrsb->GetArbitrManagerRegister($date);
	print_r($result);
}

function test_GetSroRegister($date)
{
	$efrsb= new EfrsbClient();
	$result= $efrsb->GetSroRegister($date);
	print_r($result);
}

function test_GetDebtorRegister($date)
{
	$efrsb= new EfrsbClient();
	$result= $efrsb->GetDebtorRegister($date);
	print_r($result);
}

function test_parse_for_Message_table($fname)
{
	$msg= file_get_contents($fname);
	$pmsg= parse_for_Message_table($msg);
	print_r($pmsg);
}

function test_load_old()
{
	global $current_date_time;
	$current_date_time= date_create('2019-02-13');
	Load_old_messages();
}

function test_load_new($start_date)
{
	global $current_date_time;
	$current_date_time= date_create(($start_date!=null) ? $start_date : '2019-02-13');
	Load_new_messages();
}

function test_load_day($day)
{
	global $current_date_time;
	$current_date_time= date_create('2019-02-13');
	Load_old_messages_for_day($day);
}

function test_get($url)
{
	$url= str_replace(' ','%20',$url);
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
	{
		echo "HTTPCODE $httpcode";
	}
	else
	{
		echo $curl_response;
	}
}

function test_post($url,$post_data_filepath)
{
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	$post_data_json= file_get_contents($post_data_filepath);
	$post_data= json_decode($post_data_json);
	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));
	$curl_response = curl_exec($curl);
	$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);

	if (200!=$httpcode)
	{
		echo "HTTPCODE $httpcode";
	}
	else
	{
		echo $curl_response;
	}
}

function test_load_from_old_db()
{
	global $olddb_params;
	$olddb_params->from_date= '2019-02-02';
	$olddb_params->portion_size= 2;
	$olddb_params->max_portions= 2;

	global $current_date_time;
	$current_date_time= date_create('2019-02-13');

	LoadFromOldEfrsb();
}

function test_UpdateSRORegister($date_after)
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');
	if (null==$date_after)
	{
		UpdateSRORegister();
	}
	else
	{
		UpdateSRORegisterAfter(date_create($date_after));
	}
}

function test_UpdateSROFromEGRUL()
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');
	UpdateSROFromEGRUL();
}

function test_UpdateManagerRegister($date_after)
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');
	if (null==$date_after)
	{
		UpdateManagerRegister();
	}
	else
	{
		UpdateManagerRegisterAfter(date_create($date_after));
	}
}

function test_UpdateDebtorRegisterByLastPublicationPeriod($date_after)
{
	global $current_date_time, $load_debtor_interval;
	$current_date_time= date_create('2019-06-21');
	$load_debtor_interval= date_interval_create_from_date_string('30 days');
	if (null==$date_after)
	{
		UpdateDebtorRegisterByLastPublicationPeriod();
	}
	else
	{
		UpdateDebtorRegisterByLastPublicationPeriodAfter(date_create($date_after));
	}
}

function test_LoadSkippedDebtors()
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');
	LoadSkippedDebtors();
}

function test_LoadSkippedDebtorManager()
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');
	LoadSkippedDebtorManager();
}

function test_UpdateFieldsInExistingMessages()
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');

	UpdateFieldsInExistingMessages(null,2);
}

function test_UpdateArbitManagerIdInDebtor($date_after)
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');

	UpdateArbitrManagerIdInDebtor($date_after);
}

function test_UpdateDebtorManager($date_after)
{
	global $current_date_time;
	$current_date_time= date_create('2019-06-21');

	UpdateDebtorManager($date_after);
}

function test_MessageTypeDescriptions()
{
	global $MessageInfo_MessageType_desciptions;
	$MessageInfo_MessageType_desciptions_by_api_name= array();
	$MessageInfo_MessageType_desciptions_by_db_value= array();
	foreach ($MessageInfo_MessageType_desciptions as $d)
	{
		echo str_replace("\n","\r\n",print_r($d,true));
		$api_name= $d['api_name'];
		if (isset($MessageInfo_MessageType_desciptions_by_api_name[$api_name]))
			throw new Exception("duplicate api_name \"$api_name\"!");
		$MessageInfo_MessageType_desciptions_by_api_name[$api_name]= $d;

		$db_value= $d['db_value'];
		if (isset($MessageInfo_MessageType_desciptions_by_db_value[$db_value]))
			throw new Exception("duplicate db_value \"$db_value\"!");
		$MessageInfo_MessageType_desciptions_by_db_value[$db_value]= $d;
	}
}

function test_ParseManagerContacts()
{
	global $current_date_time;
	$current_date_time= date_create('2019-07-23');

	ParseManagerContacts();
}


function Process_command_line_arguments()
{
	global $argv;
	switch ($argv[1])
	{
		case 'GetMessageContent': test_GetMessageContent($argv[2]); break;
		case 'GetMessageIds': test_GetMessageIds($argv[2],3==count($argv)?null:$argv[3]); break;
		case 'GetArbitrManagerRegister': test_GetArbitrManagerRegister(2==count($argv)?null:$argv[2]); break;
		case 'GetDebtorRegister': test_GetDebtorRegister(2==count($argv)?null:$argv[2]); break;
		case 'GetDebtorByIdBankrupt': test_GetDebtorByIdBankrupt($argv[2]); break;
		case 'GetDebtorsByLastPublicationPeriod': test_GetDebtorsByLastPublicationPeriod($argv[2],3==count($argv)?null:$argv[3]); break;

		case 'load_old': test_load_old(); break;
		case 'load_old_day': test_load_day($argv[2]); break;
		case 'parse_for_Message_table': test_parse_for_Message_table($argv[2]); break;

		case 'load_new': test_load_new(2==count($argv)?null:$argv[2]); break;

		case 'test_get': test_get($argv[2]); break;
		case 'test_post': test_post($argv[2],$argv[3]); break;

		case 'load_from_old_db': test_load_from_old_db(); break;

		case 'UpdateSRORegister': test_UpdateSRORegister(2==count($argv)?null:$argv[2]); break;
		case 'UpdateSROFromEGRUL': test_UpdateSROFromEGRUL(); break;
		case 'UpdateManagerRegister' : test_UpdateManagerRegister(2==count($argv)?null:$argv[2]); break;
		case 'LoadSkippedDebtors' : test_LoadSkippedDebtors(); break;
		case 'UpdateDebtorRegister' : test_UpdateDebtorRegisterByLastPublicationPeriod(2==count($argv)?null:$argv[2]); break;
		case 'UpdateFieldsInExistingMessages' : test_UpdateFieldsInExistingMessages(); break;

		case 'UpdateDebtorManager' : test_UpdateDebtorManager(2==count($argv)?null:$argv[2]); break;

		case 'MessageTypeDescriptions' : test_MessageTypeDescriptions(); break;

		case 'LoadSkippedDebtorManager' : test_LoadSkippedDebtorManager(); break;

		case 'ParseManagerContacts': test_ParseManagerContacts(); break;
	}
}

try
{
	Process_command_line_arguments();
}
catch (nusoapException $ex)
{
	$nusoap_err= $ex->nusoap_err;
	echo "nusoap error $nusoap_err\r\n";
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	echo "Unhandled exception occurred: $exception_class - $exception_Message\r\n";
	print_r($ex);
}
catch (Exception $ex)
{
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	echo "Unhandled exception occurred: $exception_class - $exception_Message\r\n";
	echo 'catched Exception:';
	print_r($ex);
}