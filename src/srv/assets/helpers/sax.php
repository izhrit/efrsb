<?

class XMLParser
{
	//function tag_open($xml_parser, $name, $attributes) {}
	//function cdata($xml_parser, $cdata) {}
	//function tag_close($xml_parser, $name) {}

	function bind($xml_parser)
	{
		xml_set_object($xml_parser, $this);
		xml_set_element_handler($xml_parser, "tag_open", "tag_close");
		xml_set_character_data_handler($xml_parser, "cdata");
	}
}

class XMLParser_echor extends XMLParser
{
	private $depth= 0;
	function tag_open($xml_parser, $name, $attributes, $depth)
	{
		echo "$depth-".$name."\r\n";
	}
	function cdata($xml_parser, $cdata)
	{
		print_r($cdata);
	}
}

class Field_parser extends XMLParser
{
	private $depth= 0;
	private $index_in_path= null;
	public $value= null;

	function __construct($path)
	{
		$this->path= $path;
	}

	function tag_open($xml_parser, $name, $attributes, $depth)
	{
		$path_length= count($this->path);
		if ($depth<$path_length && $name==mb_strtoupper($this->path[$depth]))
		{
			if (null===$this->index_in_path)
			{
				if (0==$depth)
					$this->index_in_path= 0;
			}
			else if ($depth==$this->index_in_path+1)
			{
				$this->index_in_path++;
			}
		}
		if ($depth==$this->index_in_path && $this->index_in_path==$path_length-2)
		{
			$last_name= $this->path[$path_length-1];
			if ('@'==substr($last_name,0,1))
			{
				$attrname= substr($last_name,1);
				if (isset($attributes[$attrname]))
					$this->value= $attributes[$attrname];
			}
		}
	}

	function cdata($xml_parser, $cdata, $depth) 
	{
		if ($this->index_in_path==count($this->path)-1)
		{
			$this->value= $cdata;
		}
	}

	function tag_close($xml_parser, $name, $depth)
	{
		if ($depth==$this->index_in_path+1)
			$this->index_in_path--;
	}
}

class XMLParserArray extends XMLParser
{
	private $depth= 0;
	function __construct($parsers)
	{
		$this->depth= 0;
		$this->parsers= $parsers;
	}

	function tag_open($xml_parser, $name, $attributes) 
	{
		foreach ($this->parsers as $parser)
		{
			$parser->tag_open($xml_parser, $name, $attributes, $this->depth);
		}
		$this->depth++;
	}

	function cdata($xml_parser, $cdata) 
	{
		foreach ($this->parsers as $field_parser)
		{
			$field_parser->cdata($xml_parser, $cdata, $this->depth);
		}
	}

	function tag_close($xml_parser, $name) 
	{
		foreach ($this->parsers as $field_parser)
		{
			$field_parser->tag_close($xml_parser, $name, $this->depth);
		}
		$this->depth--;
	}
}
