<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';
require_once '../assets/libs/efrsb.php';

$txt_query_select_debtor_data= "
		select 
			  d.BankruptId
			, d.Name
			, uncompress(d.Body) as body_Debtor
			, uncompress(m.Body) as body_Manager
			, s.Name sro_Name
			, s.INN sro_INN
			, s.RegNum sro_RegNum
		from debtor d
		inner join manager m on m.ArbitrManagerID=d.ArbitrManagerID
";

function prepareDebtorData($row)
{
	$data= (object)array(
		'Должник'=> json_decode($row->body_Debtor)
		,'АУ'=> json_decode($row->body_Manager)
	);
	if (null!=$row->sro_Name)
	{
		$data->SRO= array(
			'Name'=>$row->sro_Name
			,'INN'=>$row->sro_INN
			,'RegNum'=>$row->sro_RegNum
		);
	}
	return $data;
}