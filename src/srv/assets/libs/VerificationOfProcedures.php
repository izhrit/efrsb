<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/sax.php';

class VerificationOfProcedures
{
	public function CheckProcedures($request)
	{
		$managers= $request["Managers"];

		$manager_names= self::GetManagerNamesFromRequest($managers);
		$manager_efrsb_numbers= self::GetManagerRegnumsFromRequest($managers);

		$procedures_answers= array();
		foreach ($request["Procedures"] as $p)
		{
			$debtor= self::GetDebtorByInnOrOGRN(trim($p["deptor_inn"]), trim($p["deptor_ogrn"]), trim($p["DealNumber"]));

			if (!empty($debtor) && !empty($debtor->ManagerName))
			{
				$debtor->FixedManagerName= str_replace('ё', 'е', $debtor->ManagerName);
				if (!in_array($debtor->FixedManagerName, $manager_names, false) &&
					!in_array($debtor->ManagerRegNum, $manager_efrsb_numbers, false))
				{
					$procedures_answers[]= array
					(
						'contract_number'  => $p["contract_number"]
						,'id_Procedure'    => $p["id_Procedure"]
						,'deal_number'     => $p["DealNumber"]
						,'au_server'       => $p["manager_name"]
						,'au_message'      => $debtor->ManagerName
						,'message_efrsbId' => $debtor->efrsb_id
						,'status'          => "Warning"
					);
				}
			}
			
		}
		return $procedures_answers;
	}

	static private function GetManagerNamesFromRequest($managers)
	{
		$manager_names= array();
		foreach ($managers as $manager)
			$manager_names[] = preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $manager["Name"]);
		return $manager_names;
	}

	static private function GetManagerRegnumsFromRequest($managers)
	{
		$managers_efrsb_numbers= array();
		foreach ($managers as $manager)
			$managers_efrsb_numbers[] = preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $manager["EFRSBNumber"]);
		return $managers_efrsb_numbers;
	}

	static function GetDebtorByInnOrOGRN($INN, $OGRN, $DealNumber)
	{
		if (empty($INN) && empty($OGRN))
		{
			return null;
		}
		else
		{
			$txt_query= "SELECT
				D.BankruptId,
				D.Category,
				D.DateLastModif,
				D.INN,
				D.OGRN,
				D.SNILS,
				D.ArbitrManagerID,
				D.LastMessageDate,
				D.LastReportDate,
				D.Name,

				TRIM(concat(TRIM(M.LastName), ' ',TRIM(M.FirstName) , ' ', TRIM(M.MiddleName))) AS ManagerName,
				M.RegNum as ManagerRegNum,

				MSG.efrsb_id,
				uncompress(MSG.Body) Body_Msg,
				MSG.PublishDate
			FROM message MSG
			LEFT JOIN debtor D ON MSG.BankruptId = D.BankruptId
			LEFT JOIN manager M ON MSG.ArbitrManagerID=M.ArbitrManagerID
			WHERE MSG.INN=? OR MSG.OGRN=? AND BINARY MSG.MessageInfo_MessageType <> 'b' 
			ORDER BY MSG.PublishDate DESC 
			LIMIT 1";
			$debtors = execute_query($txt_query, array('ss', $INN, $OGRN));
			if (empty($debtors))
			{
				return null;
			}
			else
			{
				$debtor= $debtors[0];
				$CaseNumber_Msg = self::parse_CaseNumber($debtor->Body_Msg);
				return (trim($DealNumber) != trim($CaseNumber_Msg)) ? null : $debtor;
			}
		}
	}

	static function parse_CaseNumber($msg)
	{
		$CaseNumber_parser = new Field_parser(array('MessageData', 'CaseNumber'));

		$msg_parser = new XMLParserArray(array($CaseNumber_parser));

		$xml_parser = xml_parser_create();
		$msg_parser->bind($xml_parser);
		if (!xml_parse($xml_parser, $msg, /* is_final= */
					   TRUE)) {
			$ex_text = sprintf("Ошибка XML: %s на строке %d",
							   xml_error_string(xml_get_error_code($xml_parser)),
							   xml_get_current_line_number($xml_parser));
			xml_parser_free($xml_parser);
			throw new Exception($ex_text);
		}
		xml_parser_free($xml_parser);

		$CaseNumber = str_replace("№", "", $CaseNumber_parser->value);

		return $CaseNumber;
	}
}