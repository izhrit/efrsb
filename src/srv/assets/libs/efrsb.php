<?php

require_once('../assets/nusoap/lib/nusoap.php');

class nusoapException extends Exception
{
	public $nusoap_err= null;

	public function __construct($string_ex, $nusoap_err)
	{
		parent::__construct($string_ex);
		$this->nusoap_err= $nusoap_err;
	}
}

function compare_LegalCaseInfo($a,$b)
{
	return $a['Number'] < $b['Number'] ? -1 : ($a['Number'] == $b['Number'] ? 0 : 1);
}

function compare_by_DateLastModif($a,$b)
{
	$da= date_create($a['!DateLastModif']);
	$db= date_create($b['!DateLastModif']);
	return ($da < $db) ? -1 : (($da == $db) ? 0 : 1);
}

function debtor_LastPublishDateTime($a)
{
	$aLastReportDate= date_create((!isset($a['LastReportDate']) || null==$a['LastReportDate'] || ''==$a['LastReportDate']) ? '2001-01-01' : $a['LastReportDate']);
	$aLastMessageDate= date_create((!isset($a['LastMessageDate']) || null==$a['LastMessageDate'] || ''==$a['LastMessageDate']) ? '2001-01-01' : $a['LastMessageDate']);
	return ($aLastReportDate>$aLastMessageDate) ? $aLastReportDate : $aLastMessageDate;
}

function compare_DebtorPublishDateTime($a,$b)
{
	$da= $a['LastPublishDateTime'];
	$db= $b['LastPublishDateTime'];
	return ($da < $db) ? -1 : (($da == $db) ? 0 : 1);
}

class EfrsbClient
{
	private $client= null;

	public function __construct($options= null)
	{
		global $efrsb_api_params;
		$this->client = new nusoap_client($efrsb_api_params->url, 'wsdl');
		$this->client->soap_defencoding = 'UTF-8';
		$this->client->decode_utf8 = FALSE;
		$this->SafeThrowExceptionAfterClientAction('construct');

		$login= $efrsb_api_params->login;
		$password= $efrsb_api_params->password;
		$auth_method= $efrsb_api_params->auth_method;
		
		$password_to_log= str_pad('',strlen($password),'?');
		$this->client->setCredentials($login, $password, $auth_method);
		$this->SafeThrowExceptionAfterClientAction("setCredentials($login,,$auth_method)");

		$this->client->loadWSDL();
		$this->SafeThrowExceptionAfterClientAction("loadWSDL()");
	}

	private function SafeThrowExceptionAfterClientAction($action_name)
	{
		$err= $this->client->getError();
		if ($err) 
			throw new nusoapException("EfrsbClient error after $action_name",$err);
	}

	public function GetMessageContent($id)
	{
		$result= $this->client->call('GetMessageContent', array('id'=>$id));
		$this->SafeThrowExceptionAfterClientAction("GetMessageContent($id)");
		return $result['GetMessageContentResult'];
	}

	static function FixDebtor(&$debtor)
	{
		if (isset($debtor['LegalCaseList']['LegalCaseInfo']))
		{
			$LegalCaseInfo_array= $debtor['LegalCaseList']['LegalCaseInfo'];
			if (isset($LegalCaseInfo_array['Number']))
				$debtor['LegalCaseList']['LegalCaseInfo']= $LegalCaseInfo_array= array($LegalCaseInfo_array);
			usort($LegalCaseInfo_array,'compare_LegalCaseInfo');
			$debtor['LegalCaseList']['LegalCaseInfo']= $LegalCaseInfo_array;
		}
		$debtor['LastPublishDateTime']= debtor_LastPublishDateTime($debtor);
		if (!isset($debtor['DebtorType']))
		{
			$debtor['DebtorType']= isset($debtor['!LastName'])
				? 'DebtorPerson' : 'DebtorCompany';
		}
			
	}

	public function GetDebtorByIdBankrupt($idBankrupt)
	{
		$result= $this->client->call('GetDebtorByIdBankrupt', array('idBankrupt'=>$idBankrupt));
		$this->SafeThrowExceptionAfterClientAction("GetDebtorByIdBankrupt($idBankrupt)");
		$debtor= $result['GetDebtorByIdBankruptResult'];
		self::FixDebtor($debtor);
		return $debtor;
	}

	function xsi_date_tomorrow()
	{
		$date = date_create();
		date_add($date, date_interval_create_from_date_string('10 days'));
		$res= date_format($date, 'Y-m-d');
		return $res;
	}

	public function GetMessageIds($startDate,$endDate)
	{
		$fixedEndDate= (null!=$endDate ? $endDate : $this->xsi_date_tomorrow());
		$params= array('startDate'=>$startDate,'endDate'=>$fixedEndDate);
		$result= $this->client->call('GetMessageIds', $params);
		$this->SafeThrowExceptionAfterClientAction("GetMessageIds($startDate,$fixedEndDate)");
		$arr= $result['GetMessageIdsResult'];
		return is_array($arr) ? $arr['int'] : array();
	}

	static function FixDebtorList(&$debtor_list)
	{
		$result= array();
		if (isset($debtor_list['DebtorCompany']))
		{
			$debtor_array= $debtor_list['DebtorCompany'];
			if (isset($debtor_array['LastMessageDate']))
				$debtor_array= array($debtor_array);
			foreach ($debtor_array as $debtor)
			{
				$debtor['DebtorType']= 'DebtorCompany';
				self::FixDebtor($debtor);
				$result[]= $debtor;
			}
		}
		if (isset($debtor_list['DebtorPerson']))
		{
			$debtor_array= $debtor_list['DebtorPerson'];
			if (isset($debtor_array['LastMessageDate']))
				$debtor_array= array($debtor_array);
			foreach ($debtor_array as $debtor)
			{
				$debtor['DebtorType']= 'DebtorPerson';
				self::FixDebtor($debtor);
				$result[]= $debtor;
			}
		}
		return $result;
	}

	public function GetDebtorsByLastPublicationPeriod($startDate,$endDate)
	{
		$params= array('startDate'=>$startDate,'endDate'=>(null!=$endDate ? $endDate : $this->xsi_date_tomorrow()));
		$result= $this->client->call('GetDebtorsByLastPublicationPeriod', $params);
		$this->SafeThrowExceptionAfterClientAction("GetDebtorsByLastPublicationPeriod($startDate,$endDate)");
		$result= $result['GetDebtorsByLastPublicationPeriodResult'];
		$result= self::FixDebtorList($result['DebtorList']);
		usort($result,'compare_DebtorPublishDateTime');
		return $result;
	}

	public function GetDebtorRegister($date)
	{
		$params= array('date'=>$date);
		$result= $this->client->call('GetDebtorRegister', $params);
		$this->SafeThrowExceptionAfterClientAction("GetDebtorRegister($date)");
		$result= $result['GetDebtorRegisterResult'];
		$result= self::FixDebtorList($result['DebtorList']);
		usort($result,'compare_by_DateLastModif');
		return $result;
	}

	public function GetArbitrManagerRegister($date)
	{
		$params= array('date'=>$date);
		$result= $this->client->call('GetArbitrManagerRegister', $params);
		$this->SafeThrowExceptionAfterClientAction("GetArbitrManagerRegister($date)");
		$result= $result['GetArbitrManagerRegisterResult'];

		if (!isset($result['AMList']['ArbitrManager']))
		{
			$result['AMList']['ArbitrManager']= array();
		}
		else
		{
			$manager_array= $result['AMList']['ArbitrManager'];
			if (isset($manager_array["!DateLastModif"]))
				$result['AMList']['ArbitrManager']= $manager_array= array($manager_array);
			usort($manager_array,'compare_by_DateLastModif');
			$result['AMList']['ArbitrManager']= $manager_array;
		}

		return $result;
	}

	public function GetSroRegister($date)
	{
		$params= array('date'=>$date);
		$result= $this->client->call('GetSroRegister', $params);
		$this->SafeThrowExceptionAfterClientAction("GetSroRegister($date)");
		$result= $result['GetSroRegisterResult'];

		if (!isset($result['SROList']['SRO']))
		{
			$result['SROList']['SRO']= array();
		}
		else
		{
			$sro_array= $result['SROList']['SRO'];
			if (isset($sro_array['!DateLastModif']))
				$result['SROList']['SRO']= $sro_array= array($sro_array);
			usort($sro_array,'compare_by_DateLastModif');
			$result['SROList']['SRO']= $sro_array;
		}

		return $result;
	}
}

$efrsb_client_count_using= 0;
$efrsb_client= null;

function SafePrepareEfrbClient()
{
	global $efrsb_client, $efrsb_client_count_using, $efrsb_api_params;
	if (null==$efrsb_client || $efrsb_client_count_using>$efrsb_api_params->recreate_client_count)
	{
		unset($efrsb_client);
		$efrsb_client= new EfrsbClient();
		$efrsb_client_count_using= 0;
	}
	$efrsb_client_count_using++;
	return $efrsb_client;
}
