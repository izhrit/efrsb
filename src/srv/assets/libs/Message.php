<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';
require_once '../assets/libs/efrsb.php';

require_once '../assets/libs/MessageType.php';

$current_date_time= null;
function get_current_datetime()
{
	global $current_date_time;
	if (null==$current_date_time)
	{
		return date_format(date_create(), 'Y-m-d\TH:i:s');
	}
	else
	{
		$current_date_time= date_add($current_date_time, date_interval_create_from_date_string('1 second'));
		return date_format($current_date_time, 'Y-m-d\TH:i:s');
	}
}

function parse_for_Message_table($msg)
{
	global $MessageInfo_MessageType_desciptions_by_api_name;

	$PublishDate_parser= new Field_parser(array('MessageData','PublishDate'));
	$Person_INN_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptPerson','INN'));
	$Firm_INN_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptFirm','INN'));
	$OGRN_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptFirm','@OGRN'));
	$SNILS_parser= new Field_parser(array('MessageData','BankruptInfo','BankruptPerson','SNILS'));
	$Id_parser= new Field_parser(array('MessageData','Id'));
	$ArbitrManagerID_parser= new Field_parser(array('MessageData','PublisherInfo','ArbitrManager','@ID'));
	$BankruptId_parser= new Field_parser(array('MessageData','BankruptId'));
	$BankruptType_parser= new Field_parser(array('MessageData','BankruptInfo','@BANKRUPTTYPE'));
	$MessageInfo_MessageType_parser= new Field_parser(array('MessageData','MessageInfo','@MESSAGETYPE'));
	$Number_parser= new Field_parser(array('MessageData','Number'));
	$MessageGUID_parser= new Field_parser(array('MessageData','MessageGUID'));

	$msg_parser = new XMLParserArray(array(
		$PublishDate_parser
		,$Person_INN_parser
		,$Firm_INN_parser
		,$OGRN_parser
		,$SNILS_parser
		,$Id_parser
		,$ArbitrManagerID_parser
		,$BankruptId_parser
		,$BankruptType_parser
		,$MessageInfo_MessageType_parser
		,$Number_parser
		,$MessageGUID_parser
		//,new XMLParser_echor()
	));

	$xml_parser = xml_parser_create();
	$msg_parser->bind($xml_parser);
	if (!xml_parse($xml_parser, $msg, /* is_final= */TRUE))
	{
		$ex_text= sprintf("Ошибка XML: %s на строке %d",
						xml_error_string(xml_get_error_code($xml_parser)),
						xml_get_current_line_number($xml_parser));
		xml_parser_free($xml_parser);
		throw new Exception($ex_text);
	}
	xml_parser_free($xml_parser);

	$parsed_msg= (object)array();
	$parsed_msg->INN= null==$Person_INN_parser->value 
		? $Firm_INN_parser->value : $Person_INN_parser->value;
	$parsed_msg->OGRN= $OGRN_parser->value;
	$parsed_msg->SNILS= $SNILS_parser->value;
	$parsed_msg->PublishDate= $PublishDate_parser->value;
	$parsed_msg->Id= $Id_parser->value;
	$parsed_msg->ArbitrManagerID= $ArbitrManagerID_parser->value;
	$parsed_msg->BankruptType= $BankruptType_parser->value;
	$parsed_msg->MessageInfo_MessageType= $MessageInfo_MessageType_parser->value;
	$parsed_msg->BankruptId= $BankruptId_parser->value;
	$parsed_msg->Number= $Number_parser->value;
	$parsed_msg->MessageGUID= $MessageGUID_parser->value;

	return $parsed_msg;
}

$count_messages_inserted= 0;
function Add_downloaded_message($msg,$id)
{
	global $MessageInfo_MessageType_desciptions_by_api_name;

	$pmsg= parse_for_Message_table($msg);
	if (!isset($id) || null==$id)
		$id= $pmsg->Id;

	$t= $pmsg->MessageInfo_MessageType;
	$t= (!isset($MessageInfo_MessageType_desciptions_by_api_name[$t])) ? 'H' : $MessageInfo_MessageType_desciptions_by_api_name[$t]['db_value'];

	$txt_query= "call Add_downloaded_message(?,?,?,?,?,?,?,?,?,?,?)";
	try
	{
		global $count_messages_inserted;
		execute_query_no_result($txt_query,array('sssssssssss',
			$pmsg->INN,$pmsg->SNILS,$pmsg->OGRN,$pmsg->PublishDate,$msg,$id,$pmsg->ArbitrManagerID,$pmsg->BankruptId,$t,$pmsg->Number,$pmsg->MessageGUID));
		$count_messages_inserted++;
	}
	catch (Exception $ex)
	{
		write_to_log($pmsg);
		throw $ex;
	}
}

function UpdateFieldsInExistingMessages($somedatetime= null, $portion_to_read_size= 1000)
{
	$where= 'WHERE Number IS NULL';

	echo get_current_datetime(). " update messages $where\r\n";

	$rows = execute_query("SELECT count(*) AS c FROM message $where", array());
	$messages_to_update= $rows[0]->c;
	echo get_current_datetime() . " $messages_to_update messages to update..\r\n";

	if (0!=$messages_to_update)
	{
		$txt_query= "SELECT id_Message, uncompress(Body) as Body, efrsb_id 
					FROM message $where ORDER BY PublishDate desc LIMIT $portion_to_read_size";

		$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('4 hours'));

		for ($messages = execute_query($txt_query,array()), $count= count($messages); 
			0!=$count && date_create() < $time_start_to_stop; 
			$messages = execute_query($txt_query,array()), $count= count($messages))
		{
			echo get_current_datetime() . " read $count messages to update\r\n";
			echo get_current_datetime() . " updated:";

			foreach ($messages as $m)
			{
				Add_downloaded_message($m->Body, $m->efrsb_id);
				echo ' '.$m->id_Message;
			}

			echo "\r\n";
		}
	}

	echo get_current_datetime(). " stop for today\r\n";
}

function prep_ids_downloaded(&$rows)
{
	$ids_downloaded= array();
	foreach ($rows as $row)
	{
		$efrsb_id= $row->efrsb_id;
		$ids_downloaded[$efrsb_id]= $efrsb_id;
	}
	return $ids_downloaded;
}

function load_Messages_with_ids(&$ids_to_download,&$ids_downloaded)
{
	global $count_messages_inserted, $job_params;
	$efrsb_client= SafePrepareEfrbClient();
	$ids_to_download= is_array($ids_to_download) ? $ids_to_download : array($ids_to_download);
	echo get_current_datetime().'    '.count($ids_to_download)." messages to download\r\n";
	echo "        download messages ";
	foreach ($ids_to_download as $id) // iv.	Для всех id из ids_to_download
	{
		if ($count_messages_inserted>=$job_params->max_download_message)
		{
			echo "\r\n".get_current_datetime()."loaded $count_messages_inserted messages (max count)!\r\n";
			break;
		}
		if (!isset($ids_downloaded[$id])) // 1.	Если id нет в ids_downloaded
		{
			echo "$id ";
			$msg= $efrsb_client->GetMessageContent($id); // a.	Загрузить сообщение id в msg
			Add_downloaded_message($msg,$id);            // b.	Записать msg в таблицу
			$ids_downloaded[$id]= $id;                   // c.	Добавить Id в ids_downloaded
		}
	}
	echo "\r\n";
}

$day_interval= date_interval_create_from_date_string('1 day');

function Load_old_messages_for_day($loopdate,$i= null)
{
	global $day_interval, $count_messages_inserted;
	$sloopdate= is_string($loopdate) ? $loopdate : date_format($loopdate, 'Y-m-d');
	$startDate= date_format(date_sub(date_create($sloopdate),$day_interval), 'Y-m-d').'T23:59:00';
	$endDate= date_format(date_add(date_create($sloopdate),$day_interval), 'Y-m-d').'T00:01:00';
	$i= (null==$i) ? 0 : $i;

	// a.	Если 0 == select count(*) from Downloaded_day where DownloadedDate=циклодень
	$txt_query= "select count(*) c from downloaded_day where DownloadedDate=date(?);";
	$rows= execute_query($txt_query,array('s',$sloopdate));

	$count_messages= $rows[0]->c;
	if (0==$count_messages)
	{
		echo get_current_datetime()." loading $sloopdate - $i ($count_messages_inserted already loaded)\r\n";

		// i.	ids_downloaded= select efrsb_id from Message where date(PublishDate)=день
		$txt_query= "select efrsb_id from message where PublishDate between ? and ?;";
		$rows= execute_query($txt_query,array('ss',$startDate,$endDate));
		echo get_current_datetime().'    '.count($rows)." messages in the database\r\n";

		// ii.	Сложить ids_downloaded в  Hashtable (чтобы поиск был не линейный, а логарифмический)
		$ids_downloaded= prep_ids_downloaded($rows);

		// iii.	Запросить с ефрсб список id сообщений опубликованных в циклодень с перехлёстом в обе стороны и сложить их в ids_to_download
		$efrsb_client= SafePrepareEfrbClient();
		$ids_to_download= $efrsb_client->GetMessageIds($startDate,$endDate);

		load_Messages_with_ids($ids_to_download,$ids_downloaded);

		// v.	Записать циклодень в Downloaded_day
		$txt_query= 'insert into downloaded_day(DownloadedDate)values(?);';
		execute_query_no_result($txt_query,array('s',$sloopdate));
	}
}

$seconds_sleep_after_nusoap_error= 60;
$seconds_sleep_log= 10;

function sleep_aftter_nusoap_error()
{
	global $seconds_sleep_after_nusoap_error, $seconds_sleep_log;
	if ($seconds_sleep_after_nusoap_error<=0 || $seconds_sleep_after_nusoap_error>3600)
		$seconds_sleep_after_nusoap_error= 60;
	if ($seconds_sleep_log<=0 || $seconds_sleep_log>3600)
		$seconds_sleep_log= 10;
	for ($s= 0; $s<$seconds_sleep_after_nusoap_error; $s+=$seconds_sleep_log)
	{
		echo "let's sleep $seconds_sleep_log seconds..\r\n";
		sleep($seconds_sleep_log);
	}
}

function Load_old_messages($final_date=null/* date_create('2008-01-01') */)
{
	$today= date_create();
	global $day_interval, $efrsb_api_params, $count_messages_inserted, $job_params;

	$start_date= null==$final_date ? date_create('2019-02-12') : date_sub($today, $day_interval);
	//$final_date= date_create('2008-01-01');
	$final_date= date_create(null!=$final_date ? $final_date : '2019-01-31');

	echo "load old messages\r\n";
	echo ' after  '.date_format($final_date, 'Y-m-d\TH:i:s')."\r\n";
	echo ' before '.date_format($start_date, 'Y-m-d\TH:i:s')."\r\n";
	echo "from efrsb\r\n";
	echo ' url= "'.$efrsb_api_params->url."\"\r\n";
	echo ' login= "'.$efrsb_api_params->login."\"\r\n";
	echo ' password= "'.str_pad('',strlen($efrsb_api_params->password),'?')."\"\r\n";
	echo get_current_datetime()." start loading old messages\r\n";

	$final_date_timestamp= date_timestamp_get($final_date);

	$i= 0;
	// 1)	Для каждого циклодня начиная со вчерашнего и до 1 января 2008
	for ($loopdate= $start_date; 
		date_timestamp_get($loopdate)>=$final_date_timestamp; 
		$loopdate= date_sub($loopdate, $day_interval))
	{
		if ($count_messages_inserted>=$job_params->max_download_message)
		{
			echo "\r\n".get_current_datetime()."loaded $count_messages_inserted messages (max count)!\r\n";
			break;
		}
		$i++;
		try
		{
			Load_old_messages_for_day($loopdate,$i);
		}
		catch (nusoapException $ex)
		{
			$nusoap_err= $ex->nusoap_err;
			echo "nusoap $nusoap_err\r\n";
			echo 'exception : ' . get_class($ex) . ' - ' . $ex->getMessage()."\r\n";
			sleep_aftter_nusoap_error();
		}
	}

	echo get_current_datetime()." finish loading old messages\r\n";
}

function Load_new_messages($final_date=null/* date_create('2008-01-01') */)
{
	global $efrsb_api_params;
	echo "load new messages\r\n";
	echo "from efrsb\r\n";
	echo ' url= "'.$efrsb_api_params->url."\"\r\n";
	echo ' login= "'.$efrsb_api_params->login."\"\r\n";
	echo ' password= "'.str_pad('',strlen($efrsb_api_params->password),'?')."\"\r\n";
	echo get_current_datetime()." start loading new messages\r\n";

	// 1)	PublishDate_max_downloaded= select max(PublishDate) from Message;
	$txt_query= "select max(PublishDate) max_PublishDate from message;";
	$rows= execute_query($txt_query,array());
	$PublishDate_max_downloaded= $rows[0]->max_PublishDate;
	echo get_current_datetime().' max(PublishDate)='.$PublishDate_max_downloaded."\r\n";
	if (null==$PublishDate_max_downloaded)
		$PublishDate_max_downloaded= date_format(null!=$final_date ? $final_date : date_create('2020-01-15'),'Y-m-d\TH:i:s');

	// 2)	PublishDate_to_read= PublishDate_max_downloaded –  %глубина_перехлёста% минут;
	$PublishDate_to_read= date_sub(date_create($PublishDate_max_downloaded), date_interval_create_from_date_string('1 minute'));
	$PublishDate_to_read= date_format($PublishDate_to_read, 'Y-m-d\TH:i:s');
	echo get_current_datetime().'       check from '.$PublishDate_to_read."\r\n";

	// 3)	ids_downloaded= select efrsb_id from Message where PublishDate > PublishDate_to_read;
	$txt_query= "select efrsb_id from message where PublishDate > ?;";
	$rows= execute_query($txt_query,array('s',$PublishDate_to_read));
	echo get_current_datetime().'     '.count($rows)." messages in the database\r\n";

	// 4)	Сложить ids_downloaded в  Hashtable (чтобы поиск был не линейный, а логарифмический)
	$ids_downloaded= prep_ids_downloaded($rows);

	// 5)	Запросить с ефрсб список id сообщений, начиная с PublishDate_to_read и сложить их в ids_to_download
	$efrsb_client= SafePrepareEfrbClient();
	$ids_to_download= $efrsb_client->GetMessageIds($PublishDate_to_read,null);

	load_Messages_with_ids($ids_to_download,$ids_downloaded);

	echo get_current_datetime()." finish loading new messages\r\n";
}

function LoadFromOldEfrsb()
{
	global $olddb_params;
	$portion_size= $olddb_params->portion_size;
	$max_portions= $olddb_params->max_portions;

	$i= 0;
	do
	{
		echo "--------------------------------------------------------------------------------\r\n";
		echo get_current_datetime()." select min_publishdate from efrsb.messages..\r\n";
		$txt_query= "select date(min(PublishDate)) min_PublishDate, count(*) c from message;";
		$rows= execute_query($txt_query,array());
		$from_date= $rows[0]->min_PublishDate;
		$count_date= $rows[0]->c;
		echo get_current_datetime()." ..selected $from_date from $count_date.\r\n";
		if (null==$from_date)
			$from_date= $olddb_params->from_date;

		$iportion= 0;
		do
		{
			$offset= $iportion*$portion_size;
			echo "\r\n".get_current_datetime()." select from $from_date, offset $offset, portion_size $portion_size..\r\n";
			$txt_query= " select result from efrsb.messages where publishdate <= ? order by publishdate desc limit ?, ?;";
			$rows= execute_query($txt_query,array('sii',$from_date,$offset,$portion_size));
			$count_rows= count($rows);
			echo get_current_datetime()." ..selected $count_rows messages.\r\n";

			echo get_current_datetime()." insert..\r\n";
			foreach ($rows as $row)
			{
				echo "*";
				Add_downloaded_message($row->result,null);
			}
			echo "\r\n".get_current_datetime()." ..inserted.\r\n";

			$i++;
			$iportion++;
		}
		while (0!=$count_rows && $i<100000000 && $iportion<$max_portions);
	}
	while (0!=$count_rows && $i<100000000);
}