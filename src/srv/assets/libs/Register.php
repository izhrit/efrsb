<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';
require_once '../assets/helpers/json.php';
require_once '../assets/libs/efrsb.php';

function UpdateLoadedSRORegister(&$sro_array)
{
	foreach ($sro_array as $sro)
	{
		try
		{
			$INN = !isset($sro['!INN']) ? null : $sro['!INN'];
			$Name = $sro['!FullName'];
			$DateLastModif = $sro['!DateLastModif'];
			$RegNum = !isset($sro['!RegNum']) ? null : $sro['!RegNum'];
			$SROID = $sro['!SROID'];
			$UrAdress=$sro['!UrAdress'];

			$sro_json_txt= nice_json_encode($sro);
			$txt_query= 'insert into sro
					   (RegNum, Body,        INN, Name, DateLastModif, id_SRO, UrAdress)
				values (?,      compress(?), ?,   ?,    ?, ?, ?) on duplicate key update
				Body=compress(?), INN=?, Name=?, DateLastModif=?;';

			execute_query_no_result($txt_query,array('sssssssssss',
				$RegNum,
				$sro_json_txt,$INN,$Name,$DateLastModif, $SROID, $UrAdress,
				$sro_json_txt,$INN,$Name,$DateLastModif));

			$txtDateLastModif= mb_substr($DateLastModif,0,10);
			echo get_current_datetime(). " updated DateLastModif:$txtDateLastModif RegNum:$RegNum ($Name)\r\n";
		}
		catch (Exception $ex)
		{
			echo get_current_datetime(). " can not update sro:\r\n";
			print_r($sro);
			throw $ex;
		}
	}
}

function UpdateSRORegisterAfter($date_after)
{
	//DateTime -> String, иначе Exception
	$date_after_string = date_format($date_after, "Y-m-d\TH:i:s");

	echo get_current_datetime(). "   after \"$date_after_string\"\r\n";

	$efrsb_client= SafePrepareEfrbClient();
	$sro_register= $efrsb_client->GetSroRegister($date_after_string);
	$sro_array= $sro_register['SROList']['SRO'];
	$sro_array_count= count($sro_array);

	echo get_current_datetime(). "   loaded $sro_array_count sro\r\n";

	UpdateLoadedSRORegister($sro_array);

	echo get_current_datetime(). " sro updated!\r\n";
}

function UpdateSRORegister($loopdate= null)
{
	$txt_query= 'select max(DateLastModif) max_DateLastModif from sro;';
	$rows= execute_query($txt_query,array());
	$date_after= (0==count($rows) || null==$rows[0]->max_DateLastModif)
		? date_create(null==$loopdate ? '2001-01-01' : $loopdate) : date_create($rows[0]->max_DateLastModif);

	UpdateSRORegisterAfter($date_after);
}

function UpdateLoadedSROFromEGRUL($ogrn,$stitle,$inn)
{
	try
	{
		$txt_query = 'UPDATE sro SET OGRN=?, Stitle=? WHERE (INN = ?);';
		execute_query_no_result($txt_query,array('sss',	$ogrn, $stitle, $inn));
		echo get_current_datetime(). " updated INN:$inn OGRN:$ogrn ($stitle)\r\n";
	}
	catch (Exception $ex)
	{
		echo get_current_datetime(). " can not update sro:\r\n";
		print_r($inn);
		throw $ex;
	}

}

function Request_to_EGRUL($inn)
{
	try
	{
		$auth_token='7b944ac60e6b04df5324565f2bd5fece';
		$url= "https://probili.ru/sro/$inn/?auth_token=$auth_token";
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_FAILONERROR, 1);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		$ans= curl_exec($curl);
		if (curl_error($curl)) {
            echo 'Error:' . curl_error($curl);
        }
		/*$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
		if (200!=$httpcode){
			throw new Exception("can not get sro from egrul (code $httpcode for url \"$url\")");
		}*/
		curl_close($curl);
		return $ans;
	}
	catch (Exception $ex)
	{
		echo get_current_datetime(). " can not get infromation from egrul:\r\n";
		print_r($inn);
		throw $ex;
	}

}

function UpdateSROFromEGRUL($loopdate= null)
{
	$txt_query= 'select INN from sro where OGRN is NULL or Stitle is NULL;';
	$rows= execute_query($txt_query,array());
	$sro_array_count=count($rows);

	echo get_current_datetime(). " $sro_array_count sro for update\r\n";
	foreach($rows as $row)
	{
		$sro=array();
		$sro=json_decode(Request_to_EGRUL($row->INN),true);
		$ogrn=$sro['data']['item']['ogrn'];
		$stitle=$sro['data']['item']['stitle'];

		UpdateLoadedSROFromEGRUL($ogrn, $stitle, $row->INN);
	}
	echo get_current_datetime(). " sro updated!\r\n";
}

$region_cashe= null;
function safe_load_regions()
{
	global $region_cashe;
	if (null==$region_cashe)
	{
		$rows = execute_query('SELECT id_Region, Name FROM region;', array());
		foreach ($rows as $row)
		{
			if (null==$region_cashe)
				$region_cashe= array();
			$region_cashe[$row->Name]= $row;
		}
	}
}

function SaveRegionAndReturnId($region_name)
{
	global $region_cashe;
	safe_load_regions();
	if (isset($region_cashe[$region_name]))
	{
		return $region_cashe[$region_name]->id_Region;
	}
	else
	{
		$region_id = execute_query_get_last_insert_id('INSERT INTO region (Name) VALUES (?)', array('s', $region_name));
		$region_cashe[$region_name]= (object)array('id_Region'=>$region_id, 'Name'=>$region_name);
		echo "\r\n".get_current_datetime() . " saved Region with Name: $region_name\r\n";
		return $region_id;
	}
}

function UpdateLoadedManager($m)
{
	/*
	Отличия от документации:
	SRORegNum  - может быть пустым
	SRORegDate - может быть пустым
	DateReg - может быть пустым
	OGRNIP - может быть пустым

	RegNum - может быть пустым
	Region - может быть пустым
	MiddleName - может быть пустым
	*/
	$ArbitrManagerID = $m['!ArbitrManagerID'];
	$DateLastModif   = $m['!DateLastModif'];
	$FirstName       = !isset($m['!FirstName']) ? null : $m['!FirstName'];
	$LastName        = !isset($m['!LastName']) ? null : $m['!LastName'];
	$MiddleName      = !isset($m['!MiddleName']) ? null : $m['!MiddleName'];
	$RegNum          = !isset($m['!RegNum']) ? null : $m['!RegNum'];
	$OGRNIP          = !isset($m['!OGRNIP']) ? null : $m['!OGRNIP'];
	$INN             = $m['!INN'];
	$SRORegNum       = !isset($m['!SRORegNum']) ? null : $m['!SRORegNum'];
	$DateReg         = !isset($m['!DateReg']) ? null : $m['!DateReg'];
	$SRORegDate      = !isset($m['!SRORegDate']) ? null : $m['!SRORegDate'];
	$DateDelete      = !isset($m['!DateDelete']) ? null : $m['!DateDelete'];
	$Region          = !isset($m['!Region']) ? null : $m['!Region'];

	$Region_Id = null==$Region ? null : SaveRegionAndReturnId($Region);

	$m_json_txt = nice_json_encode($m);
	$txt_query  = "INSERT INTO manager (ArbitrManagerID,
Body,             DateLastModif,   FirstName,   MiddleName,   LastName,   RegNum,   OGRNIP,   INN,   SRORegNum,   DateReg,   SRORegDate,   DateDelete, id_Region)
VALUES (?,
compress(?),      ?,               ?,           ?,            ?,          ?,        ?,        ?,     ?,           ?,         ?,            ?, ?)
ON DUPLICATE KEY UPDATE
Body=compress(?), DateLastModif=?, FirstName=?, MiddleName=?, LastName=?, RegNum=?, OGRNIP=?, INN=?, SRORegNum=?, DateReg=?, SRORegDate=?, DateDelete=?, id_Region=?;";
		execute_query_no_result($txt_query, array('sssssssssssssssssssssssssss',
												  $ArbitrManagerID,
$m_json_txt,      $DateLastModif,  $FirstName,  $MiddleName,  $LastName,  $RegNum,  $OGRNIP,  $INN,  $SRORegNum,  $DateReg,  $SRORegDate,  $DateDelete,  $Region_Id,
$m_json_txt,      $DateLastModif,  $FirstName,  $MiddleName,  $LastName,  $RegNum,  $OGRNIP,  $INN,  $SRORegNum,  $DateReg,  $SRORegDate,  $DateDelete,  $Region_Id));

	$txtDateLastModif= mb_substr($DateLastModif,0,10);
	echo get_current_datetime() . " updated DateLastModif:$txtDateLastModif ArbitrManagerID:$ArbitrManagerID ($LastName $FirstName $MiddleName) \r\n";
}

function UpdateManagerRegisterAfter($date_after)
{
	//DateTime -> String, иначе Exception
	$date_after_string = date_format($date_after, "Y-m-d\TH:i:s");

	echo get_current_datetime() . "   after \"$date_after_string\"\r\n";

	$efrsb_client     = SafePrepareEfrbClient();
	$manager_register = $efrsb_client->GetArbitrManagerRegister($date_after_string);

	$manager_array= $manager_register['AMList']['ArbitrManager'];
	$manager_array_count= count($manager_array);

	echo get_current_datetime(). "   loaded $manager_array_count managers\r\n";

	foreach ($manager_array as $m)
	{
		try
		{
			UpdateLoadedManager($m);
		}
		catch (Exception $ex)
		{
			echo get_current_datetime(). " can not update manager:\r\n";
			print_r($m);
			throw $ex;
		}
	}
}

function UpdateManagerRegister($loopdate= null)
{
	$txt_query  = 'SELECT max(DateLastModif) max_DateLastModif FROM manager;';
	$rows       = execute_query($txt_query, array());
	$date_after= (0==count($rows) || null==$rows[0]->max_DateLastModif)
		? date_create(null==$loopdate ? '2001-01-01' : $loopdate) : date_create($rows[0]->max_DateLastModif);

	UpdateManagerRegisterAfter($date_after);
}

function SaveDeptorRecord($Debtor)
{
	$BankruptId=      $Debtor["!BankruptId"];
	$LastMessageDate= $Debtor["LastMessageDate"];
	$LastReportDate=  $Debtor["LastReportDate"];
	$INN=             !isset($Debtor["!INN"]) ? null : $Debtor["!INN"];
	$SNILS=           !isset($Debtor["!SNILS"]) ? null : $Debtor["!SNILS"];
	switch ($Debtor['DebtorType'])
	{
		case 'DebtorCompany':
			$Name= $Debtor["!ShortName"];
			$OGRN= $Debtor["!OGRN"];
			break;
		case 'DebtorPerson':
			$Name= trim($Debtor["!LastName"]) . " " . trim($Debtor["!FirstName"]) . (!isset($Debtor["!MiddleName"]) ? '' : ' ' . trim($Debtor["!MiddleName"]));
			$OGRN= !isset($Debtor["!OGRNIP"]) ? null : $Debtor["!OGRNIP"];
			break;
	}
	$DateLastModif   = $Debtor["!DateLastModif"];
	$Category        = $Debtor["!Category"];
	$CategoryCode    = $Debtor["!CategoryCode"];
	$Region          = $Debtor["!Region"];

	$LastPublishDateTime= date_format($Debtor['LastPublishDateTime'], 'd.m.Y H:i:s');
	unset($Debtor['LastPublishDateTime']);
	unset($Debtor['DebtorType']);

	$RegionId = null==$Region ? null : SaveRegionAndReturnId($Region);
	$Body     = nice_json_encode($Debtor);

	$txt_query = "INSERT INTO debtor (BankruptId,
Body,             DateLastModif,   LastMessageDate,   LastReportDate,   INN,   SNILS,   Name,   OGRN,   Category,   CategoryCode,   id_Region)
VALUES (?,
compress(?),      ?,               ?,                 ?,                ?,     ?,       ?,      ?,      ?,          ?,              ?)
ON DUPLICATE KEY UPDATE 
Body=compress(?), DateLastModif=?, LastMessageDate=?, LastReportDate=?, INN=?, SNILS=?, NAME=?, OGRN=?, Category=?, CategoryCode=?, id_Region=?;";

	execute_query_no_result($txt_query, array('sssssssssssssssssssssss',
		$BankruptId,
		$Body,    $DateLastModif,  $LastMessageDate,  $LastReportDate,  $INN,  $SNILS,  $Name,  $OGRN,  $Category,  $CategoryCode,  $RegionId,
		$Body,    $DateLastModif,  $LastMessageDate,  $LastReportDate,  $INN,  $SNILS,  $Name,  $OGRN,  $Category,  $CategoryCode,  $RegionId));

	//echo get_current_datetime() . " updated Debtor LastPublishDateTime:$LastPublishDateTime BankruptId:$BankruptId ($Name)\r\n";
	echo " $BankruptId";
}

function LoadSkippedDebtors($loopdate = null)
{
	$txt_query  = "select distinct m.BankruptId BankruptId
	from message m 
	left join debtor d on m.BankruptId=d.BankruptId 
	where m.BankruptId is not null && d.BankruptId is null 
	limit 1000;";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('10 minutes'));

	for ($rows = execute_query($txt_query,array()), $count= count($rows); 
		0!=$count && date_create() < $time_start_to_stop; 
		$rows = execute_query($txt_query,array()), $count= count($rows))
	{
		echo get_current_datetime(). " $count debtors skipped\r\n";
		echo "debtors with BankruptId:";
		foreach ($rows as $row)
		{
			$efrsb_client= SafePrepareEfrbClient();
			$debtor= $efrsb_client->GetDebtorByIdBankrupt($row->BankruptId);
			try
			{
				SaveDeptorRecord($debtor);
			}
			catch (Exception $ex)
			{
				echo get_current_datetime(). " can not update debtor:\r\n";
				print_r($debtor);
				throw $ex;
			}
		}
		echo "\r\n";
	}

	echo "\r\n";
}

$load_debtor_interval= date_interval_create_from_date_string('1 days');

function UpdateDebtorRegisterByLastPublicationPeriodAfter($date_after)
{
	global $current_date_time, $load_debtor_interval;
	$finish_time= null==$current_date_time ? date_create() : $current_date_time;
	$interval= $load_debtor_interval;
	for
	(
		$start_time= $date_after,
		$stop_time= date_add(clone $start_time, $interval),
		$stop_time= ($stop_time <= $finish_time) ? $stop_time : $finish_time
		;
		$start_time!=$stop_time
		;
		$start_time= $stop_time,
		$stop_time= date_add(clone $start_time, $interval),
		$stop_time= ($stop_time <= $finish_time) ? $stop_time : $finish_time
	)
	{
		echo get_current_datetime() . " get debtors published\r\n";
		echo get_current_datetime() . "     after  " . date_format($start_time, 'd.m.Y H:i:s'). "\r\n";
		echo get_current_datetime() . "     before " . date_format($stop_time, 'd.m.Y H:i:s'). "\r\n";

		$efrsb_client    = SafePrepareEfrbClient();
		$debtor_array = $efrsb_client->GetDebtorsByLastPublicationPeriod(date_format($start_time, "Y-m-d\TH:i:s"), date_format($stop_time, "Y-m-d\TH:i:s"));
		$debtor_array_count= count($debtor_array);
		echo get_current_datetime() . "   loaded $debtor_array_count debtors\r\n";

		foreach ($debtor_array as $debtor)
		{
			try
			{
				SaveDeptorRecord($debtor);
			}
			catch (Exception $ex)
			{
				echo get_current_datetime(). " can not update debtor:\r\n";
				print_r($debtor);
				throw $ex;
			}
		}

		echo "\r\n" . get_current_datetime() . "   updated loaded debtors\r\n";
	}
}

function UpdateDebtorRegisterByLastPublicationPeriod($loopdate = null)
{
	$txt_query  = "SELECT max(LastMessageDate) max_LastMessageDate, max(LastReportDate) max_LastReportDate FROM debtor;";
	$rows       = execute_query($txt_query, array());
	$date_after = null;
	if (0 == count($rows))
	{
		$date_after= date_create(null==$loopdate ? '2001-01-01' : $loopdate);
	}
	else
	{
		$row= $rows[0];
		if (null == $row->max_LastMessageDate)
		{
			$date_after= (null == $row->max_LastReportDate) 
				? date_create(null==$loopdate ? '2001-01-01' : $loopdate) 
				: date_create($row->max_LastReportDate);
		}
		else
		{
			if (null == $row->max_LastReportDate)
			{
				$date_after= date_create($row->max_LastMessageDate);
			}
			else
			{
				$d1= date_create($row->max_LastMessageDate);
				$d2= date_create($row->max_LastReportDate);
				$date_after= $d1 > $d2 ? $d1 : $d2;
			}
		}
	}

	$date_after= date_sub($date_after, date_interval_create_from_date_string('1 days'));
	UpdateDebtorRegisterByLastPublicationPeriodAfter($date_after);
}

function UpdateDebtorRegisterAfter($date_after)
{
	$date_after_string = date_format($date_after, "Y-m-d\TH:i:s");

	echo get_current_datetime() . "   after \"$date_after_string\"\r\n";

	$efrsb_client     = SafePrepareEfrbClient();
	$debtor_array= $efrsb_client->GetDebtorRegister($date_after_string);
	$debtor_array_count= count($debtor_array);

	echo get_current_datetime(). "   loaded $debtor_array_count debtors\r\n";

	foreach ($debtor_array as $d)
	{
		try
		{
			SaveDeptorRecord($d);
		}
		catch (Exception $ex)
		{
			echo get_current_datetime(). " can not update debtor:\r\n";
			print_r($d);
			throw $ex;
		}
	}

	echo "\r\n" . get_current_datetime() . "   updated loaded debtors\r\n";
}

function UpdateDebtorRegister($loopdate = null)
{
	$txt_query  = 'SELECT max(DateLastModif) max_DateLastModif FROM debtor;';
	$rows       = execute_query($txt_query, array());
	$date_after= (0==count($rows) || null==$rows[0]->max_DateLastModif)
		? date_create(null==$loopdate ? '2001-01-01' : $loopdate) : date_create($rows[0]->max_DateLastModif);

	//$date_after= date_sub($date_after, date_interval_create_from_date_string('1 minute'));
	UpdateDebtorRegisterAfter($date_after);
}

function UpdateDebtorManagerRow($row)
{
	if (!is_null($row->ArbitrManagerID) && !is_null($row->BankruptId))
	{
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			$txt_query = "INSERT INTO debtor_manager
					   (BankruptId, ArbitrManagerID,      DateTime_MessageFirst, DateTime_MessageLast)
				VALUES (?,          ?,                    ?,                     ?) 
				ON DUPLICATE KEY UPDATE DateTime_MessageLast=?;";
			$connection->execute_query_no_result($txt_query, array('sssss', 
				$row->BankruptId,   $row->ArbitrManagerID,$row->min_PublishDate, $row->max_PublishDate
										, $row->max_PublishDate));
			$connection->execute_query_no_result("update debtor set ArbitrManagerID=? where BankruptId=?",
				array('ss',$row->ArbitrManagerID,$row->BankruptId));
			$connection->commit();
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}
		echo ' '.$row->BankruptId;
	}
}

function UpdateDebtorManagerRows($rows,$count)
{
	echo "debtors with BankruptId:";
	foreach ($rows as $row)
	{
		UpdateDebtorManagerRow($row);
	}
	echo "\r\n";
	return $rows[$count-1]->max_PublishDate;
}

function UpdateDebtorManagerAfter($date_after)
{
	echo get_current_datetime(). " update debtor_manager with messages after $date_after \r\n";

	$txt_query1= "SELECT m.BankruptId, m.ArbitrManagerID, max(m.PublishDate) max_PublishDate, min(m.PublishDate) min_PublishDate
		FROM
			(
				select BankruptId, ArbitrManagerID, PublishDate
				from message 
				where PublishDate>? 
				order by PublishDate 
				limit 10000
			)
			as m 
		GROUP BY BankruptId, ArbitrManagerID 
		ORDER BY PublishDate;";

	$txt_query2= "SELECT m.BankruptId, m.ArbitrManagerID, max(m.PublishDate) max_PublishDate, min(m.PublishDate) min_PublishDate
		FROM
			(
				select BankruptId, ArbitrManagerID, PublishDate
				from message 
				where PublishDate=? 
			) 
			as m 
		GROUP BY BankruptId, ArbitrManagerID 
		ORDER BY PublishDate;";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('10 minutes'));

	for ($rows = execute_query($txt_query1,array('s',$date_after)), $count= count($rows); 
		0!=$count && date_create() < $time_start_to_stop; 
		$rows = execute_query($txt_query1,array('s',$date_after)), $count= count($rows))
	{
		echo get_current_datetime(). " $count (BankruptId,ArbitrManagerID) after $date_after\r\n";
		$date_after= UpdateDebtorManagerRows($rows,$count);
		$rows = execute_query($txt_query2,array('s',$date_after));
		$count= count($rows);
		echo get_current_datetime(). " $count (BankruptId,ArbitrManagerID) on $date_after\r\n";
		if ($count>0)
			$date_after= UpdateDebtorManagerRows($rows,$count);
	}

	echo "\r\n";
}

function UpdateDebtorManager($loopdate = null)
{
	$txt_query= 'select max(DateTime_MessageLast) max_DateTime_MessageLast from debtor_manager;';
	$rows= execute_query($txt_query,array());
	$date_after= (0!=count($rows) && null!=$rows[0]->max_DateTime_MessageLast) 
		? $rows[0]->max_DateTime_MessageLast : (null!=$loopdate ? $loopdate : '2001-01-01');

	UpdateDebtorManagerAfter($date_after);
}

function LoadSkippedDebtorManager($loopdate = null)
{
	echo get_current_datetime(). " update skipped debtor_manager for debtor with ArbitrManagerID is null\r\n";

	$txt_query= "SELECT m.BankruptId, m.ArbitrManagerID, max(m.PublishDate) max_PublishDate, min(m.PublishDate) min_PublishDate
		from debtor d
		left join message m on d.BankruptId=m.BankruptId
		where d.ArbitrManagerID is null && m.ArbitrManagerID is not null 
		GROUP BY m.BankruptId, m.ArbitrManagerID 
		ORDER BY m.PublishDate
		limit 1000
	;";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('1 minute'));

	for ($rows = execute_query($txt_query,array()), $count= count($rows); 
		0!=$count && date_create() < $time_start_to_stop; 
		$rows = execute_query($txt_query,array()), $count= count($rows))
	{
		echo get_current_datetime(). " load $count (BankruptId,ArbitrManagerID)\r\n";
		$date_after= UpdateDebtorManagerRows($rows,$count);
	}

	echo get_current_datetime(). " load $count (BankruptId,ArbitrManagerID)\r\n";

	$txt_query= "select count(distinct BankruptId) c from debtor where ArbitrManagerID is null;";
	$rows = execute_query($txt_query,array()); 
	$count= $rows[0]->c;

	echo get_current_datetime(). " $count debtors with ArbitrManagerID is null left..\r\n";

	echo "\r\n";
}

const phone_matches_pattern = "/(\s+|\.)(([87+][0-9 -]{2,3}|[87+]\s?\([0-9 -]{2,3}\)s?|\([0-9 ]{4,5}\))([0-9 -]{7,14}))/u";
const mail_matches_pattern  = "/[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/im";

function ParseManagerContacts_from_message($con, $msg)
{
	try
	{
		$body= trim($msg->Body);
		$phones = ParsePhonesFromMessage($body);
		$emails = ParseEmailsFromMessage($body);

		$con->begin_transaction();
		try
		{
			if (!empty($emails) || !empty($phones))
			{
				if (!empty($phones))
				{
					foreach ($phones as $phone)
						$con->execute_query_no_result("call Add_phone(?,?,?);", array('sss',$msg->id_Manager,$phone,$msg->id_Message));
				}
				if (!empty($emails))
				{
					foreach ($emails as $email)
						$con->execute_query_no_result("call Add_email(?,?,?);", array('sss',$msg->id_Manager,$email,$msg->id_Message));
				}
				echo " " . $msg->Revision;
			}
			$con->execute_query_no_result("update processstatus set LastParsedContactsMessageRevision=?;", array('s',$msg->Revision));
			$con->commit();
		}
		catch (Exception $ex)
		{
			$con->rollback();
			throw $ex;
		}
	}
	catch (Exception $ex)
	{
		echo get_current_datetime(). " can not parse message:\r\n";
		print_r($msg);
		$exception_class= get_class($ex);
		$exception_Message= $ex->getMessage();
		echo "Unhandled exception occurred: $exception_class - $exception_Message\r\n";
		echo 'catched Exception:';
		print_r($ex);
	}
}

function ParseManagerContacts($loopdate = null)
{
	$con= default_dbconnect();
	mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

	echo get_current_datetime() . " select LastParsedContactsMessageRevision..\r\n";

	$max_Revision= execute_query('select LastParsedContactsMessageRevision from processstatus',array());
	if (!empty($max_Revision))
	{
		$max_Revision= $max_Revision[0]->LastParsedContactsMessageRevision;
	}
	else
	{
		$max_Revision= 0;
		execute_query_no_result('insert into processstatus set LastParsedContactsMessageRevision= 0;',array());
	}
	$part_size      = 10000;

	echo get_current_datetime() . " let's read & parse messages with Revision>=$max_Revision\r\n";

	$txt_query= "SELECT
			MSG.id_Message,
			MSG.Revision,
			MSG.ArbitrManagerID,
			uncompress(MSG.Body) AS Body,
			M.id_Manager
		FROM message MSG
			LEFT JOIN manager M ON M.ArbitrManagerID=MSG.ArbitrManagerID 
		WHERE MSG.ArbitrManagerID IS NOT NULL AND MSG.Revision > ?
		ORDER BY MSG.Revision ASC
		LIMIT $part_size;";

	$time_start_to_stop= date_add(date_create(), date_interval_create_from_date_string('20 minutes'));

	for (
		$messages = $con->execute_query($txt_query, array('s', $max_Revision)), $count= count($messages);
		0!=$count && date_create() < $time_start_to_stop;
		$messages = $con->execute_query($txt_query, array('s', $max_Revision)), $count= count($messages)
	)
	{
		echo get_current_datetime() . " read $count messages to parse, parse message Revisions:\r\n";
		foreach ($messages as $msg)
		{
			ParseManagerContacts_from_message($con, $msg);
			$max_Revision= $msg->Revision;
		}
		echo "\r\n";
	}
	if (0==$count)
		echo get_current_datetime() . " no messages to parse!\r\n";
}

function ParsePhonesFromMessage($msg_body)
{
	preg_match_all(phone_matches_pattern, $msg_body, $matches_phone);

	$matches_phone = array_filter($matches_phone);

	$phones = array();

	if (!empty($matches_phone)) {
		foreach ($matches_phone as $phone) {
			if (is_array($phone)) {
				foreach ($phone as $item) {
					$format_phone = FormatPhone($item);
					if (!is_null($format_phone)) {
						array_push($phones, $format_phone);
					}
				}
			} else {
				$format_phone = FormatPhone($phone);
				if (!is_null($format_phone)) {
					array_push($phones, $format_phone);
				}
			}
		}
	}

	return array_unique($phones);
}

function ParseEmailsFromMessage($msg_body)
{
	preg_match_all(mail_matches_pattern, $msg_body, $matches_email);

	$matches_email = array_filter($matches_email);

	$emails = array();

	if (!empty($matches_email)) {
		foreach ($matches_email as $email) {
			if (is_array($email)) {
				foreach ($email as $item) {
					array_push($emails, $item);
				}
			} else {
				array_push($emails, $email);
			}
		}
	}

	return array_unique($emails);
}

function FormatPhone($phone)
{
	$phone = str_replace(array("+", "(", ")", "-", "_", " ",), "", trim($phone));

	$str_length = strlen($phone);
	if ($str_length >= 9 && $str_length <= 11) {
		$first_two_characters = mb_substr($phone, 0, 2);
		$first_character      = mb_substr($phone, 0, 1);
		if ($first_two_characters == "79" || $first_two_characters == "89" || $first_character == "9") {
			return $phone;
		}
	}

	return null;
}

