<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';

class Manager
{
	public function GetAllManagers($date_after = null)
	{
		$date_after = !is_null($date_after) ? $date_after : date("Y-m-d", strtotime("- 1 day"));
		$managers = execute_query("
	SELECT
		M.id_Manager,
		M.ArbitrManagerID,
		FirstName,
		LastName,
		MiddleName,
		RegNum,
		SRORegNum,
		group_concat(distinct p.Number  order by p.Number ) Phones,
		group_concat(distinct e.address order by e.address) Emails
	FROM manager M
	inner join debtor_manager dm on dm.ArbitrManagerID=M.ArbitrManagerID && dm.DateTime_MessageLast > DATE(NOW() - INTERVAL 4 MONTH)
	left join email_manager em on M.id_Manager=em.id_Manager
	left join email e on e.id_Email=em.id_Email
	left join phone_manager pm on M.id_Manager=pm.id_Manager
	left join phone p on p.id_Phone=pm.id_Phone
	GROUP BY M.id_Manager
	HAVING phones IS NOT NULL OR emails IS NOT NULL",
								  array());
		return $managers;
	}

	public function GetManagerInfoByName($managerFullName)
	{
		return execute_query(
		"SELECT 			
			M.LastName,
			M.FirstName,
			M.MiddleName,		
			M.RegNum,	
			GROUP_CONCAT(DISTINCT E.address) Emails,
			GROUP_CONCAT(DISTINCT P.Number) Phones,
			(SELECT MAX(PublishDate) FROM message WHERE ArbitrManagerID = M.ArbitrManagerID) MessageLastDate
		FROM manager M
		LEFT JOIN email_manager EM ON M.id_Manager = EM.id_Manager
		LEFT JOIN email E ON E.id_Email = EM.id_Email
		LEFT JOIN phone_manager PM ON M.id_Manager = PM.id_Manager
		LEFT JOIN phone P ON P.id_Phone = PM.id_Phone
		WHERE
			CONCAT(LastName,
					' ',
					FirstName,
					' ',
					MiddleName) = ?
		GROUP BY M.ArbitrManagerID", 
		array('s', $managerFullName));		
	}
}