<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';

try
{
	if (!isset($_GET['inn'])&&(!isset($_GET['regnum'])||$_GET['regnum']==null))
		throw new Exception('skipped mandatory argument inn or regnum!');
	else if (isset($_GET['inn'])&&$_GET['inn']==null&&(!isset($_GET['regnum'])||$_GET['regnum']==null))
			throw new Exception('skipped mandatory argument inn or regnum!');

	if (isset($_GET['inn']))
	if ($_GET['inn']!=null)
	{
		$txt_query= "select
		d.Name Name
		,d.INN INN
		,d.OGRN OGRN
		,d.SNILS SNILS
		,UNCOMPRESS(d.Body) Body
		from debtor d
		inner join manager m on m.ArbitrManagerID=d.ArbitrManagerID
		where m.INN=?
		;";
		$rows= execute_query($txt_query,array('s',$_GET['inn']));
	}

	if (isset($_GET['regnum']))
	if($_GET['regnum']!=null)
	{
		$txt_query= "select
		d.Name Name
		,d.INN INN
		,d.OGRN OGRN
		,d.SNILS SNILS
		,UNCOMPRESS(d.Body) Body
		from debtor d
		inner join manager m on m.ArbitrManagerID=d.ArbitrManagerID
		where m.RegNum=?
		;";
		$rows= execute_query($txt_query,array('s',$_GET['regnum']));
	}
	foreach($rows as $value)
	{
		$arr=array();
		$arr=json_decode($value->Body,true);

		$case_numbers=array();
		foreach($arr['LegalCaseList']['LegalCaseInfo'] as $number)
			$case_numbers[]=$number['Number'];
		$value->Case_numbers=$case_numbers;

		$value->Address=$arr['!LegalAddress'];
		unset($value->Body);
	}

	echo nice_json_encode($rows);
}
catch (Exception $exception)
{	
    header("HTTP/1.1 500 Internal Server Error");
    write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
    write_to_log('$_GET:');
    write_to_log($_GET);
    throw new Exception("can not get debtors information!");
}
