<?

/*

ВНИМАНИЕ!


*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

/*mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
*/
try
{
	$txt_query= "
		select 
			  s.RegNum RegNum
			, s.INN INN
			, s.OGRN OGRN
			, s.UrAdress UrAdress
			, s.Name Name
			, s.Stitle Stitle
		from sro s
	;";

	$rows= execute_query($txt_query,array());

	echo nice_json_encode($rows);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute GetAllSROInformation!");
}
