<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';

try
{
	if (!isset($_GET['q']))
		throw new Exception('skipped mandatory argument q!');

	if ($_GET['q']==null)
		throw new Exception('skipped mandatory argument q!');
	else
	{
		$q=$_GET['q'];
		$txt_query= "SELECT 
			m.LastName
			,m.FirstName
			,m.MiddleName
			,m.RegNum
			,m.INN
			,s.Stitle
		FROM manager m
			inner join sro s on m.SRORegNum = s.RegNum
		where 
			concat(LastName,' ',FirstName,' ',MiddleName) like ?
			or concat(FirstName,' ',MiddleName,' ',LastName) like ?
			or concat(FirstName,' ',LastName) like ?
			or MiddleName like ?
		;";
		$rows= execute_query($txt_query,array('ssss',$q.'%',$q.'%',$q.'%',$q.'%'));
	}

	echo nice_json_encode($rows);
}
catch (Exception $exception)
{	
    header("HTTP/1.1 500 Internal Server Error");
    write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
    write_to_log('$_GET:');
    write_to_log($_GET);
    throw new Exception("can not get manager information!");
}
