<?

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once('../assets/libs/Message.php');

try
{
	global $argv;
	if (count($argv)>1 && 'test'==$argv[1])
	{
		global $current_date_time;
		$current_date_time= date_create('2019-06-21');
	}
	UpdateFieldsInExistingMessages();
}
catch (Exception $ex)
{
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	echo "Unhandled exception occurred: $exception_class - $exception_Message\r\n";
	echo 'catched Exception:';
	print_r($ex);
}