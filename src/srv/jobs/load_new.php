<?

require_once '../assets/config.php';
require_once '../assets/libs/Message.php';

function Job_load_new_files()
{
	global $job_params;

	// 1)	���� ���������� pid ����, ��������� ������ (������� � ���)
	if (file_exists($job_params->pid_file_path_for_new_job))
	{
		echo 'pid file exists!';
		exit();
	}

	// 2)	������� pid ����
	file_put_contents($job_params->pid_file_path_for_new_job,date_format(date_create(),'Y-m-d\TH:i:s'));

	try
	{
		// 3)	��������� �� ����� ����� ��������� �������������
		$last_time_sync= !file_exists($job_params->file_path_last_sync_time) 
			? '2010-01-01T00:00:00' : file_get_contents($job_params->file_path_last_sync_time);

		// 4)	���� � ��������� ������������� ������ ����� 5 ����� ��� ���������� �������-����
		if (FALSE==$last_time_sync 
			|| date_diff(date_create(),date_create($last_time_sync))->i > $job_params->load_new_messages_after_minutes
			|| file_exists($job_params->file_path_sync_trigger))
		{
			if (file_exists($job_params->file_path_sync_trigger))
				unlink($job_params->file_path_sync_trigger);
			// a.	��������� �������� �������� ����� ��������� �� �������
			Load_new_messages();
			// b.	�������� � ���� ��������� ������������� ������� �����
			file_put_contents($job_params->file_path_last_sync_time,date_format(date_create(),'Y-m-d\TH:i:s'));
		}
	}
	catch (nusoapException $ex)
	{
		unlink($job_params->pid_file_path_for_new_job);
		$nusoap_err= $ex->nusoap_err;
		echo "nusoap $nusoap_err";
		throw $ex;
	}
	catch (Exception $ex)
	{
		// 5)	������� pid ����
		unlink($job_params->pid_file_path_for_new_job);
		throw $ex;
	}
	unlink($job_params->pid_file_path_for_new_job);
}


try
{
	Job_load_new_files();
}
catch (Exception $ex)
{
	$exception_class= get_class($exception);
	$exception_Message= $exception->getMessage();
	echo "Unhandled exception occurred: $exception_class - $exception_Message";
	throw $ex;
}