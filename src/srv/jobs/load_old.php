<?

ini_set("memory_limit", "1024M");

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/libs/efrsb.php';
require_once '../assets/libs/Message.php';
require_once '../assets/libs/Register.php';

function Job_load_old_records_part($func,$title)
{
	global $job_params;
	echo "-------------------------------------------------------------------------------- { \r\n";
	echo get_current_datetime(). " $title\r\n";
	try
	{
		try
		{
			$func($job_params->load_old_oldest_date);
		}
		catch (nusoapException $ex)
		{
			$nusoap_err= $ex->nusoap_err;
			echo "nusoap $nusoap_err\r\n";
			throw $ex;
		}
	}
	catch (Exception $exception)
	{
		$exception_class= get_class($exception);
		$exception_Message= $exception->getMessage();
		echo "\r\nException occurred: $exception_class - $exception_Message\r\n";
	}
	echo get_current_datetime(). " done ($title)\r\n";
	echo "-------------------------------------------------------------------------------- } \r\n";
}

function Job_load_old_records_parts($parts)
{
	foreach ($parts as $part)
	{
		Job_load_old_records_part($part->load,$part->title);
	}
}

$job_parts= (object)array(
	(object)array('load'=>'Load_old_messages', 'title'=>'load old messages')
	,(object)array('load'=>'UpdateFieldsInExistingMessages', 'title'=>'update messages fields')
	,(object)array('load'=>'UpdateSRORegister', 'title'=>'update sro')
	,(object)array('load'=>'UpdateSROFromEGRUL', 'title'=>'update sro1')
	,(object)array('load'=>'UpdateManagerRegister', 'title'=>'update manager')
	//,(object)array('load'=>'UpdateDebtorRegisterByLastPublicationPeriod', 'title'=>'update debtor by publish date')
	,(object)array('load'=>'UpdateDebtorRegister', 'title'=>'update debtors')
	,(object)array('load'=>'LoadSkippedDebtors', 'title'=>'update skipped debtor')
	,(object)array('load'=>'UpdateDebtorManager', 'title'=>'update debtor manager link table')
	,(object)array('load'=>'LoadSkippedDebtorManager', 'title'=>'load skipped debtor manager')
	,(object)array('load'=>'ParseManagerContacts', 'title'=>'parse manager contacts')
);

function Job_load_old_records_locked()
{
	global $job_params, $job_parts;

	// 1)	���� ���������� pid ����, ��������� ������ (������� � ���)
	if (file_exists($job_params->pid_file_path_for_old_job))
	{
		echo 'pid file exists!';
		exit();
	}

	// 2)	������� pid ����
	file_put_contents($job_params->pid_file_path_for_old_job,date_format(date_create(),'Y-m-d\TH:i:s'));

	try
	{
		echo "******************************************************************************** { \r\n";
		// 3)	��������� �������� �������� ������ ���������
		Job_load_old_records_parts($job_parts);
		echo "******************************************************************************** } \r\n";
	}
	catch (Exception $ex)
	{
		// 4)	������� pid ����
		unlink($job_params->pid_file_path_for_old_job);
		throw $ex;
	}
	unlink($job_params->pid_file_path_for_old_job);
}

try
{
	Job_load_old_records_locked();
}
catch (Exception $exception)
{
	$exception_class= get_class($exception);
	$exception_Message= $exception->getMessage();
	echo "\r\nUnhandled exception occurred: $exception_class - $exception_Message\r\n";
	throw $exception;
}

