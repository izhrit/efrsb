<?

/*

ВНИМАНИЕ!
данный сервис планируется использовать в сервисе "Витрина данных ПАУ"
для демонстрации последних сообщений ЕФРСБ

*/

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

try
{
	if (!isset($_GET['BankruptId']))
		throw new Exception('skipped mandatory argument BankruptId!');

	$Bankruptids= preg_replace("/[^0-9,]/", "", $_GET['BankruptId']);
	$fields= "
		DATE_FORMAT(me.PublishDate, '%d.%m.%Y-%H:%i') PublishDate
		,me.Messageinfo_MessageType Messageinfo_MessageType
		,me.Number Number
		,me.MessageGUID MessageGUID
		,d.Name Name
		";
	$from_where="
		from {$tbl_prefix}message me
		inner join {$tbl_prefix}debtor d on d.Bankruptid=me.Bankruptid
		where me.Bankruptid in ($Bankruptids)
		";
	$filter_rule_builders= array(
		'PublishDate'=>function($l,$rule)
		{
			$data= mysqli_real_escape_string($l,$rule->data);
			if (strlen($data) == 11)
			{
				$newdata= substr($data,-4). substr($data,-7,2). substr($data,-10,2);
				$operator= substr($data,0,1);
				if (in_array($operator,array('>','<','=')))
					return " and me.message $operator $newdata ";
			}
			return '';
		}
		,'Name'=> 'std_filter_rule_builder'
		,'Number'=> 'std_filter_rule_builder'
		,'Messageinfo_MessageType'=> 'std_filter_rule_builder'
	);
	execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not get messages!");
}
