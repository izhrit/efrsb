<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/config.php';
require_once '../assets/helpers/json.php';
require_once '../assets/libs/MessageTypeField.php';


function cmp($a, $b) 
{
    if ($a->PublishDate == $b->PublishDate) {
        return 0;
    }
    return (strtotime($a->PublishDate) > strtotime($b->PublishDate)) ? -1 : 1;
}
try
{
	if (!isset($_GET['BankruptId'])||''==$_GET['BankruptId'])
		return '';
	else 
	{
		$Bankruptids= preg_replace("/[^0-9,]/", "", $_GET['BankruptId']);
		$fields= "
			DATE_FORMAT(me.PublishDate, '%d.%m.%Y') PublishDate
			,me.Messageinfo_MessageType Messageinfo_MessageType
			,UNCOMPRESS(me.Body) Body
			,me.MessageGUID MessageGUID
			,d.Name Name
			";
		$from_where="
			from message me
			inner join debtor d on d.Bankruptid=me.Bankruptid
			where me.Bankruptid in ($Bankruptids) and me.PublishDate>= DATE_SUB(CURRENT_DATE, INTERVAL 30 DAY)
			";
		$filter_rule_builders= array(
			'PublishDate'=>function($l,$rule)
			{
				$data= mysqli_real_escape_string($l,$rule->data);
				if (strlen($data) == 11)
				{
					$newdata= substr($data,-4). substr($data,-7,2). substr($data,-10,2);
					$operator= substr($data,0,1);
					if (in_array($operator,array('>','<','=')))
						return " and me.message $operator $newdata ";
				}
				return '';
			}
			,'Name'=> 'std_filter_rule_builder'
			,'Messageinfo_MessageType'=> 'std_filter_rule_builder'
		);
		$result=execute_query_for_jqgrid_and_return_result($fields,$from_where,$filter_rule_builders,'');
		//work with xml
		foreach($result['rows'] as $value)
		{
			$value->NewsType="EfrsbMessage";
			$xml = new SimpleXMLElement($value->Body);
			//get short message
			if(!$MessageType=strval($xml->MessageInfo['MessageType']))
				$value->Body="";
			else{
				global $MessageTypeField;
				$value->Body=strval($xml->MessageInfo->{$MessageTypeField[$MessageType]}->Text);
				global $SubstringInMessage;
				foreach($SubstringInMessage as $sub){
					if($tmp=stristr($value->Body,$sub)){
					$value->Body="..." . $tmp;
					break;
					}
				}
				$value->Body=mb_ereg_replace ('\n', '<br />',$value->Body);
			}
			//get GUID
			if($GUID=strval($xml->MessageGUID))
				$value->MessageGUID=$GUID;
		}
	}
	echo json_encode($result);
}
catch (Exception $exception)
{
    header("HTTP/1.1 500 Internal Server Error");
    write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
    write_to_log('$_GET:');
    write_to_log($_GET);
    throw new Exception("can not get messages!");
}
