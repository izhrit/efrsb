spec = {
  "openapi": "3.0.0",
  "servers": [   
	{
      "url": "http://192.168.0.206/efrsb/api.php",
      "description": "тестовый сервер ЕФРСБ"
    },
    {
      "url": "https://rsit.ru/efrsb/api.php",
      "description": "рабочий сервер ЕФРСБ"
    }
  ],
  "info": {
    "version": "1.0",
    "title": "API ЕФРСБ",
      "description": "Методы для взаимодействия с сервером ЕФРСБ.\n\nДанные на ЕФРСБ загружаются с ОТКУДА ТО\n * [ГДЕ ТО](https://bankrot.fedresurs.ru) - ЭТО ЧТО ТО \n"
  },
  "tags": [
    {
      "name": "Из ПАУ",
      "description": "получение данных из базы данных efrsb"
    }
  ],
  "paths": {
    "/GetDebtorsInformation": {
      "get": {
        "summary": "Получить информацию о должниках по INN или RegNum Арбитражного управляющего",
        "tags": [
          "Из ПАУ"
        ],
        "security": [
          {
            "Доступ по токену": []
          }
        ],
        "parameters": [
          {
            "in": "query",
            "name": "inn",
            "description":"ИНН арбитражного управляющего",
            "schema": {
              "type": "string",
              "default": null
            },
            "example": 0
          },
          {
            "in": "query",
             "name": "regnum",
             "description": "Регистрационный номер арбитражного управляющего",
            "schema": {
              "type": "string",
              "default": null
            },
            "example": 10
          },
        ],
        "responses": {
          "500": {
            "description": "HTTPCODE 500",
            "content": {
              "application/json": {
                  "schema": {
                      "type": "array",
                      "items": {
                          "type": "object",
                          "required": [
                              "Name",
                              "INN",
                              "OGRN",
                              "SNILS",
                              "Case_numbers",
                              "Address"
                          ],
                          "properties": {
                              "Name": {
                                  "type": "string",
                                  "example":"ООО Герундий"
                              },
                              "INN": {
                                  "type": "string",
                                  "example": "7907655280"
                              },
                              "OGRN": {
                                  "type": "string",
                                  "example": "1136422185130"
                              },
                              "SNILS": {
                                  "type": "string",
                                  "example": "79972271685"
                              },
                              "Case_numbers": {
                                  "type": "array",
                                  "items": {
                                      "type": "string",
                                      "example": "08АП-9/2017"
                                  }
                              },
                              "Address": {
                                  "type": "string",
                                  "example": "105062, г.Москва, Лялин пер., д. 19, корп. 1, пом. XXIV, комн. 18"
                              }
                          }
                      }
                  }
              }
            }
          }
        }
      }
    },
     "/GetAllSROInformation": {
         "get": {
             "summary": "Получить список всех СРО арбитражных управляющих",
             "tags": [
                 "Из ПАУ"
             ],
             "security": [
                 {
                     "Доступ по токену": []
                 }
             ],
             "responses": {
                 "500": {
                     "description": "HTTPCODE 500",
                     "content": {
                         "application/json": {
                             "schema": {
                                 "type": "array",
                                 "items": {
                                     "type": "object",
                                     "required": [
                                         "RegNum",
                                         "Name",
                                         "INN",
                                         "OGRN",
                                         "Stitle",
                                         "UrAdress"
                                     ],
                                     "properties": {
                                         "RegNum": {
                                             "type": "string",
                                             "description": "Регистрационный номер СРО",
                                             "example": "001-4"
                                         },
                                         "INN": {
                                             "type": "string",
                                             "description": "ИНН",
                                             "example": "6167065084"
                                         },
                                         "OGRN": {
                                             "type": "string",
                                             "description": "ОГРН",
                                             "example": "1026104143218"
                                         },
                                         "UrAdress": {
                                             "type": "string",
                                             "description": "Юридический адрес",
                                             "example": "344011,г.Ростов-на-Дону, пер.Гвардейский д.7"
                                         },
                                         "Name": {
                                             "type": "string",
                                             "description": "Наименование СРО",
                                             "example": "Ассоциация \"МСРО АУ\"  -  Ассоциация \"Межрегиональная саморегулируемая организация арбитражных управляющих\""
                                         },
                                         "Stitle": {
                                             "type": "string",
                                             "description": "Сокрщенное наименование",
                                             "example": "МСРО АУ"
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
         }
      },
      "/GetManagersInformation": {
          "get": {
              "summary": "Получить список АУ и информацию о них по префиксу ФИО АУ",
              "tags": [
                  "Из ПАУ"
              ],
              "security": [
                  {
                      "Доступ по токену": []
                  }
              ],
              "parameters": [
                  {
                      "in": "query",
                      "name": "q",
                      "description": "Пефикс ФИО АУ",
                      "example": "Иванов Ива",
                      "schema": {
                          "type": "string",
                          "default": null
                      },
                      "example": 0
                  }
              ],
              "responses": {
                  "500": {
                      "description": "HTTPCODE 500",
                      "content": {
                          "application/json": {
                              "schema": {
                                  "type": "array",
                                  "items": {
                                      "type": "object",
                                      "required": [
                                          "LastName",
                                          "FirstName",
                                          "MiddleName",
                                          "RegNum",
                                          "Stitle"
                                      ],
                                      "properties": {
                                          "LastName": {
                                              "type": "string",
                                              "description": "Фамилия АУ",
                                              "example": "Иванов"
                                          },
                                          "FirstName": {
                                              "type": "string",
                                              "description": "Имя АУ",
                                              "example": "Иван"
                                          },
                                          "MiddleName": {
                                              "type": "string",
                                              "description": "Отчество АУ",
                                              "example": "Иванович"
                                          },
                                          "RegNum": {
                                              "type": "string",
                                              "description": "Регитрационный номер АУ на ЕФРСБ",
                                              "example": "12546"
                                          },
                                          "INN": {
                                              "type": "string",
                                              "description": "ИНН АУ",
                                              "example": "364484332544"
                                          },
                                          "Stitle": {
                                              "type": "string",
                                              "description": "Короктое название СРО",
                                              "example": "ПАУ ЦФО"
                                          }
                                      }
                                  }
                              }
                          }
                      }
                  }
              }
          }
      }
    
  },
  "components": {
    "securitySchemes": {
      "Доступ по токену": {
        "description": "---\n\nДля работы с API Витрины данных ПАУ,\n\nнеобходимо получить токен доступа методом Авторизации \"**Auth**\"\n\nв дальнейшем этот токен всегда должен подаваться как поле в header\n\nama-datamart-access-token:...\n\nAPI генерирует одноразовый токен с ограниченным временем действия.\n\n---\n",
        "type": "apiKey",
        "in": "header",
        "name": "ama-datamart-access-token"
      }
    }
  }
}