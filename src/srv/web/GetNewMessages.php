<?

/*

ВНИМАНИЕ!
данный сервис используется напрямую программой ПАУ
для получения новых сообщений

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function start_build_query()
{
	return (object)array('query'=>'','args'=>array(''),'arg0'=>'');
}

function safe_add_condition($q,$fname,$fvalue,$revision_greater_than)
{
	if (''!=$fvalue)
	{
		if (''!=$q->query)
			$q->query.= " union \r\n";
		$q->query.= "(select Revision, uncompress(Body) Body from message where Revision > ? && $fname=?)\r\n";
		$q->arg0.= 'ss';
		$q->args[]= $revision_greater_than;
		$q->args[]= $fvalue;
	}
	return $q;
}

function stop_build_query($q,$limit)
{
	$q->query.= 'order by Revision limit ?;';
	$q->arg0.= 's';
	$q->args[]= $limit;
	$q->args[0]= $q->arg0;
	return $q;
}

function fix_argument($fvalue)
{
	$fvalue= str_replace('-', '', $fvalue);
	$fvalue= str_replace(' ', '', $fvalue);
	return $fvalue;
}

function get_string_arg($argname)
{
	global $_GET;
	return !isset($_GET[$argname]) ? '' : fix_argument($_GET[$argname]);
}

try
{
	$inn= get_string_arg('inn');
	$snils= get_string_arg('snils');
	$ogrn= get_string_arg('ogrn');

	if (''==$inn && ''==$snils && ''==$ogrn)
		throw new Exception('skipped mandatory GET parameters inn|snils|ogrn!');

	$revision_greater_than= !isset($_GET['revision-greater-than']) ? 0 : $_GET['revision-greater-than'];
	$limit= !isset($_GET['limit']) ? 100 : $_GET['limit'];
	if ($limit>200)
		$limit= 100;

	$q= start_build_query();
	$q= safe_add_condition($q,'INN',$inn,$revision_greater_than);
	$q= safe_add_condition($q,'OGRN',$ogrn,$revision_greater_than);
	$q= safe_add_condition($q,'SNILS',$snils,$revision_greater_than);
	$q= stop_build_query($q,$limit);
	$rows= execute_query($q->query,$q->args);

	header('Content-Type: text/plain',false);
	echo nice_json_encode($rows);

	global $job_params;
	file_put_contents($job_params->file_path_sync_trigger,date_format(date_create(),'Y-m-d\TH:i:s'));
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute GetNewMessages!");
}

