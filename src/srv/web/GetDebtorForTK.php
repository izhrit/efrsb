<?

/*

��������!
������ ������ ����������� ������������ � ������� "������� ������ ���"
��� ������ ���������� ��� ������ ��������� � ���������� ��������� 

*/

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/sax.php';

require_once '../assets/libs/Debtor.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function parse_Message_for_tk($msg)
{
	global $MessageInfo_MessageType_desciptions_by_api_name;

	$PublishDate_parser= new Field_parser(array('MessageData','PublishDate'));
	$Id_parser= new Field_parser(array('MessageData','Id'));
	$MessageInfo_MessageType_parser= new Field_parser(array('MessageData','MessageInfo','@MESSAGETYPE'));

	$Manager_CorrespondenceAddress_parser= new Field_parser(array('MessageData','PublisherInfo','ArbitrManager','CorrespondenceAddress'));
	$DecisionType_Name_parser= new Field_parser(array('MessageData','MessageInfo','CourtDecision','DecisionType','@Name'));
	$DecisionType_Id_parser= new Field_parser(array('MessageData','MessageInfo','CourtDecision','DecisionType','@Id'));

	$msg_parser = new XMLParserArray(array(
		$PublishDate_parser
		,$Id_parser
		,$MessageInfo_MessageType_parser
		,$Manager_CorrespondenceAddress_parser
		,$DecisionType_Name_parser
		,$DecisionType_Id_parser
		//,new XMLParser_echor()
	));

	$xml_parser = xml_parser_create();
	$msg_parser->bind($xml_parser);
	if (!xml_parse($xml_parser, $msg, /* is_final= */TRUE))
	{
		$ex_text= sprintf("������ XML: %s �� ������ %d",
						xml_error_string(xml_get_error_code($xml_parser)),
						xml_get_current_line_number($xml_parser));
		xml_parser_free($xml_parser);
		throw new Exception($ex_text);
	}
	xml_parser_free($xml_parser);

	$parsed_msg= (object)array();
	$parsed_msg->PublishDate= $PublishDate_parser->value;
	$parsed_msg->Id= $Id_parser->value;
	$parsed_msg->MessageInfo_MessageType= $MessageInfo_MessageType_parser->value;
	$parsed_msg->Manager_CorrespondenceAddress= $Manager_CorrespondenceAddress_parser->value;
	$parsed_msg->DecisionType_Name= $DecisionType_Name_parser->value;
	$parsed_msg->DecisionType_Id= $DecisionType_Id_parser->value;

	return $parsed_msg;
}

/*

�� ����� ��������� ����� ����� ��������� ��� �� �������� ������ ��� ��������������� ��
������ ������ ����� ����� ����� ��� ��������������� ��:
2. ����� �� ������� ��������� �������� ��
1. ����� �� ���������� ��������� �������� ��

3. ����� �� ��������� � �������� ���������
4. ����� �� ������� ���������
5. ����� �� ���������� ���������
*/

function Prepare_messages_for_tk($BankruptId)
{
	$txt_query= "select PublishDate, uncompress(Body) Body, MessageInfo_MessageType 
		from message where d.BankruptId = ? order by PublishDate desc limit 100;";
	$rows= execute_query($txt_query,array('s',$BankruptId));
	
	$messages= array();

	$au_address_info= null;

	foreach ($rows as $row)
	{
		$parsed_msg= parse_Message_for_tk($row->Body);
		$message= (object)array(
			'PublishDate'=>$row->PublishDate
			,'MessageData_Id'=>$row->efrsb_id
			,'Readable'=>$MessageInfo_MessageType_desciptions_by_api_name[$row->MessageInfo_MessageType]['Readable']
		);
		if (null!=$parsed_msg->DecisionType_Name)
			$message->DecisionType_Name= $parsed_msg->DecisionType_Name;
	}

	return (object)array('���������'=>$messages);
}

try
{
	$BankruptId= $_GET['BankruptId'];

	$txt_query= $txt_query_select_debtor_data + "where d.BankruptId = ?;";
	$rows= execute_query($txt_query,array('s',$BankruptId));

	$debtor_Data= prepareDebtorData($rows[0]);

	$debtor_Data->��������������= Prepare_messages_for_tk($BankruptId);

	header('Content-Type: text/plain');
	echo nice_json_encode($debtor_Data);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute GetDebtorsForSelect2!");
}

