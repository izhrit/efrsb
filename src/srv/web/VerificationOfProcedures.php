<?php

/*

ВНИМАНИЕ!
данный сервис используется сервером лицензий ПАУ
для проверки законности процедур АУ

*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';
require_once '../assets/libs/VerificationOfProcedures.php';

mb_internal_encoding("utf-8");
mb_http_output("UTF-8");
mb_http_input("UTF-8");

ini_set('error_reporting', E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

try
{
	if (!isset($_POST["procedures"]))
		throw new Exception("skipped post parameter procedures");

	$procedures= $_POST["procedures"];
	$checker= new VerificationOfProcedures();
	$check_result= $checker->CheckProcedures($procedures);
	echo nice_json_encode($check_result);
}
catch (Exception $ex)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
	write_to_log('$_POST:');
	write_to_log($_POST);
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute VerificationOfProcedures!");
}