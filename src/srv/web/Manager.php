<?php

/*

ВНИМАНИЕ!
данный сервис используется СРМ программы ПАУ
для обновления перечня АУ с контактами

*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET');

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';
require_once '../assets/libs/Manager.php';

mb_internal_encoding("utf-8");
mb_http_output("UTF-8");
mb_http_input("UTF-8");

ini_set('error_reporting', E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

try
{
	$action = $_GET["action"];
	if (empty($action))
		throw new Exception("skipped parameter action");

	switch ($action)
	{
		case "GetAll":
		{
			$date_after = isset($_GET["date_after"]) ? $_GET["date_after"] : null;
			$manager  = new Manager();
			$managers = $manager->GetAllManagers($date_after);
			echo nice_json_encode($managers);			
		}
			break;
		case "GetManagerInfo":
		{
			if (!isset($_POST["manager_name"]))
				throw new Exception("skipped post parameter managers");			
				
			$managerName = $_POST['manager_name'];
			$manager  = new Manager();
			$managers = $manager->GetManagerInfoByName($managerName);
			echo nice_json_encode($managers);			
		}
			break;
		default: throw new Exception("unknown action $action");
	}
}
catch (Exception $ex)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($ex) . ' - ' . $ex->getMessage());
	write_to_log('$_POST:');
	write_to_log($_POST);
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute Manager!");
}