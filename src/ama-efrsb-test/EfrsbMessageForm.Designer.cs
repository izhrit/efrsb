﻿namespace ama_efrsb_test
{
    partial class EfrsbMessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageWeb = new System.Windows.Forms.TabPage();
            this.buttonOpenBrowser = new System.Windows.Forms.Button();
            this.textBoxURL = new System.Windows.Forms.TextBox();
            this.labelURL = new System.Windows.Forms.Label();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.tabPageCommon = new System.Windows.Forms.TabPage();
            this.textBoxRevision = new System.Windows.Forms.TextBox();
            this.labelRevision = new System.Windows.Forms.Label();
            this.textBoxText = new System.Windows.Forms.TextBox();
            this.labelText = new System.Windows.Forms.Label();
            this.textBoxMessageType = new System.Windows.Forms.TextBox();
            this.labelMessageType = new System.Windows.Forms.Label();
            this.dateTimePickerPublishDate = new System.Windows.Forms.DateTimePicker();
            this.labelPublishDate = new System.Windows.Forms.Label();
            this.tabPageRaw = new System.Windows.Forms.TabPage();
            this.textBoxRaw = new System.Windows.Forms.TextBox();
            this.buttonClose = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.tabPageWeb.SuspendLayout();
            this.tabPageCommon.SuspendLayout();
            this.tabPageRaw.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tabPageWeb);
            this.tabControl.Controls.Add(this.tabPageCommon);
            this.tabControl.Controls.Add(this.tabPageRaw);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(760, 512);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageWeb
            // 
            this.tabPageWeb.Controls.Add(this.buttonOpenBrowser);
            this.tabPageWeb.Controls.Add(this.textBoxURL);
            this.tabPageWeb.Controls.Add(this.labelURL);
            this.tabPageWeb.Controls.Add(this.webBrowser);
            this.tabPageWeb.Location = new System.Drawing.Point(4, 22);
            this.tabPageWeb.Name = "tabPageWeb";
            this.tabPageWeb.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageWeb.Size = new System.Drawing.Size(752, 486);
            this.tabPageWeb.TabIndex = 0;
            this.tabPageWeb.Text = "Веб страница";
            this.tabPageWeb.UseVisualStyleBackColor = true;
            // 
            // buttonOpenBrowser
            // 
            this.buttonOpenBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenBrowser.Location = new System.Drawing.Point(604, 6);
            this.buttonOpenBrowser.Name = "buttonOpenBrowser";
            this.buttonOpenBrowser.Size = new System.Drawing.Size(142, 23);
            this.buttonOpenBrowser.TabIndex = 3;
            this.buttonOpenBrowser.Text = "Открыть в браузере..";
            this.buttonOpenBrowser.UseVisualStyleBackColor = true;
            this.buttonOpenBrowser.Click += new System.EventHandler(this.buttonOpenBrowser_Click);
            // 
            // textBoxURL
            // 
            this.textBoxURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxURL.Location = new System.Drawing.Point(53, 8);
            this.textBoxURL.Name = "textBoxURL";
            this.textBoxURL.ReadOnly = true;
            this.textBoxURL.Size = new System.Drawing.Size(545, 20);
            this.textBoxURL.TabIndex = 2;
            // 
            // labelURL
            // 
            this.labelURL.AutoSize = true;
            this.labelURL.Location = new System.Drawing.Point(6, 11);
            this.labelURL.Name = "labelURL";
            this.labelURL.Size = new System.Drawing.Size(41, 13);
            this.labelURL.TabIndex = 1;
            this.labelURL.Text = "Адрес:";
            // 
            // webBrowser
            // 
            this.webBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser.Location = new System.Drawing.Point(6, 35);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(740, 445);
            this.webBrowser.TabIndex = 0;
            // 
            // tabPageCommon
            // 
            this.tabPageCommon.Controls.Add(this.textBoxRevision);
            this.tabPageCommon.Controls.Add(this.labelRevision);
            this.tabPageCommon.Controls.Add(this.textBoxText);
            this.tabPageCommon.Controls.Add(this.labelText);
            this.tabPageCommon.Controls.Add(this.textBoxMessageType);
            this.tabPageCommon.Controls.Add(this.labelMessageType);
            this.tabPageCommon.Controls.Add(this.dateTimePickerPublishDate);
            this.tabPageCommon.Controls.Add(this.labelPublishDate);
            this.tabPageCommon.Location = new System.Drawing.Point(4, 22);
            this.tabPageCommon.Name = "tabPageCommon";
            this.tabPageCommon.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCommon.Size = new System.Drawing.Size(752, 512);
            this.tabPageCommon.TabIndex = 1;
            this.tabPageCommon.Text = "Общие сведения";
            this.tabPageCommon.UseVisualStyleBackColor = true;
            // 
            // textBoxRevision
            // 
            this.textBoxRevision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRevision.Location = new System.Drawing.Point(646, 26);
            this.textBoxRevision.Name = "textBoxRevision";
            this.textBoxRevision.ReadOnly = true;
            this.textBoxRevision.Size = new System.Drawing.Size(100, 20);
            this.textBoxRevision.TabIndex = 7;
            // 
            // labelRevision
            // 
            this.labelRevision.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRevision.AutoSize = true;
            this.labelRevision.Location = new System.Drawing.Point(553, 29);
            this.labelRevision.Name = "labelRevision";
            this.labelRevision.Size = new System.Drawing.Size(87, 13);
            this.labelRevision.TabIndex = 6;
            this.labelRevision.Text = "номер ревизии:";
            // 
            // textBoxText
            // 
            this.textBoxText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxText.Location = new System.Drawing.Point(10, 52);
            this.textBoxText.Multiline = true;
            this.textBoxText.Name = "textBoxText";
            this.textBoxText.ReadOnly = true;
            this.textBoxText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxText.Size = new System.Drawing.Size(736, 454);
            this.textBoxText.TabIndex = 5;
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(6, 29);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(104, 13);
            this.labelText.TabIndex = 4;
            this.labelText.Text = "Текст объявления:";
            // 
            // textBoxMessageType
            // 
            this.textBoxMessageType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMessageType.Location = new System.Drawing.Point(409, 3);
            this.textBoxMessageType.Name = "textBoxMessageType";
            this.textBoxMessageType.ReadOnly = true;
            this.textBoxMessageType.Size = new System.Drawing.Size(337, 20);
            this.textBoxMessageType.TabIndex = 3;
            // 
            // labelMessageType
            // 
            this.labelMessageType.AutoSize = true;
            this.labelMessageType.Location = new System.Drawing.Point(313, 6);
            this.labelMessageType.Name = "labelMessageType";
            this.labelMessageType.Size = new System.Drawing.Size(90, 13);
            this.labelMessageType.TabIndex = 2;
            this.labelMessageType.Text = "Тип объявления";
            // 
            // dateTimePickerPublishDate
            // 
            this.dateTimePickerPublishDate.Location = new System.Drawing.Point(107, 3);
            this.dateTimePickerPublishDate.Name = "dateTimePickerPublishDate";
            this.dateTimePickerPublishDate.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerPublishDate.TabIndex = 1;
            // 
            // labelPublishDate
            // 
            this.labelPublishDate.AutoSize = true;
            this.labelPublishDate.Location = new System.Drawing.Point(6, 6);
            this.labelPublishDate.Name = "labelPublishDate";
            this.labelPublishDate.Size = new System.Drawing.Size(95, 13);
            this.labelPublishDate.TabIndex = 0;
            this.labelPublishDate.Text = "Дата публикации";
            // 
            // tabPageRaw
            // 
            this.tabPageRaw.Controls.Add(this.textBoxRaw);
            this.tabPageRaw.Location = new System.Drawing.Point(4, 22);
            this.tabPageRaw.Name = "tabPageRaw";
            this.tabPageRaw.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRaw.Size = new System.Drawing.Size(752, 512);
            this.tabPageRaw.TabIndex = 2;
            this.tabPageRaw.Text = "Необработанное представление";
            this.tabPageRaw.UseVisualStyleBackColor = true;
            // 
            // textBoxRaw
            // 
            this.textBoxRaw.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRaw.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxRaw.Location = new System.Drawing.Point(6, 6);
            this.textBoxRaw.Multiline = true;
            this.textBoxRaw.Name = "textBoxRaw";
            this.textBoxRaw.ReadOnly = true;
            this.textBoxRaw.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxRaw.Size = new System.Drawing.Size(740, 500);
            this.textBoxRaw.TabIndex = 0;
            this.textBoxRaw.WordWrap = false;
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(697, 530);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 1;
            this.buttonClose.Text = "Закрыть";
            this.buttonClose.UseVisualStyleBackColor = true;
            // 
            // EfrsbMessageForm
            // 
            this.AcceptButton = this.buttonClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.tabControl);
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "EfrsbMessageForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Объявление в ЕФРСБ";
            this.tabControl.ResumeLayout(false);
            this.tabPageWeb.ResumeLayout(false);
            this.tabPageWeb.PerformLayout();
            this.tabPageCommon.ResumeLayout(false);
            this.tabPageCommon.PerformLayout();
            this.tabPageRaw.ResumeLayout(false);
            this.tabPageRaw.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageWeb;
        private System.Windows.Forms.TabPage tabPageCommon;
        private System.Windows.Forms.TabPage tabPageRaw;
        private System.Windows.Forms.TextBox textBoxRaw;
        private System.Windows.Forms.TextBox textBoxText;
        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.TextBox textBoxMessageType;
        private System.Windows.Forms.Label labelMessageType;
        private System.Windows.Forms.DateTimePicker dateTimePickerPublishDate;
        private System.Windows.Forms.Label labelPublishDate;
        private System.Windows.Forms.Button buttonOpenBrowser;
        private System.Windows.Forms.TextBox textBoxURL;
        private System.Windows.Forms.Label labelURL;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.TextBox textBoxRevision;
        private System.Windows.Forms.Label labelRevision;
        private System.Windows.Forms.Button buttonClose;
    }
}