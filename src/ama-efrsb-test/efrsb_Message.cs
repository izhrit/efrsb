﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Xml.Serialization;

namespace ama.efrsb
{
    public class Other
    {
        public string Text;
    }

    public class MessageInfo
    {
        [XmlAttribute()]
        public string MessageType;

        public Other Other;

        public string ReadableMessageType()
        {
            return sReadableMessageType(MessageType);
        }

        static string sReadableMessageType(string s)
        {
            switch (s)
            {
                case "ArbitralDecree": return "Сведения о судебном акте";
                case "Auction": return "Объявление о проведении торгов";
                case "Meeting": return "Сведения о собрании кредиторов";
                case "MeetingResult": return "Сведения о результатах проведения собрания кредиторов";
                case "TradeResult": return "Сведения о результатах торгов";
                case "Other": return "Иные сведения";
                case "AppointAdministration": return "Сведения о решении о назначении временной администрации";
                case "ChangeAdministration": return "Сведения об изменении состава временной администрации";
                case "TerminationAdministration": return "Сведения о прекращении деятельности временной администрации";
                case "BeginExecutoryProcess": return "Сведения о начале исполнительного производства";
                case "TransferAssertsForImplementation": return "Сведения о передаче имущества на реализацию";
                case "Annul": return "Сведения об аннулировании ранее опубликованных сообщений";
                case "PropertyInventoryResult": return "Сведения о результатах инвентаризации имущества должника";
                case "PropertyEvaluationReport": return "Сведения об отчете оценщика, об оценке имущества должника";
                case "SaleContractResult": return "Сведения о заключении договора купли-продажи";
                case "SaleContractResult2": return "Сведения о заключении договора купли-продажи";
                case "Committee": return "Сведения о проведении комитета кредиторов";
                case "CommitteeResult": return "Сообщение о результатах проведения комитета кредиторов";
                case "SaleOrderPledgedProperty": return "Об определении начальной продажной цены, утверждении порядка и условий проведения торгов по реализации предмета залога, порядка и условий обеспечения сохранности предмета залога";
                case "ReceivingCreditorDemand": return "Сведения о получении требования кредитора";
                case "DemandAnnouncement": return "Извещение о возможности предъявления требований";
                case "CourtAssertAcceptance": return "Объявление о принятии арбитражным судом заявления";
                case "FinancialStateInformation": return "Информация о финансовом состоянии";
                case "BankPayment": return "Объявление о выплатах Банка России";
                case "AssetsReturning": return "Объявление о возврате ценных бумаг и иного имущества";
                case "CourtAcceptanceStatement": return "Сведения о принятии заявления о признании должника банкротом";
                case "DeliberateBankruptcy": return "Сообщение о наличии или об отсутствии признаков преднамеренного или фиктивного банкротства";
                case "IntentionCreditOrg": return "Сообщение о намерении исполнить обязательства кредитной организации";
                case "LiabilitiesCreditOrg": return "Сообщение о признании исполнения заявителем обязательств кредитной организации несостоявшимся";
                case "PerformanceCreditOrg": return "Сообщение об исполнении обязательств кредитной организации";
                case "BuyingProperty": return "Сообщение о преимущественном праве выкупа имущества";
                case "DeclarationPersonDamages": return "Заявление о привлечении контролирующих должника лиц, а также иных лиц, к ответственности в виде возмещения убытков";
                case "ActPersonDamages": return "Судебный акт по результатам рассмотрения заявления о привлечении контролирующих должника лиц, а также иных лиц, к ответственности в виде возмещения убытков";
                case "ActReviewPersonDamages": return "Судебный акт по результатам пересмотра рассмотрения заявления о привлечении контролирующих должника лиц, а также иных лиц, к ответственности в виде возмещения убытков";
                case "DealInvalid": return "Заявление о признании сделки должника недействительной";
                case "ActDealInvalid": return "Судебный акт по результатам рассмотрения заявления об оспаривании сделки должника";
                case "ActDealInvalid2": return "Судебный акт по результатам рассмотрения заявления об оспаривании сделки должника";
                case "ActReviewDealInvalid": return "Судебный акт по результатам пересмотра рассмотрения заявления об оспаривании сделки должника";
                case "DeclarationPersonSubsidiary": return "Заявление о привлечении контролирующих должника лиц к субсидиарной ответственности";
                case "ActPersonSubsidiary": return "Судебный акт по результатам рассмотрения заявления о привлечении контролирующих должника лиц к субсидиарной ответственности";
                case "ActReviewPersonSubsidiary": return "Судебный акт по результатам пересмотра рассмотрения заявления о привлечении контролирующих должника лиц к субсидиарной ответственности";
                case "MeetingWorker": return "Уведомление о проведении собрания работников, бывших работников должника";
                case "MeetingWorkerResult": return "Сведения о решениях, принятых собранием работников, бывших работников должника";
                case "ViewDraftRestructuringPlan": return "Сведения о порядке и месте ознакомления с проектом плана реструктуризаци";
            }
            return s;
        }
    }

    public class BankruptPerson
    {
        [XmlAttribute()]
        public string Id;
        [XmlAttribute()]
        public string InsolventCategoryName;

        [XmlAttribute()]
        public string FirstName;
        [XmlAttribute()]
        public string MiddleName;
        [XmlAttribute()]
        public string LastName;
        [XmlAttribute()]
        public string Address;

        public string INN;
        public string Birthdate;
        public string Birthplace;
    }

    public class BankruptFirm
    {
        [XmlAttribute()]
        public string Id;
        [XmlAttribute()]
        public string InsolventCategoryName;

        [XmlAttribute()]
        public string FullName;
        [XmlAttribute()]
        public string ShortName;
        [XmlAttribute()]
        public string PostAddress;
        [XmlAttribute()]
        public string OKPO;
        [XmlAttribute()]
        public string OGRN;
        [XmlAttribute()]
        public string LegalAddress;

        public string INN;
    }

    public class BankruptInfo
    {
        [XmlAttribute()]
        public string BankruptType;
        [XmlAttribute()]
        public string BankruptCategory;

        public BankruptFirm BankruptFirm;
        public BankruptPerson BankruptPerson;
    }

    public class Sro
    {
        [XmlAttribute()]
        public string SroId;

        public string SroName;
        public string SroRegistryNumber;
        public string OGRN;
        public string INN;
        public string LegalAddress;
    }

    public class ArbitrManager
    {
        [XmlAttribute()]
        public string Id;

        [XmlAttribute()]
        public string FirstName;
        [XmlAttribute()]
        public string MiddleName;
        [XmlAttribute()]
        public string LastName;
        [XmlAttribute()]
        public string INN;
        [XmlAttribute()]
        public string SNILS;
        [XmlAttribute()]
        public string RegistryNumber;

        public string OGRN;

        public Sro Sro;

        public string CorrespondenceAddress;
    }

    public class PublisherInfo
    {
        [XmlAttribute()]
        public string PublisherType;

        public ArbitrManager ArbitrManager;
    }

    public class MessageData
    {
        public string Id;
        public string Number;
        public string CaseNumber;

        public PublisherInfo PublisherInfo;
        public MessageInfo MessageInfo;
        public BankruptInfo BankruptInfo;

        public DateTime PublishDate;
        public string BankruptId;
        public string MessageGUID;

        static XmlSerializer serializer = new XmlSerializer(typeof(MessageData));

        public static MessageData ReadFromXmlText(string xml_text)
        {
            using (StringReader reader = new StringReader(xml_text))
            using (System.Xml.XmlReader xreader = System.Xml.XmlReader.Create(reader))
            {
                return serializer.Deserialize(xreader) as MessageData;
            }
        }

        public string ToXmlString()
        {
            using (StringWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, this);
                return writer.ToString();
            }
        }
    }
}
