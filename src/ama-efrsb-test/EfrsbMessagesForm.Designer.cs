﻿namespace ama_efrsb_test
{
    partial class EfrsbMessagesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDebtor = new System.Windows.Forms.Label();
            this.textBoxDebtorName = new System.Windows.Forms.TextBox();
            this.labelINN = new System.Windows.Forms.Label();
            this.textBoxINN = new System.Windows.Forms.TextBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PublishDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MessageType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelProgress = new System.Windows.Forms.Label();
            this.labelOGRN = new System.Windows.Forms.Label();
            this.textBoxOGRN = new System.Windows.Forms.TextBox();
            this.labelSNILS = new System.Windows.Forms.Label();
            this.textBoxSNILS = new System.Windows.Forms.TextBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // labelDebtor
            // 
            this.labelDebtor.AutoSize = true;
            this.labelDebtor.Location = new System.Drawing.Point(12, 15);
            this.labelDebtor.Name = "labelDebtor";
            this.labelDebtor.Size = new System.Drawing.Size(68, 13);
            this.labelDebtor.TabIndex = 0;
            this.labelDebtor.Text = "О должнике";
            // 
            // textBoxDebtorName
            // 
            this.textBoxDebtorName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDebtorName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxDebtorName.Location = new System.Drawing.Point(86, 15);
            this.textBoxDebtorName.Name = "textBoxDebtorName";
            this.textBoxDebtorName.ReadOnly = true;
            this.textBoxDebtorName.Size = new System.Drawing.Size(526, 13);
            this.textBoxDebtorName.TabIndex = 3;
            // 
            // labelINN
            // 
            this.labelINN.AutoSize = true;
            this.labelINN.Location = new System.Drawing.Point(46, 39);
            this.labelINN.Name = "labelINN";
            this.labelINN.Size = new System.Drawing.Size(34, 13);
            this.labelINN.TabIndex = 2;
            this.labelINN.Text = "ИНН:";
            // 
            // textBoxINN
            // 
            this.textBoxINN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxINN.Location = new System.Drawing.Point(86, 39);
            this.textBoxINN.Name = "textBoxINN";
            this.textBoxINN.ReadOnly = true;
            this.textBoxINN.Size = new System.Drawing.Size(136, 13);
            this.textBoxINN.TabIndex = 4;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.PublishDate,
            this.MessageType});
            this.dataGridView.Location = new System.Drawing.Point(15, 100);
            this.dataGridView.MultiSelect = false;
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView.Size = new System.Drawing.Size(597, 298);
            this.dataGridView.TabIndex = 0;
            this.dataGridView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView_MouseDoubleClick);
            // 
            // Index
            // 
            this.Index.HeaderText = "Index";
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Visible = false;
            // 
            // PublishDate
            // 
            this.PublishDate.HeaderText = "Дата публикации";
            this.PublishDate.Name = "PublishDate";
            this.PublishDate.ReadOnly = true;
            this.PublishDate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.PublishDate.Width = 120;
            // 
            // MessageType
            // 
            this.MessageType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MessageType.HeaderText = "Тип сообщения";
            this.MessageType.Name = "MessageType";
            this.MessageType.ReadOnly = true;
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(17, 71);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "Обновить";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonOpen
            // 
            this.buttonOpen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpen.Location = new System.Drawing.Point(539, 71);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(75, 23);
            this.buttonOpen.TabIndex = 1;
            this.buttonOpen.Text = "Открыть";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(537, 407);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 5;
            this.buttonClose.Text = "Закрыть";
            this.buttonClose.UseVisualStyleBackColor = true;
            // 
            // labelProgress
            // 
            this.labelProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelProgress.ForeColor = System.Drawing.Color.ForestGreen;
            this.labelProgress.Location = new System.Drawing.Point(98, 73);
            this.labelProgress.Name = "labelProgress";
            this.labelProgress.Size = new System.Drawing.Size(435, 18);
            this.labelProgress.TabIndex = 6;
            this.labelProgress.Text = "Обновление";
            // 
            // labelOGRN
            // 
            this.labelOGRN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelOGRN.AutoSize = true;
            this.labelOGRN.Location = new System.Drawing.Point(428, 39);
            this.labelOGRN.Name = "labelOGRN";
            this.labelOGRN.Size = new System.Drawing.Size(39, 13);
            this.labelOGRN.TabIndex = 7;
            this.labelOGRN.Text = "ОГРН:";
            // 
            // textBoxOGRN
            // 
            this.textBoxOGRN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOGRN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxOGRN.Location = new System.Drawing.Point(473, 39);
            this.textBoxOGRN.Name = "textBoxOGRN";
            this.textBoxOGRN.ReadOnly = true;
            this.textBoxOGRN.Size = new System.Drawing.Size(136, 13);
            this.textBoxOGRN.TabIndex = 8;
            // 
            // labelSNILS
            // 
            this.labelSNILS.AutoSize = true;
            this.labelSNILS.Location = new System.Drawing.Point(230, 39);
            this.labelSNILS.Name = "labelSNILS";
            this.labelSNILS.Size = new System.Drawing.Size(48, 13);
            this.labelSNILS.TabIndex = 9;
            this.labelSNILS.Text = "СНИЛС:";
            // 
            // textBoxSNILS
            // 
            this.textBoxSNILS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSNILS.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSNILS.Location = new System.Drawing.Point(284, 39);
            this.textBoxSNILS.Name = "textBoxSNILS";
            this.textBoxSNILS.ReadOnly = true;
            this.textBoxSNILS.Size = new System.Drawing.Size(136, 13);
            this.textBoxSNILS.TabIndex = 10;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDelete.Location = new System.Drawing.Point(458, 71);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 11;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // EfrsbMessagesForm
            // 
            this.AcceptButton = this.buttonOpen;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(624, 442);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.textBoxSNILS);
            this.Controls.Add(this.labelSNILS);
            this.Controls.Add(this.textBoxOGRN);
            this.Controls.Add(this.labelOGRN);
            this.Controls.Add(this.labelProgress);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonOpen);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.textBoxINN);
            this.Controls.Add(this.labelINN);
            this.Controls.Add(this.textBoxDebtorName);
            this.Controls.Add(this.labelDebtor);
            this.MinimumSize = new System.Drawing.Size(640, 480);
            this.Name = "EfrsbMessagesForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Объявления в ЕФРСБ";
            this.Load += new System.EventHandler(this.EfrsbMessagesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelDebtor;
        private System.Windows.Forms.Label labelINN;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.DataGridViewTextBoxColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn PublishDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn MessageType;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelProgress;
        public System.Windows.Forms.TextBox textBoxDebtorName;
        public System.Windows.Forms.TextBox textBoxINN;
        private System.Windows.Forms.Label labelOGRN;
        private System.Windows.Forms.Label labelSNILS;
        public System.Windows.Forms.TextBox textBoxOGRN;
        public System.Windows.Forms.TextBox textBoxSNILS;
        private System.Windows.Forms.Button buttonDelete;
    }
}