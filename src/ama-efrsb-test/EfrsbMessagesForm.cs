﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using ama.efrsb;

namespace ama_efrsb_test
{
    public partial class EfrsbMessagesForm : Form
    {
        public EfrsbMessagesForm()
        {
            InitializeComponent();
        }

        public List<PlainMessage> _messages;

        public void DataLoad(List<PlainMessage> messages)
        {
            labelProgress.Text = "";
            _messages = messages;
            dataGridView.Rows.Clear();
            int i = 0;
            foreach (PlainMessage pm in messages)
            {
                int irow = dataGridView.Rows.Add();
                DataGridViewRow row = dataGridView.Rows[irow];
                DataGridViewCellCollection cells = row.Cells;

                MessageData msg = MessageData.ReadFromXmlText(pm.Body);

                cells["Index"].Value = (i++).ToString();
                cells["PublishDate"].Value = msg.PublishDate.ToShortDateString();
                if (null != msg.MessageInfo)
                    cells["MessageType"].Value = msg.MessageInfo.ReadableMessageType();

                if (null!=msg.BankruptInfo)
                {
                    BankruptInfo bi= msg.BankruptInfo;
                    BankruptFirm bf = bi.BankruptFirm;
                    BankruptPerson bp = bi.BankruptPerson;
                    if (null!=bp)
                    {
                        if (string.IsNullOrEmpty(textBoxDebtorName.Text))
                            textBoxDebtorName.Text = bp.LastName + " " + bp.FirstName + " " + bp.MiddleName;
                        if (string.IsNullOrEmpty(textBoxINN.Text))
                            textBoxINN.Text = bp.INN;
                    }
                    if (null != bf)
                    {
                        if (string.IsNullOrEmpty(textBoxDebtorName.Text))
                            textBoxDebtorName.Text = bf.ShortName;
                        if (string.IsNullOrEmpty(textBoxINN.Text))
                            textBoxINN.Text = bf.INN;
                    }
                }
            }
        }

        private void dataGridView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            OpenMessage();
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            OpenMessage();
        }

        void OpenMessage()
        {
            DataGridViewSelectedRowCollection srows= dataGridView.SelectedRows;
            if (null!=srows && 0!=srows.Count)
            {
                int irow = srows[0].Index;
                PlainMessage pm= _messages[irow];
                EfrsbMessageForm frm = new EfrsbMessageForm();
                frm.LoadData(pm.Revision, pm.Body);
                frm.ShowDialog();
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxINN.Text))
            {
                string inn = textBoxINN.Text;
                string ogrn = textBoxOGRN.Text;
                string snils = textBoxSNILS.Text;
                Client c = new Client();

                int revision_greater_than = 0;
                List<PlainMessage> messages= null;
                do
                {
                    if (null!=_messages)
                    {
                        foreach (PlainMessage pm in _messages)
                        {
                            if (pm.Revision > revision_greater_than)
                                revision_greater_than = pm.Revision;
                        }
                    }
                    labelProgress.Text = DateTime.Now.ToLocalTime() + " запросили ревизии >" + revision_greater_than.ToString();
                    Update();
                    Application.DoEvents();
                    messages = c.GetNewMessages(inn, ogrn, snils, revision_greater_than, 100);
                    if (null == _messages)
                    {
                        _messages = messages;
                    }
                    else
                    {
                        _messages.AddRange(messages);
                    }
                }
                while (null != messages && 0 != messages.Count);
            }
            DataLoad(_messages);
        }

        private void EfrsbMessagesForm_Load(object sender, EventArgs e)
        {
            labelProgress.Text = "";

            if (string.IsNullOrEmpty(textBoxINN.Text))
                labelINN.Visible = false;
            if (string.IsNullOrEmpty(textBoxOGRN.Text))
                labelOGRN.Visible = false;
            if (string.IsNullOrEmpty(textBoxSNILS.Text))
                labelSNILS.Visible = false;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection srows = dataGridView.SelectedRows;
            if (null != srows && 0 != srows.Count)
            {
                int irow = srows[0].Index;
                PlainMessage pm = _messages[irow];
                string text= "Вы действительно хотите удалить выделенное объявление со своего диска?";
                string caption= "Подтверждение удаления";
                if (DialogResult.Yes==MessageBox.Show(this,text,caption,MessageBoxButtons.YesNo))
                {
                    // TODO: удалить объявление (тоже через event)
                }
            }
        }
    }
}
