﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Forms;

using ama.efrsb;

namespace ama_efrsb_test
{
    public partial class EfrsbMessageForm : Form
    {
        public EfrsbMessageForm()
        {
            InitializeComponent();
        }

        public static string prefixUrl= "https://bankrot.fedresurs.ru/MessageWindow.aspx?ID=";

        public void LoadData(int Revision, string Body)
        {
            this.textBoxRevision.Text = Revision.ToString();
            this.textBoxRaw.Text = Body;

            MessageData message = MessageData.ReadFromXmlText(Body);
            if (null!=message)
            {
                this.dateTimePickerPublishDate.Value = message.PublishDate;
                if (null != message.MessageInfo && null != message.MessageInfo.Other)
                    this.textBoxText.Text = message.MessageInfo.Other.Text;
                if (null != message.MessageInfo)
                    this.textBoxMessageType.Text = message.MessageInfo.ReadableMessageType();
                string url= prefixUrl + message.MessageGUID;
                this.textBoxURL.Text = url;
                this.webBrowser.Navigate(url);

                if (null!= message.BankruptInfo)
                {
                    BankruptInfo bi = message.BankruptInfo;
                    BankruptFirm bf = bi.BankruptFirm;
                    BankruptPerson bp = bi.BankruptPerson;
                    if (null != bf)
                    {
                        this.Text = this.Text + " для " + bf.ShortName;
                    }
                    if (null != bp)
                    {
                        this.Text = this.Text + " для " + bp.LastName + " " + bp.FirstName + " " + bp.MiddleName;
                    }
                }
            }
        }

        private void buttonOpenBrowser_Click(object sender, EventArgs e)
        {
            string url= this.textBoxURL.Text;
            try
            {
                ProcessStartInfo si = new ProcessStartInfo(url);
                si.UseShellExecute = true;
                Process.Start(si);
            }
            catch
            {
                System.Diagnostics.Process.Start("iexplore.exe", url);
            }
        }
    }
}
