﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

using ama.efrsb;

namespace ama_efrsb_test
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                ProcessArgs(args);
                switch (action)
                {
                    case "GetNewMessages": test_GetNewMessages(); break;
                    case "GetDebtorsInformation": test_GetDebtorsInformation(); break;
                    case "GetAllSROInformation": test_GetAllSROInformation(); break;
                    case "GetManagersInformation": test_GetManagersInformation(); break;
                    case "ReadWriteMessage": test_ReadWriteMessage(); break;
                    case "ViewMessage": test_ViewMessage(); break;
                    case "ViewMessages": test_ViewMessages(); break;
                }
            }
            catch (Exception ex)
            {
                Console.Out.Write(ex);
            }
        }

        static void test_ViewMessage()
        {
            EfrsbMessageForm.prefixUrl = "http://test-bankrot.interfax.ru/MessageWindow.aspx?ID=";
            string xml_text = File.ReadAllText(argument_file_path);
            EfrsbMessageForm frm = new EfrsbMessageForm();
            frm.LoadData(0,xml_text);
            frm.ShowDialog();
        }

        static void test_ViewMessages()
        {
            EfrsbMessageForm.prefixUrl = "http://test-bankrot.interfax.ru/MessageWindow.aspx?ID=";
            EfrsbMessagesForm frm = new EfrsbMessagesForm();

            if (!string.IsNullOrEmpty(inn))
                frm.textBoxINN.Text = inn;
            if (!string.IsNullOrEmpty(ogrn))
                frm.textBoxOGRN.Text = ogrn;
            if (!string.IsNullOrEmpty(snils))
                frm.textBoxSNILS.Text = snils;

            if (!string.IsNullOrEmpty(argument_file_path))
            {
                string json_text = File.ReadAllText(argument_file_path);
                List<PlainMessage> messages = Client.jsSerializer.Deserialize<List<PlainMessage>>(json_text);
                frm.DataLoad(messages);
            }

            frm.ShowDialog();
        }

        static void test_ReadWriteMessage()
        {
            Console.OutputEncoding = new UTF8Encoding();
            string xml_text = File.ReadAllText(argument_file_path);
            MessageData message = MessageData.ReadFromXmlText(xml_text);
            Console.Out.Write(message.ToXmlString());
        }

        static void test_GetNewMessages()
        {
            Client.base_url = efrsb_base_url;
            Client c = new Client();
            List<PlainMessage> messages = c.GetNewMessages(inn, ogrn,snils,revision_greater_than,limit);

            foreach (PlainMessage pm in messages)
            {
                Console.Out.WriteLine("--- Message ---");
                Console.Out.Write("Revision=");
                Console.Out.WriteLine(pm.Revision);

                Console.Out.WriteLine("Body:");
                Console.Out.WriteLine(pm.Body);
            }
        }

        static void test_GetDebtorsInformation()
        {
            Client c = new Client();
            List<Debtor> information = c.GetDebtorsInformation(inn, regnum);

            foreach (Debtor pd in information)
            {
                Console.Out.WriteLine("--- Debtor ---");
                Console.Out.Write("Name=");
                Console.Out.WriteLine(pd.Name);

                Console.Out.Write("INN:");
                Console.Out.WriteLine(pd.INN);

                Console.Out.Write("OGRN:");
                Console.Out.WriteLine(pd.OGRN);

                Console.Out.Write("SNILS:");
                Console.Out.WriteLine(pd.SNILS);

                Console.Out.Write("Address:");
                Console.Out.WriteLine(pd.Address);

                Console.Out.Write("Case_numbers:");
                foreach(string number in pd.Case_numbers)
                    Console.Out.WriteLine(number);
            }
        }

        static void test_GetAllSROInformation()
        {
            Client c = new Client();
            List<SRO> information = c.GetAllSROInformation();

            foreach (SRO pd in information)
            {
                Console.Out.WriteLine("--- SRO ---");
                Console.Out.Write("RegNum=");
                Console.Out.WriteLine(pd.RegNum);

                Console.Out.WriteLine("--- Name ---");
                Console.Out.Write("Name=");
                Console.Out.WriteLine(pd.Name);

                Console.Out.Write("INN:");
                Console.Out.WriteLine(pd.INN);

                Console.Out.Write("OGRN:");
                Console.Out.WriteLine(pd.OGRN);

                Console.Out.Write("Stitle:");
                Console.Out.WriteLine(pd.Stitle);

                Console.Out.Write("UrAdress:");
                Console.Out.WriteLine(pd.UrAdress);

            }
        }

        static void test_GetManagersInformation()
        {
            Client c = new Client();
            List<Manager> information = c.GetManagersInformation(q);

            foreach (Manager pd in information)
            {
                Console.Out.WriteLine("--- Manager ---");

                Console.Out.Write("lastName=");
                Console.Out.WriteLine(pd.lastName);

                Console.Out.Write("firstName:");
                Console.Out.WriteLine(pd.firstName);

                Console.Out.Write("middleName:");
                Console.Out.WriteLine(pd.middleName);

                Console.Out.Write("RegNum=");
                Console.Out.WriteLine(pd.RegNum);

                Console.Out.Write("INN=");
                Console.Out.WriteLine(pd.INN);

                Console.Out.Write("Stitle:");
                Console.Out.WriteLine(pd.Stitle);
            }
        }

        static string efrsb_base_url = "http://local.test/efrsb/";
        static string action;

        static string inn;
        static string ogrn;
        static string snils;
        static int revision_greater_than;
        static int limit;
        static string argument_file_path;
        static string regnum;
        static string q;

        static void ProcessArgs(string[] args)
        {
            for (int i = 0; i < args.Length; i++)
            {
                string[] ap = args[i].Split('=');
                switch (ap[0])
                {
                    case "--action": action = ap[1]; break;
                    case "--regnum": regnum = ap[1]; break;
                    case "--q": q = ap[1]; break;
                    case "--inn": inn = ap[1]; break;
                    case "--ogrn": ogrn = ap[1]; break;
                    case "--snils": snils = ap[1]; break;
                    case "--limit": limit= int.Parse(ap[1]); break;
                    case "--revision-greater-than": revision_greater_than = int.Parse(ap[1]); break;
                    case "--file-path": argument_file_path = ap[1]; break;
                }
            }
        }
    }
}
